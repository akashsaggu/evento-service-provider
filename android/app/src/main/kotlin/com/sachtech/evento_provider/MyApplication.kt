package com.sachtech.evento_provider

import android.content.Context
import io.flutter.app.FlutterApplication
import androidx.multidex.MultiDex

/**
 * Created by Akash Saggu(R4X) on 6/10/19 at 11:16 AM
 * akashsaggu@protonmail.com
 * @Version 1 6/10/19
 * @Updated on 6/10/19
 */
class MyApplication: FlutterApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

}