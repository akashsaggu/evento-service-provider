class Validations {
  static Validator validateName(String value) {
    if (value.isEmpty) return Validator(msg: 'Name is required.');
    final RegExp nameExp = new RegExp(r'^[A-za-z ]+$');
    if (!nameExp.hasMatch(value))
      return Validator(msg: 'Please enter only alphabetical characters.');
    return Validator(status: true);
  }

  static Validator validateEmail(String value) {
    if (value.isEmpty) return Validator(msg: 'Email is required.');
    final RegExp nameExp = new RegExp(r'^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$');
    if (!nameExp.hasMatch(value))
      return Validator(msg: 'Invalid email address.');
    return Validator(status: true);
  }

  static Validator validatePassword(String value) {
    if (value.isEmpty) return Validator(msg: 'Please choose a password.');
    if (value.length < 6)
      return Validator(msg: 'Password must be six character long.');
    return Validator(status: true);
  }

  static Validator validatePhoneNumber(String value) {
    if (value.isEmpty) return Validator(msg: 'Please choose mobile number.');
    if (value.length < 10)
      return Validator(msg: 'Phone number must be 10 character long.');

    if(value.length > 15)
      return Validator(msg: 'Phone number not be greater than 15 characters.');
    return Validator(status: true);
  }

  static Validator validateNotEmpty(String value, String field) {
    if (value.isEmpty) return Validator(msg: 'Please choose a $field.');
    else return Validator(status: true);
  }

}

class Validator {
  bool status;
  String msg;

  Validator({this.status = false, this.msg = 'success'});
}
