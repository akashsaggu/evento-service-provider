import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

void toast(context, msg) {
  Toast.show(msg, context, duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
}


double screenWidth(BuildContext context) {
  return MediaQuery
      .of(context)
      .size
      .width;
}

double screenHeight(BuildContext context) {
  return MediaQuery
      .of(context)
      .size
      .height;
}

T getSuperWidget <T extends Widget>(BuildContext context) {
  final type = _typeOf<T>();
  T widget = context.ancestorWidgetOfExactType(type);
  return widget;
}

Type _typeOf<T>() => T;