import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OverlayHelper {
  static const int BOTTOM = 0;
  static const int CENTER = 1;
  static const int TOP = 2;

  static void show(
      {@required Widget widget,
      @required BuildContext context,
      int duration,
      int gravity = TOP,
      bool blurBackground = false}) {
    OverlayView.dismiss();
    OverlayView.createView(widget, context, duration, gravity, blurBackground);
  }
}

class OverlayView {
  static final OverlayView _singleton = new OverlayView._internal();

  factory OverlayView() {
    return _singleton;
  }

  OverlayView._internal();

  static OverlayState overlayState;
  static OverlayEntry _overlayEntry;
  static OverlayEntry _blurOverlayEntry;
  static bool _isVisible = false;

  static void createView(Widget widget, BuildContext context, int duration,
      int gravity, bool blurBackground) async {
    _isVisible = true;

    overlayState = Overlay.of(context);

    _overlayEntry = new OverlayEntry(
      builder: (BuildContext context) =>
          PositionedWidget(widget: widget, gravity: gravity),
    );

    if (blurBackground) {
      _blurOverlayEntry = new OverlayEntry(
          builder: (context) => BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
                child: new Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration:
                      new BoxDecoration(color: Colors.black.withOpacity(0.1)),
                  child: SizedBox(),
                ),
              ));
      overlayState.insert(_blurOverlayEntry);
    }

    overlayState.insert(_overlayEntry);
    if (duration != null)
      await new Future.delayed(Duration(milliseconds: duration));
    dismiss();
  }

  static dismiss() async {
    if (!_isVisible) {
      return;
    }
    _isVisible = false;
    _overlayEntry?.remove();
    _blurOverlayEntry?.remove();
  }
}

class PositionedWidget extends StatelessWidget {
  PositionedWidget({Key key, @required this.widget, @required this.gravity})
      : super(key: key);

  final Widget widget;
  final int gravity;

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: gravity == 2 ? 50 : null,
        bottom: gravity == 0 ? 50 : null,
        child: Material(
          color: Colors.transparent,
          child: widget,
        ));
  }
}
