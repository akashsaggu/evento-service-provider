import 'dart:io';
import 'dart:math';

import 'package:evento_provider/address/MapWidget.dart';
import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/bloc/CategoryBloc.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/common/repo/UserRepository.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/model/register_helper_entity.dart';
import 'package:evento_provider/screens/register/repo/RegisterRepo.dart';
import 'package:evento_provider/services/validations.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:evento_provider/widgets/AddressWidget.dart';
import 'package:evento_provider/widgets/CategorySpinner.dart';
import 'package:evento_provider/widgets/CitySpinner.dart';
import 'package:evento_provider/widgets/CountrySpinner.dart';
import 'package:evento_provider/widgets/DaysBottomSheet.dart';
import 'package:evento_provider/widgets/MultipleMobileNo.dart';
import 'package:evento_provider/widgets/ProgressWidget.dart';
import 'package:evento_provider/widgets/SubCategorySpinner.dart';
import 'package:evento_provider/widgets/TimeBottomSheet.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;

final firstNameController = TextEditingController();
final lastNameController = TextEditingController();
final phoneController = TextEditingController();
final emailController = TextEditingController();
final passwordController = TextEditingController();
final confirmPasswordController = TextEditingController();
final addressTextController = TextEditingController();
final addressController = TextEditingController();
final tagLineController = TextEditingController();
final instaController = TextEditingController();
final facebookController = TextEditingController();

var categoryValue = "";
var subCategoryValue = "";
var countryValue = "";
var cityValue = "";
var startDay =
    "${GlobalCustomer.customerEntity.data.workingDays.substring(0, 3)}day";
var endDay =
    "${GlobalCustomer.customerEntity.data.workingDays.substring(4, 7)}day";
var startTime = "";
var endTime = "";
File image;
var isDataSet = false;
String fcmToken = "dsfdg";
String gender = "Male";
String dob = "";
String lastAddress = "";
AddressHelper addressObject = AddressHelper();

class EditProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProgressWidgetWrapper(child: EditProfileScreenUi());
  }
}

class EditProfileScreenUi extends StatelessWidget {
  final registerRepo = RegisterRepo();

  profile(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          InkWell(
            onTap: () async {
              image = await ImagePicker.pickImage(source: ImageSource.gallery);
            },
            child: CircleAvatar(
                radius: 70.0,
                backgroundImage: image == null
                    ? NetworkImage(
                        GlobalCustomer.customerEntity.data.profilePic,
                        scale: 1)
                    : FileImage(image, scale: 1)),
          ),
        ],
      );

  final email = TextFormField(
    controller: emailController,
    style: TextStyle(color: Colors.white),
    keyboardType: TextInputType.emailAddress,
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Email',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  final firstName = TextFormField(
    controller: firstNameController,
    style: TextStyle(color: Colors.white),
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'First name',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  final lastName = TextFormField(
    controller: lastNameController,
    keyboardType: TextInputType.emailAddress,
    autofocus: false,
    style: TextStyle(color: Colors.white),
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Last name',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  signUpButton(BuildContext context) => Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
          ),
          onPressed: () {
            signUp(context);
          },
          padding: EdgeInsets.all(12),
          color: Colors.blueAccent,
          child: Text('Update', style: TextStyle(color: Colors.white)),
        ),
      );

  final facebookLink = TextFormField(
    controller: facebookController,
    style: TextStyle(color: Colors.white),
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Facebook Id',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  final instaLink = TextFormField(
    controller: instaController,
    style: TextStyle(color: Colors.white),
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Instagram ID',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  final category = CategorySpinner(
    (value) {
      categoryValue = value.id;
    },
    preSelectedCategory: GlobalCustomer.customerEntity.data.category,
  );

  final subCategory = SubCategorySpinner(
    (value) {
      subCategoryValue = value.id;
    },
    preSelectedSubCategory: GlobalCustomer.customerEntity.data.subcategory,
  );

  final country = CountrySpinner(
    (value) {
      countryValue = value.id;
    },
    preSelectedCountry: GlobalCustomer.customerEntity.data.country,
  );

  final city = CitySpinner(
    (value) {
      cityValue = value.id;
    },
    preSelectedCity: GlobalCustomer.customerEntity.data.city,
  );

  final countryLabel = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("Select Country", style: TextStyle(color: Colors.white)));

  final cityLabel = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("Select City", style: TextStyle(color: Colors.white)));

  final selectStartDay = DaysBottomSheet((value) {
    startDay = value;
  }, "${GlobalCustomer.customerEntity.data.workingDays.split("-")[0]}");

  final selectEndDay = DaysBottomSheet((value) {
    endDay = value;
  }, "${GlobalCustomer.customerEntity.data.workingDays.split("-")[1]}");

  final StartTime = TimeBottomSheet((value) {
    startTime = value;
  }, GlobalCustomer.customerEntity.data.workingHours
      .split("-")[0]
      .split(":")[0]
      .length ==
      1
      ? "0" +
      GlobalCustomer.customerEntity.data.workingHours
          .split("-")[0]
          .split(":")[0] +
      ":" +
      GlobalCustomer.customerEntity.data.workingHours
          .split("-")[0]
          .split(":")[1]
      : GlobalCustomer.customerEntity.data.workingHours.split("-")[0]);

  final EndTime = TimeBottomSheet((value) {
    endTime = value;
  },
      GlobalCustomer.customerEntity.data.workingHours
                  .split("-")[1]
                  .split(":")[0]
                  .length ==
              1
          ? "0" +
              GlobalCustomer.customerEntity.data.workingHours
                  .split("-")[1]
                  .split(":")[0] +
              ":" +
              GlobalCustomer.customerEntity.data.workingHours
                  .split("-")[1]
                  .split(":")[1]
          : GlobalCustomer.customerEntity.data.workingHours.split("-")[1]);

  final address = AddresWidget((value) {
    addressObject = value;
  }, address: GlobalCustomer.customerEntity.data.address);

  static List<PreSelectedMobile> getPreselectedMobile() {
    final preSelectedMobileList = <PreSelectedMobile>[];
    if (GlobalCustomer.customerEntity.data.mobile.contains(",")) {
      GlobalCustomer.customerEntity.data.mobile.split(",").forEach((mobileN) {
        final mobile = mobileN.split(" ");
        preSelectedMobileList
            .add(PreSelectedMobile(countryCode: mobile[0], mobile: mobile[1]));
      });
    } else {
      final mobile = GlobalCustomer.customerEntity.data.mobile.split(" ");
      preSelectedMobileList
          .add(PreSelectedMobile(countryCode: mobile[0], mobile: mobile[1]));
    }
    return preSelectedMobileList;
  }

  final mobileNumber = MultipleMobileNoWidget(
    preSelectedMobiles: getPreselectedMobile(),
  );

  @override
  Widget build(BuildContext context) {
    final categoryBloc = CategoryBloc();
    categoryBloc.getCategories();
    categoryBloc.getCountries("en");
    setPreviousFields();

    final alreadyAccountLabel = Center(
        child: RichText(
      text: TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: TextStyle(
          fontSize: 14.0,
          color: Colors.white,
        ),
        children: <TextSpan>[
          TextSpan(text: "Already have an account? "),
          TextSpan(
              text: 'Log in',
              style: new TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.blueAccent)),
        ],
      ),
    ));
    return WillPopScope(
      onWillPop: () async {
        /*firstNameController.clear();
        lastNameController.clear();
        phoneController.clear();
        emailController.clear();
        passwordController.clear();
        confirmPasswordController.clear();
        addressTextController.clear();
        addressController.clear();
        tagLineController.clear();
        instaController.clear();
        facebookController.clear();*/



        fcmToken = "dsfdg";
        gender = "Male";
        dob = "";
        lastAddress = "";
        addressObject = null;
        return true;
      },
      child: BlocProvider<CategoryBloc>(
        bloc: categoryBloc,
        child: Scaffold(
          backgroundColor: AppColors.primaryColor,
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30))),
            title: Text("UPDATE PROFILE"),
            elevation: 6,
            centerTitle: true,
            backgroundColor: Colors.blueAccent,
          ),
          body: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Colors.blue[900], Colors.blueAccent],
                        begin: Alignment.bottomRight,
                        end: Alignment.topCenter)),
                child: Center(
                  child: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.only(left: 24.0, right: 24.0),
                    children: <Widget>[
                      SizedBox(height: 32.0),
                      profile(context),
                      SizedBox(height: 24.0),
                      firstName,
                      SizedBox(height: 8.0),
                      lastName,
                      SizedBox(height: 8.0),
                      email,
                      SizedBox(height: 8.0),
                      facebookLink,
                      SizedBox(height: 8.0),
                      instaLink,
                      SizedBox(height: 8.0),
                      SizedBox(height: 8.0),
                      mobileNumber,
                      SizedBox(height: 8.0),
                      address,
                      SizedBox(height: 8.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: StartTime),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: EndTime),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: selectStartDay),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: selectEndDay),
                        ],
                      ),
                      SizedBox(height: 4.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: countryLabel),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: cityLabel),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: country),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: city),
                        ],
                      ),
                      SizedBox(height: 8.0),
                      /*Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: categoryLabel),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: subCategoryLabel),
                        ],
                      ),*/
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: category),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: subCategory),
                        ],
                      ),
                      signUpButton(context),
                      SizedBox(height: 16.0),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void setPreviousFields() {
    if (!isDataSet) {
      emailController.text = GlobalCustomer.customerEntity.data.email;
      firstNameController.text = GlobalCustomer.customerEntity.data.fname;
      lastNameController.text = GlobalCustomer.customerEntity.data.lname;
      facebookController.text =
          GlobalCustomer.customerEntity.data.facebookPageId;
      instaController.text = GlobalCustomer.customerEntity.data.instagramPageId;
      categoryValue = GlobalCustomer.customerEntity.data.category;
      subCategoryValue = GlobalCustomer.customerEntity.data.subcategory;
      countryValue = GlobalCustomer.customerEntity.data.country;
      cityValue = GlobalCustomer.customerEntity.data.city;
      startDay =
          "${GlobalCustomer.customerEntity.data.workingDays.substring(0, 3)}day";
      endDay =
          "${GlobalCustomer.customerEntity.data.workingDays.substring(4, 7)}day";
      setStartAndEndTime();
      isDataSet = true;
    }
  }

  void setStartAndEndTime() {
    var time = GlobalCustomer.customerEntity.data.workingHours.split("-");
    var startTimeX = time[0].split(":")[0].length == 1
        ? "0" + time[0].split(":")[0] + ":" + time[0].split(":")[1]
        : time[0];
    var endTimeX = time[1].split(":")[0].length == 1
        ? "0" + time[1].split(":")[0] + ":" + time[1].split(":")[1]
        : time[1];

    startTime = startTimeX;
    endTime = endTimeX;
  }

  String getMobileNumbers() {
    var mobileNumbers = "";
    mobileNumber.mobileForms.forEach((mobileForms) {
      if (mobileForms.controller.text.isNotEmpty)
        mobileNumbers = mobileNumbers +
            "${mobileNumbers.isEmpty ? '' : ','}" +
            "${mobileForms.formWidget.countryCode} " +
            mobileForms.controller.text;
    });
    return mobileNumbers;
  }

  void signUp(BuildContext context) async {
    final progressBloc = BlocProvider.of<ProgressBloc>(context);
    if (validate(context)) {
      progressBloc.showProgress();
      registerRepo
          .editCustomer(
        RegisterHelperEntity(
            providerID: GlobalCustomer.customerEntity.data.id,
            fname: firstNameController.text,
            lname: lastNameController.text,
            email: emailController.text,
            fcmToken: fcmToken ?? "tuyt",
            deviceId: "ghdsfhdgsjkhfh",
            currentDate: DateTime.now().toString(),
            facebookPageId: facebookController.text,
            instagramPageId: instaController.text,
            mobile: getMobileNumbers(),
            countryId: countryValue,
            cityId: cityValue,
            category: categoryValue,
            subCategory: subCategoryValue,
            serviceWorkingDays: "" + startDay + "-" + endDay + "",
            serviceWorkingHour: "" + startTime + "-" + endTime + "",
            address: addressObject == null
                ? GlobalCustomer.customerEntity.data.address
                : addressObject.address,
            lat: addressObject == null
                ? GlobalCustomer.customerEntity.data.lat
                : addressObject.lat,
            long: addressObject == null
                ? GlobalCustomer.customerEntity.data.long
                : addressObject.long,
            profilePic: done != null
                ? await uploadImageAndGetUrl()
                : GlobalCustomer.customerEntity.data.profilePic),
      )
          .then((value) {
        if (value.status) {
          progressBloc.hideProgress();
          toast(context, "Updated succesfully.");
          UserRepo().saveUser(value);
        } else {
          progressBloc.hideProgress();
          toast(context, "Sorry something went wrong.");
        }
      }).catchError((error) {
        progressBloc.hideProgress();
        toast(context, error.toString());
      });
    }
  }

  bool validate(context) {
    final firstNameValidate =
        Validations.validateName(firstNameController.text);
    final facebookLinkValidate =
        Validations.validateNotEmpty(facebookController.text, "Facebook link");
    final instaLinkValidate =
        Validations.validateNotEmpty(instaController.text, "Instagram link");
    final lastNameValidate = Validations.validateName(lastNameController.text);
    final emailNameValidate = Validations.validateEmail(emailController.text);
    if (!facebookLinkValidate.status) {
      toast(context, facebookLinkValidate.msg);
      return false;
    }

    if (!instaLinkValidate.status) {
      toast(context, instaLinkValidate.msg);
      return false;
    }

    if (!firstNameValidate.status) {
      toast(context, firstNameValidate.msg);
      return false;
    }
    if (!lastNameValidate.status) {
      toast(context, lastNameValidate.msg);
      return false;
    }

    if (!emailNameValidate.status) {
      toast(context, emailNameValidate.msg);
      return false;
    }

    if (passwordController.text != confirmPasswordController.text) {
      toast(context, "Password and Confirm password does not match.");
      return false;
    }

    return true;
  }

  List<DropdownMenuItem<String>> buildDropdownMenuItems(List categories) {
    List<DropdownMenuItem<String>> items = List();
    for (String c in categories) {
      items.add(
        DropdownMenuItem(
          value: c,
          child: Text(c),
        ),
      );
    }
    return items;
  }

  Future<String> uploadImageAndGetUrl() async {
    final StorageReference storageRef = FirebaseStorage.instance.ref().child(p
        .basename(Random().nextInt(10000).toString() + p.basename(image.path)));
    final uploadTask = storageRef.putFile(
      image,
      StorageMetadata(
        contentType: "image" + '/' + p.extension(image.path),
      ),
    );
    final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
    return await downloadUrl.ref.getDownloadURL();
  }
}

class ImageWidget extends StatefulWidget {
  final Future<File> Function() onClickCallback;

  ImageWidget(this.onClickCallback);

  @override
  State<StatefulWidget> createState() {
    return ImageWidgetState();
  }
}

File done;

class ImageWidgetState extends State<ImageWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        done = await widget.onClickCallback();
        setState(() {});
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 54.0),
        child: Container(
          constraints: BoxConstraints(maxWidth: 10.0),
          child: (done == null
              ? Image.network(GlobalCustomer.customerEntity.data.profilePic,
                  height: 180.0, width: 180.0)
              : Image.file(image, height: 160.0, width: 160.0)),
        ),
      ),
    );
  }
}
