import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/banner_repo_entity.dart';
import 'package:evento_provider/screens/banners/repo/BannerRepo.dart';
import 'package:evento_provider/screens/banners/widget/Add_Banner.dart';
import 'package:flutter/material.dart';

class BannersScreen extends StatefulWidget {
  final String serviceId;

  BannersScreen(this.serviceId);

  @override
  _BannersScreenState createState() =>
      _BannersScreenState(GlobalCustomer.customerEntity.data.id);
}

class _BannersScreenState extends State<BannersScreen> {
  final String providerId;
  final repo = BannerRepo();

  _BannersScreenState(this.providerId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Add More Banners",
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(00),
                  bottomLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30))),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => AddBanner(widget.serviceId)));
          }),
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30))),
        title: Text("BANNERS"),
        elevation: 6,
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
      ),
      body: FutureBuilder<BannerRepoEntity>(
          future: repo.fetch(widget.serviceId),
          builder: (context, snapshot) {
            return streamConsumer(snapshot, () {
              if (snapshot.data.data != null &&
                  snapshot.data.status &&
                  snapshot.data.data.isNotEmpty) {
                return ListView.builder(
                    itemCount: snapshot.data.data.length,
                    itemBuilder: (context, index) {
                      return bannerItem(snapshot.data.data[index]);
                    });
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/notFound.png"),
                      Text(
                        "No Banners Added.".toUpperCase(),
                        style: TextStyle(
                            color: Colors.blueGrey.withOpacity(0.6),
                            fontSize: 16.0),
                      )
                    ],
                  ),
                );
              }
            });
          }),
    );
  }

  bannerItem(BannerRepoData data) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 180,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(data.bannerImage),
                      fit: BoxFit.cover)),
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomRight,
            child: InkWell(
              onTap: () async {
                repo.remove(data.id, widget.serviceId).then((value) {
                  setState(() {});
                });
              },
              child: Card(
                shape: CircleBorder(),
                child: Icon(Icons.close),
              ),
            ))
      ],
    );
  }
}
