import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/banner_repo_entity.dart';
import 'package:evento_provider/model/common_entity.dart';

class BannerRepo extends BaseRepository {
  Future<BannerRepoEntity> fetch(serviceId) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceBannerImgGet, data: {
      "locale": "en",
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId
    });
    if (apiResponse.status) {
      return BannerRepoEntity.fromJson(apiResponse.response.data);
    } else {
      return BannerRepoEntity(message: apiResponse.msg, data: null);
    }
  }

  Future<CommonEntity> remove(String bannerId, serviceId) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceBannerImgRemove, data: {
      "locale": "en",
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId,
      "banner_id": bannerId
    });
    if (apiResponse.status) {
      return CommonEntity.fromJson(apiResponse.response.data);
    } else {
      return CommonEntity(message: apiResponse.msg,);
    }
  }

  Future<CommonEntity> add(String bannerImage, serviceId) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceBannerImgAdd, data: {
      "locale": "en",
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId,
      "banner_image": bannerImage
    });
    if (apiResponse.status) {
      return CommonEntity.fromJson(apiResponse.response.data);
    } else {
      return CommonEntity(message: apiResponse.msg);
    }
  }
}
