
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/constants/PrefConstant.dart';
import 'package:evento_provider/screens/signIn/SignInScreen.dart';
import 'package:evento_provider/widgets/DotIndicator/page_view_indicator.dart';
import 'package:evento_provider/widgets/DotIndicator/src/circle.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GoThrough extends StatelessWidget {
  final PageController _pageController = PageController(initialPage: 0);
  final List<Widget> _pages = List();
  final homeScreen = SignInScreen();
  final pageIndexNotifier = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    _pages.add(getPage(
        title: "All Services At One Place",
        description: "Services in your city, Includes all the best providers.",
        mainImage: "assets/ic_one.png"));
    _pages.add(getPage(
        title: "Frequent Updates",
        description: "New services and providers added daily.",
        mainImage: "assets/ic_three.png"));
    _pages.add(getPage(
        title: "Chat",
        description: "Chat With Service Provider, Easily connect with the provider of the service.",
        mainImage: "assets/ic_two.png"));

    return Scaffold(
      body: Column(children: <Widget>[
        Flexible(
            child: Container(
                child: PageView(
                    controller: _pageController,
                    onPageChanged: (pos) => pageIndexNotifier.value = pos,
                    children: _pages,
                    reverse: false))),
        Container(
          padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
          height: 48.0,
          width: double.infinity,
          color: AppColors.primaryColor,
          child: Stack(children: <Widget>[
            Center(child: _dotIndicator()),
            Builder(
              builder: (context) => Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    onTap: () async {
                      _pageController.dispose();
                      SharedPreferences prefs = await SharedPreferences
                          .getInstance();
                      await prefs.setBool(PrefConstant.GOTHROUGH, true);
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => homeScreen));
                    },
                    child: Text("SKIP",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0)),
                  )),
            )
          ]),
        )
      ]),
    );
  }

  PageViewIndicator _dotIndicator() {
    return PageViewIndicator(
      pageIndexNotifier: pageIndexNotifier,
      length: _pages.length,
      normalBuilder: (animationController, index) => Circle(
            size: 8.0,
            color: Colors.black87,
          ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
            scale: CurvedAnimation(
              parent: animationController,
              curve: Curves.ease,
            ),
            child: Circle(
              size: 8.0,
              color: Colors.white,
            ),
          ),
    );
  }

  Widget getPage(
      {String title = "WELCOME",
      String description,
      String mainImage = "assets/images/go_bg.png"}) {
    return Container(
      padding: EdgeInsets.only(top: 24.0),
      /*decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("assets/images/go_bg.png"),
              fit: BoxFit.cover))*/
      color: Colors.white,
      child: Column(children: <Widget>[
        Expanded(
            flex: 7,
            child: Image.asset(mainImage, height: 310.0, width: 310.0)),
        Expanded(
            flex: 3,
            child: Center(
                child: Column(
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      fontSize: 26.0,
                      fontWeight: FontWeight.bold,
                      color: AppColors.primaryColor),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    description,
                    style: TextStyle(fontSize: 14.0),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            )))
      ]),
    );
  }
}
