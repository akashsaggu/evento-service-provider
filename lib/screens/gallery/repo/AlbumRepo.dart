import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/album_image_entity.dart';

class AlbumRepo extends BaseRepository {
  Future<AlbumImageEntity> getAlbumImages(
      String providerId, String serviceId, String albumId) async {
    final ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceAlbumImagesGet, data: {
      "provider_id": providerId,
      "service_id": serviceId,
      "album_id": albumId
    });

    if (apiResponse.status) {
      return AlbumImageEntity.fromJson(apiResponse.response.data);
    } else {
      return AlbumImageEntity(status: false);
    }
  }

  Future<AlbumImageEntity> addAlbumImages(String serviceId, String imageTitle,
      String imageLink, String albumId) async {
    final ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceAlbumImageAdd, data: {
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId,
      "image_title": imageTitle,
      "image_name": imageLink,
      "album_id": albumId
    });

    if (apiResponse.status) {
      return AlbumImageEntity.fromJson(apiResponse.response.data);
    } else {
      return AlbumImageEntity(status: false);
    }
  }

  Future<AlbumImageEntity> removeAlbumImages(
      String serviceId, String imageId, String albumId) async {
    final ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceAlbumImageDelete, data: {
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId,
      "album_id": albumId,
      "image_id": imageId
    });

    if (apiResponse.status) {
      return AlbumImageEntity.fromJson(apiResponse.response.data);
    } else {
      return AlbumImageEntity(status: false);
    }
  }
}
