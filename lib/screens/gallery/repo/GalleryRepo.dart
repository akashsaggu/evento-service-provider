import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/gallery_entity.dart';

class GalleryRepo extends BaseRepository {
  Future<GalleryEntity> getAlbums(String providerId, String serviceId) async {
    final ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.ServiceAlbumGet,
        data: {"provider_id": providerId, "service_id": serviceId});

    if (apiResponse.status) {
      return GalleryEntity.fromJson(apiResponse.response.data);
    } else {
      return GalleryEntity(status: false);
    }
  }

  Future<GalleryEntity> addAlbums(String providerId, String serviceId,
      String albumImage, String albumName) async {
    final ApiResponse apiResponse =
    await apiHitter.getPostApiResponse(ApiEndpoint.ServiceAlbumAdd, data: {
      "provider_id": providerId,
      "service_id": serviceId,
      "album_name": albumName,
      "album_image": albumImage
    });

    if (apiResponse.status) {
      return GalleryEntity.fromJson(apiResponse.response.data);
    } else {
      return GalleryEntity(status: false);
    }
  }
  Future<GalleryEntity> removeAlbum(String serviceId, String albumId) async {
    final ApiResponse apiResponse =
    await apiHitter.getPostApiResponse(ApiEndpoint.ServiceAlbumDelete, data: {
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId,
      "album_id": albumId,
    });

    if (apiResponse.status) {
      return GalleryEntity.fromJson(apiResponse.response.data);
    } else {
      return GalleryEntity(status: false);
    }
  }
}
