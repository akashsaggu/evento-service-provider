import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/gallery_entity.dart';
import 'package:evento_provider/screens/gallery/repo/GalleryRepo.dart';
import 'package:evento_provider/screens/gallery/widget/AddAlbum.dart';
import 'package:flutter/material.dart';

import 'AlbumImageScreen.dart';

class GalleryAlbumScreen extends StatefulWidget {
  final String serviceId;

  GalleryAlbumScreen(this.serviceId);

  @override
  _GalleryAlbumScreenState createState() => _GalleryAlbumScreenState(
      GlobalCustomer.customerEntity.data.id, serviceId);
}

class _GalleryAlbumScreenState extends State<GalleryAlbumScreen> {
  final String providerId;
  final String serviceId;
  final repo = GalleryRepo();

  _GalleryAlbumScreenState(this.providerId, this.serviceId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Add More Albums",
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(00),
                  bottomLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30))),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AddAlbum(widget.serviceId)));
          }),
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30))),
        title: Text("ALBUMS"),
        elevation: 6,
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
      ),
      body: FutureBuilder<GalleryEntity>(
          future: repo.getAlbums(providerId, serviceId),
          builder: (context, snapshot) {
            return streamConsumer(snapshot, () {
              if (snapshot.data.data != null &&
                  snapshot.data.status &&
                  snapshot.data.data.isNotEmpty) {
                return Center(
                  child: GridView.builder(
                      itemCount: snapshot.data.data.length,
                      gridDelegate:
                          new SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              childAspectRatio: 0.75,
                              crossAxisSpacing: 8,
                              mainAxisSpacing: 8),
                      itemBuilder: (BuildContext context, int index) {
                        final data = snapshot.data.data[index];
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AlbumImageScreen(
                                        albumName: data.albumName,
                                        albumId: data.id,
                                        providerId: data.providerId,
                                        serviceId: data.serviceId)));
                          },
                          child: Center(child: bannerItem(data)),
                        );
                      }),
                );
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/notFound.png"),
                      Text(
                        "No Albums Added.".toUpperCase(),
                        style: TextStyle(
                            color: Colors.blueGrey.withOpacity(0.6),
                            fontSize: 16.0),
                      )
                    ],
                  ),
                );
              }
            });
          }),
    );
  }

  bannerItem(GalleryData data) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 180,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(color: Colors.black12,
                  image: DecorationImage(
                      image: NetworkImage(data.albumImage), fit: BoxFit.fill)),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 20,
              color: Colors.black54,
              child: Center(
                  child: Text(
                data.albumName,
                style: TextStyle(color: Colors.white),
              )),
            ),
          ),
        ),
        Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () async {
                repo.removeAlbum(widget.serviceId,data.id).then((value) {
                  setState(() {});
                });
              },
              child: Card(
                shape: CircleBorder(),
                child: Icon(Icons.close),
              ),
            ))
      ],
    );
  }
}
