import 'package:cached_network_image/cached_network_image.dart';
import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/model/album_image_entity.dart';
import 'package:evento_provider/screens/gallery/repo/AlbumRepo.dart';
import 'package:evento_provider/screens/gallery/widget/AddAlbumImage.dart';
import 'package:evento_provider/widgets/ImageViewerWidget.dart';
import 'package:flutter/material.dart';

class AlbumImageScreen extends StatelessWidget {
  final String providerId, serviceId, albumId, albumName;

  AlbumImageScreen(
      {this.providerId, this.serviceId, this.albumId, this.albumName});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            tooltip: "Add More Images",
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(00),
                    bottomLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30))),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => AddAlbumImage(serviceId, albumId)));
            }),
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: AppColors.primaryColor,
          title: Text(
            "${albumName.toUpperCase()} IMAGES",
            style: TextStyle(fontFamily: "Roboto"),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.keyboard_arrow_left,
                size: 35.0,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
          centerTitle: true,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(40),
                  bottomRight: Radius.circular(40))),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: AlbumImageListWidget(providerId, serviceId, albumId),
        ));
  }
}
class AlbumImageListWidget extends StatefulWidget{
  final String providerId, serviceId, albumId;
  final repo = AlbumRepo();

  AlbumImageListWidget(this.providerId, this.serviceId, this.albumId);
  @override
  State<StatefulWidget> createState() {
    return _AlbumImageListWidget(providerId,serviceId,albumId);
  }

}
class _AlbumImageListWidget extends State<AlbumImageListWidget> {
  final String providerId, serviceId, albumId;
  final repo = AlbumRepo();

  _AlbumImageListWidget(this.providerId, this.serviceId, this.albumId);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<AlbumImageEntity>(
        future: repo.getAlbumImages(providerId, serviceId, albumId),
        builder: (context, snapshot) {
          return streamConsumer(snapshot, () {
            if (snapshot.data.data != null &&
                snapshot.data.status &&
                snapshot.data.data.isNotEmpty) {
              return Center(
                child: GridView.builder(
                    itemCount: snapshot.data.data.length,
                    gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: 0.75,
                        crossAxisSpacing: 8,
                        mainAxisSpacing: 8),
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ImageViewerWidget(
                                        snapshot.data.data, index)));
                          },
                          child: Center(
                              child: Stack(
                            children: <Widget>[
                              CachedNetworkImage(
                                imageUrl: snapshot.data.data[index].imageName,
                                placeholder: (context, url) => Image.asset(
                                    "assets/ic_placeholder_logo.png",
                                    fit: BoxFit.fill),
                                fit: BoxFit.fill,
                                height: double.infinity,
                                width: double.infinity,
                              ),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  height: 20,
                                  color: Colors.black54,
                                  child: Center(
                                      child: Text(
                                        snapshot.data.data[index].imageTitle,
                                    style: TextStyle(color: Colors.white),
                                  )),
                                ),
                              ),
                              Align(
                                  alignment: Alignment.topRight,
                                  child: InkWell(
                                    onTap: () async {
                                      repo
                                          .removeAlbumImages(snapshot.data.data[index].serviceId,
                                          snapshot.data.data[index].id, snapshot.data.data[index].albumId)
                                          .then((value) {
                                        setState(() {});
                                      });
                                    },
                                    child: Card(
                                      shape: CircleBorder(),
                                      child: Icon(Icons.close),
                                    ),
                                  ))
                            ],
                          )));
                    }),
              );
            } else {
              return Center(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image.asset("assets/notFound.png"),
                  Text(
                    "No Images added.",
                    style: TextStyle(color: Colors.blueGrey, fontSize: 16.0),
                  )
                ],
              ));
            }
          });
        });
  }
}
