import 'dart:io';
import 'dart:math';

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/screens/gallery/repo/AlbumRepo.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:evento_provider/widgets/ProgressWidget.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;

final nameController = TextEditingController();

class AddAlbumImage extends StatelessWidget {
  final String serviceId;
  final String albumId;

  AddAlbumImage(this.serviceId, this.albumId);

  @override
  Widget build(BuildContext context) {
    return ProgressWidgetWrapper(
      child: AddBannerUi(serviceId, albumId),
    );
  }
}

class AddBannerUi extends StatefulWidget {
  final String serviceId;
  final String albumId;

  AddBannerUi(this.serviceId, this.albumId);

  @override
  _AddBannerState createState() => _AddBannerState();
}

class _AddBannerState extends State<AddBannerUi> {
  final repo = AlbumRepo();
  String dropDownValue1 = "Image Link";

  bool image = true, upload = false;

  String imageLink;

  bool visible = false;
  File _image;

  Widget checkBanner() {
    if (image) {
      return _imageContent();
    } else if (upload) {
      return _UploadNewImage();
    }
  }

  @override
  Widget build(BuildContext context) {
    final name = TextFormField(
      controller: nameController,
      style: TextStyle(color: Colors.white),
      autofocus: false,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        filled: true,
        hintText: 'Image Name',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        elevation: 7,
        centerTitle: true,
        title: Text(
          "ADD IMAGE",
          style: TextStyle(color: Colors.white),
        ),
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30))),
      ),
      body: new Theme(
        data: ThemeData(canvasColor: Colors.white),
        child: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: name,
              ),
              Padding(
                padding: const EdgeInsets.all(28.0),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    isExpanded: true,
                    style: TextStyle(
                      color: Colors.blueGrey,
                      fontSize: 25,
                    ),
                    items: ["Image Link", "Upload New"]
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Center(child: Text(value)),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        dropDownValue1 = value;
                        if (value == "image Link") {
                          image = true;
                          upload = false;
                        } else {
                          image = false;
                          upload = true;
                        }
                      });
                    },
                    value: dropDownValue1,
                    elevation: 5,
                  ),
                ),
              ),
              checkBanner(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _imageContent() {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 8, 20, 8),
            child: TextField(
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.ondemand_video),
                fillColor: Colors.white,
                filled: true,
                labelText: "image Link",
                hintText:
                    "E.g https://miro.medium.com/max/1000/1*OSGMyU5VoUrXedAdsWKYew.jpeg",
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueAccent, width: 2),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
              ),
              onChanged: (linkId) {
                imageLink = linkId;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 30, 00, 00),
            child: SizedBox(
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Material(
                elevation: 10,
                shape: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent, width: 0),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        bottomLeft: Radius.circular(30))),
                child: InkWell(
                  onTap: () {
                    if (imageLink != null) {
                      final progressBloc =
                          BlocProvider.of<ProgressBloc>(context);
                      progressBloc.showProgress();
                      repo
                          .addAlbumImages(widget.serviceId, nameController.text,
                              imageLink, widget.albumId)
                          .then((value) {
                        progressBloc.hideProgress();
                        if (value.status) {
                          toast(context, "Added Successfully.");
                          Navigator.pop(context);
                        } else {
                          toast(context, "Sorry something went wrong.");
                        }
                      });
                    }
                  },
                  child: Container(
                    decoration: ShapeDecoration(
                        gradient: LinearGradient(colors: [
                          Colors.blueAccent,
                          Colors.lightBlueAccent,
                          Colors.lightBlue,
                        ]),
                        shape: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.transparent, width: 0),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                bottomLeft: Radius.circular(30)))),
                    child: Center(
                        child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                        Text(
                          "ADD IMAGE",
                          style: TextStyle(color: Colors.white, fontSize: 17),
                        ),
                      ],
                    )),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _UploadNewImage() {
    Future getimage() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image = image;
        visible = _image == null ? false : true;
      });
    }

    Future<String> uploadImageAndGetUrl() async {
      final StorageReference storageRef = FirebaseStorage.instance
          .ref()
          .child("banner_image")
          .child(p.basename(
              Random().nextInt(10000).toString() + p.basename(_image.path)));
      final uploadTask = storageRef.putFile(
        _image,
        StorageMetadata(
          contentType: "image" + '/' + p.extension(_image.path),
        ),
      );
      final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
      return await downloadUrl.ref.getDownloadURL();
    }

    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(00, 30, 20, 00),
            child: SizedBox(
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Material(
                elevation: 10,
                shape: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent, width: 0),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                child: InkWell(
                  onTap: () {
                    getimage();
                  },
                  child: Container(
                    decoration: ShapeDecoration(
                        gradient: LinearGradient(colors: [
                          Colors.blueAccent,
                          Colors.lightBlueAccent,
                        ]),
                        shape: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.transparent, width: 0),
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                bottomRight: Radius.circular(30)))),
                    child: Center(
                        child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Choose image",
                          style: TextStyle(color: Colors.white, fontSize: 17),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.image,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    )),
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: visible,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 30, 00, 00),
              child: SizedBox(
                height: 60,
                width: MediaQuery.of(context).size.width,
                child: Material(
                  elevation: 10,
                  shape: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.transparent, width: 0),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          bottomLeft: Radius.circular(30))),
                  child: InkWell(
                    onTap: () async {
                      final progressBloc =
                          BlocProvider.of<ProgressBloc>(context);
                      progressBloc.showProgress();
                      await uploadImageAndGetUrl().then((value) {
                        repo
                            .addAlbumImages(widget.serviceId,
                                nameController.text, value, widget.albumId)
                            .then((value) {
                          progressBloc.hideProgress();
                          if (value.status) {
                            toast(context, "Added Successfully.");
                            Navigator.pop(context);
                          } else {
                            toast(context, "Sorry something went wrong.");
                          }
                        });
                        ;
                      });
                    },
                    child: Container(
                      decoration: ShapeDecoration(
                          gradient: LinearGradient(colors: [
                            Colors.blueAccent,
                            Colors.lightBlueAccent,
                          ]),
                          shape: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.transparent, width: 0),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  bottomLeft: Radius.circular(30)))),
                      child: Center(
                          child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.file_upload,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            "UPLOAD",
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          ),
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: _image == null ? Text('') : Text('image Found successfully'),
          ),
        ],
      ),
    );
  }
}
