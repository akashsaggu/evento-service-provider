import 'package:evento_provider/model/booking_entity.dart';
import 'package:evento_provider/screens/chat/chat_screen.dart';
import 'package:flutter/material.dart';

class BookingItem extends StatelessWidget {
  BookingData data;

  BookingItem(this.data);

  row(String name, value) => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Text(
              name,
              style: TextStyle(color: Colors.white),
            ),
          ),
          Expanded(
            child: Text(
              ":",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Expanded(
            child: Text(
              value,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      color: Colors.blueAccent,
      elevation: 6,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.white60,
                  radius: 40,
                  backgroundImage: data.userData.profilePic != null
                      ? NetworkImage(data.userData.profilePic)
                      : AssetImage("assets/user.png"),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "${data.userData.fname} ${data.userData.lname}",
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        data.userData.mobile.isNotEmpty
                            ? data.userData.mobile
                            : data.userData.email,
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.white60, fontSize: 16),
                      ),
                    )
                  ],
                ),
                Spacer(),
                SizedBox(
                    height: 67,
                    width: 67,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChatScreen(
                                  name:
                                  "${data.userData.fname} ${data.userData.lname}",
                                  photoUrl: data.userData.profilePic ?? "",
                                  receiverUid: data.userData.id,
                                )));
                      },
                    child: Card(
                      shape: CircleBorder(),
                      elevation: 6,
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.chat_bubble,
                              color: Colors.blueAccent,
                              size: 18,
                            ),
                            Text(
                              "Chat",
                              style: TextStyle(
                                  color: Colors.blueAccent, fontSize: 14),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Divider(
                color: Colors.white,
                height: 2,
              ),
            ),
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: row("Service Name", data.serviceData.serviceTitle)),
            Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 8, 0),
                child: row("Event Name", data.eventData.eventTitle)),
            Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 0),
                child: row("Price", data.totalPrice)),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: row("Event Date", data.bookingDate.substring(0,10)),
            ),
            SizedBox(
              height: 2,
            )
          ],
        ),
      ),
    );
  }
}
