import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/model/booking_entity.dart';
import 'package:evento_provider/screens/Bookings/BookingItem.dart';
import 'package:evento_provider/screens/Bookings/repo/BookingRepo.dart';
import 'package:evento_provider/screens/UserDetailsScreen/UserDetailsScreen.dart';
import 'package:flutter/material.dart';

class BookingScreen extends StatelessWidget {
  final repo = BookingRepo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30))),
        title: Text("BOOKING LIST"),
        elevation: 6,
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
      ),
      body: FutureBuilder<BookingEntity>(
          future: repo.fetch(),
          builder: (context, snapshot) {
            return streamConsumer(snapshot, () {
              if (snapshot.data.data != null &&
                  snapshot.data.status &&
                  snapshot.data.data.isNotEmpty) {
                return ListView.builder(
                    reverse: false,
                    itemCount: snapshot.data.data.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UserDetailsScreen(
                                        snapshot.data.data[index])));
                          },
                          child: BookingItem(snapshot.data.data[index]));
                    });
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/notFound.png"),
                      Text(
                        "No Bookings".toUpperCase(),
                        style: TextStyle(
                            color: Colors.blueGrey.withOpacity(0.6),
                            fontSize: 16.0),
                      )
                    ],
                  ),
                );
              }
            });
          }),
    );
  }
}
