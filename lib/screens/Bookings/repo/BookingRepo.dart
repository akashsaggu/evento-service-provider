
import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/booking_entity.dart';

class BookingRepo extends BaseRepository {

  Future<BookingEntity> fetch() async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.GetProviderBookings,
        data: {"locale": "en", "provider_id": GlobalCustomer.customerEntity.data.id});
    if (apiResponse.status) {
      return BookingEntity.fromJson(apiResponse.response.data);
    } else {
      return BookingEntity(message: apiResponse.msg, data: null);
    }
  }
}