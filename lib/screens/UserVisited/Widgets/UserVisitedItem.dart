import 'package:evento_provider/model/booking_entity.dart';
import 'package:evento_provider/model/user_visited_entity.dart';
import 'package:evento_provider/screens/chat/chat_screen.dart';
import 'package:flutter/material.dart';

class UserVisitedItem extends StatelessWidget {
  UserVisitedData data;

  UserVisitedItem(this.data);

  row(String name, value) => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Text(
              name,
              style: TextStyle(color: Colors.white),
            ),
          ),
          Expanded(
            child: Text(
              ":",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Expanded(
            child: Text(
              value,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(90)),
      color: Colors.blueAccent,
      elevation: 6,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          CircleAvatar(
            backgroundColor: Colors.white60,
            radius: 40,
            backgroundImage: data.userData.profilePic != null
                ? NetworkImage(data.userData.profilePic)
                : AssetImage("assets/user.png"),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: SizedBox(
              height: 80,
              width: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "${data.userData.fname} ${data.userData.lname}",
                    textAlign: TextAlign.start,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
               ],
              ),
            ),
          ),
          SizedBox(
              height: 80,
              width: 80,
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChatScreen(
                            name:
                            "${data.userData.fname} ${data.userData.lname}",
                            photoUrl: data.userData.profilePic ?? "",
                            receiverUid: data.userData.id,
                          )));
                },
              child: Card(
                margin: EdgeInsets.all(0),
                shape: CircleBorder(),
                elevation: 6,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.chat_bubble,
                        color: Colors.blueAccent,
                        size: 18,
                      ),
                      Text(
                        "Chat",
                        style: TextStyle(
                            color: Colors.blueAccent, fontSize: 14),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
