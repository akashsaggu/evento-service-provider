import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/model/user_visited_entity.dart';
import 'package:evento_provider/screens/UserDetailsScreen/UserVisitedDetail.dart';
import 'package:evento_provider/screens/UserVisited/repo/UserVisitedRepo.dart';
import 'package:evento_provider/screens/services/listing/widget/ServiceItems.dart';
import 'package:flutter/material.dart';

import 'Widgets/UserVisitedItem.dart';

class UserVisitedScreen extends StatelessWidget {
  UserVisitedRepo repo = UserVisitedRepo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: InkWell(onTap: ()=>Navigator.pop(context),child: Icon(Icons.arrow_back_ios)),
          title: Text("User Who Have Visited Your Profile"),
          centerTitle: true,
          elevation: 6,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
        ),
        body: FutureBuilder<UserVisitedEntity>(
            future: repo.userVisited(),
            builder: (context, snapshot) {
              return streamConsumer(snapshot, () {
                if (snapshot.data.data != null &&
                    snapshot.data.status &&
                    snapshot.data.data.isNotEmpty) {
                  return ListView.builder(
                      itemCount: snapshot.data.data.length,
                      itemBuilder: (context, index) => InkWell(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (Context) => UserVisitedDetail(snapshot.data.data[index]))),
                          child: UserVisitedItem(snapshot.data.data[index])));
                } else {
                  return Container(child: Center(child: Text("No User Found")));
                }
              });
            }));
  }
}
