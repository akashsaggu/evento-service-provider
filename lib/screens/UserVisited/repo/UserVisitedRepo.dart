import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/user_visited_entity.dart';
import 'package:evento_provider/model/user_called_entity.dart';


class UserVisitedRepo extends BaseRepository{
   Future<UserVisitedEntity> userVisited() async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.GetUserVisitorProviderLogsData, data: {
          "user_type":"provider",
          "id":GlobalCustomer.customerEntity.data.id,
          "request_type":"ProviderScreenVisitor",
    });
    if (apiResponse.status) {
      return UserVisitedEntity.fromJson(apiResponse.response.data);
    } else {
      return UserVisitedEntity(
        message: apiResponse.msg,
      );
    }
  }
   Future<UserCalledEntity> userCalled() async {
     ApiResponse apiResponse = await apiHitter
         .getPostApiResponse(ApiEndpoint.GetUserVisitorProviderLogsData, data: {
       "user_type":"provider",
       "id":GlobalCustomer.customerEntity.data.id,
       "request_type":"ProviderCallerUser",
     });
     if (apiResponse.status) {
       return UserCalledEntity.fromJson(apiResponse.response.data);
     } else {
       return UserCalledEntity(
         message: apiResponse.msg,
       );
     }
   }
}
