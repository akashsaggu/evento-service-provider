import 'dart:io';
import 'dart:math';

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/screens/videos/repo/VideoRepo.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:evento_provider/widgets/ProgressWidget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:toast/toast.dart';

class AddVideo extends StatelessWidget {
  final String serviceId;

  AddVideo(this.serviceId);
  @override
  Widget build(BuildContext context) {
    return ProgressWidgetWrapper(
      child: AddVideoUi(serviceId),
    );
  }
}

class AddVideoUi extends StatefulWidget {
  final String serviceId;

  AddVideoUi(this.serviceId);
  @override
  _AddVideoState createState() => _AddVideoState();
}

class _AddVideoState extends State<AddVideoUi> {
  final repo = VideoRepo();

  String dropDownValue = "Youtube Link";

  bool youtube = true, upload = false;

  String youtubeLink;

  bool visible = false;
  File _videos;

  Widget check() {
    if (youtube) {
      return _YoutubeContent();
    } else if (upload) {
      return _Uploadnew();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        elevation: 7,
        centerTitle: true,
        title: Text(
          "Add Video",
          style: TextStyle(color: Colors.white),
        ),
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30))),
      ),
      body: new Theme(
        data: ThemeData(canvasColor: Colors.white),
        child: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(28.0),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    isExpanded: true,
                    style: TextStyle(
                      color: Colors.blueGrey,
                      fontSize: 25,
                    ),
                    items: ["Youtube Link", "Upload New"]
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Center(child: Text(value)),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        dropDownValue = value;
                        if (value == "Youtube Link") {
                          youtube = true;
                          upload = false;
                        } else {
                          youtube = false;
                          upload = true;
                        }
                      });
                    },
                    value: dropDownValue,
                    elevation: 5,
                  ),
                ),
              ),
              check(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _YoutubeContent() {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 8, 20, 8),
            child: TextField(
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.ondemand_video),
                fillColor: Colors.white,
                filled: true,
                labelText: "YouTube Video ID",
                hintText: "E.g 9xwazD5SyVg",
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueAccent, width: 2),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
              ),
              onChanged: (linkId) {
                youtubeLink = linkId;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 30, 00, 00),
            child: SizedBox(
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Material(
                elevation: 10,
                shape: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent, width: 0),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        bottomLeft: Radius.circular(30))),
                child: InkWell(
                  onTap: () async {
                    if (youtubeLink != null) {
                      ProgressBloc bloc =
                          BlocProvider.of<ProgressBloc>(context);
                      bloc.showProgress();
                      repo.add(youtubeLink, "youtube_link", "a", widget.serviceId ).then((value) {
                        bloc.hideProgress();
                        if (value.status) {
                          toast(context, "Video added successfuly");
                          Navigator.pop(context);
                        } else {
                          toast(context, "Sorry Something went wrong.");
                        }
                      });
                    }
                  },
                  child: Container(
                    decoration: ShapeDecoration(
                        gradient: LinearGradient(colors: [
                          Colors.blueAccent,
                          Colors.lightBlueAccent,
                          Colors.lightBlue,
                        ]),
                        shape: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.transparent, width: 0),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                bottomLeft: Radius.circular(30)))),
                    child: Center(
                        child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                        Text(
                          "ADD VIDEO",
                          style: TextStyle(color: Colors.white, fontSize: 17),
                        ),
                      ],
                    )),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _Uploadnew() {
    Future getVideo() async {
      var video = await FilePicker.getFile(type: FileType.VIDEO);
      setState(() {
        _videos = video;
        visible = _videos == null ? false : true;
      });
    }

    Future<String> uploadImageAndGetUrl() async {
      final StorageReference storageRef = FirebaseStorage.instance.ref().child("banner_video").child(
          p.basename(
              Random().nextInt(10000).toString() + p.basename(_videos.path)));
      final uploadTask = storageRef.putFile(
        _videos,
        StorageMetadata(
          contentType: "video" + '/' + p.extension(_videos.path),
        ),
      );
      final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
      return await downloadUrl.ref.getDownloadURL();
    }

    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(00, 30, 20, 00),
            child: SizedBox(
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Material(
                elevation: 10,
                shape: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent, width: 0),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                child: InkWell(
                  onTap: () {
                    getVideo();
                  },
                  child: Container(
                    decoration: ShapeDecoration(
                        gradient: LinearGradient(colors: [
                          Colors.blueAccent,
                          Colors.lightBlueAccent,
                        ]),
                        shape: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.transparent, width: 0),
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                bottomRight: Radius.circular(30)))),
                    child: Center(
                        child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Choose Video",
                          style: TextStyle(color: Colors.white, fontSize: 17),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.video_library,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    )),
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: visible,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 30, 00, 00),
              child: SizedBox(
                height: 60,
                width: MediaQuery.of(context).size.width,
                child: Material(
                  elevation: 10,
                  shape: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.transparent, width: 0),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          bottomLeft: Radius.circular(30))),
                  child: InkWell(
                    onTap: () async {
                      ProgressBloc bloc =
                          BlocProvider.of<ProgressBloc>(context);
                      bloc.showProgress();
                      await uploadImageAndGetUrl().then((value) {
                        repo.add(value, "simple_video", "a", widget.serviceId).then((value) {
                          bloc.hideProgress();
                          if (value.status) {
                            Navigator.pop(context);
                            Toast.show("Video added successfully.", context,
                                duration: Toast.LENGTH_LONG);
                          } else {
                            Toast.show("Sorry something went wrong.", context,
                                duration: Toast.LENGTH_LONG);
                          }
                        });
                      });
                    },
                    child: Container(
                      decoration: ShapeDecoration(
                          gradient: LinearGradient(colors: [
                            Colors.blueAccent,
                            Colors.lightBlueAccent,
                          ]),
                          shape: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.transparent, width: 0),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  bottomLeft: Radius.circular(30)))),
                      child: Center(
                          child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.file_upload,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            "UPLOAD",
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          ),
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(50.0),
            child:
                _videos == null ? Text('') : Text('Video Found successfully'),
          ),
        ],
      ),
    );
  }
}
