import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/video_repo_entity.dart';

class VideoRepo extends BaseRepository {
  Future<VideoRepoEntity> fetch(serviceId) async {
    ApiResponse apiResponse =
        await apiHitter.getPostApiResponse(ApiEndpoint.ServiceVideoGet, data: {
      "locale": "en",
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId
    });
    if (apiResponse.status) {
      return VideoRepoEntity.fromJson(apiResponse.response.data);
    } else {
      return VideoRepoEntity(message: apiResponse.msg, data: null);
    }
  }

  Future<VideoRepoEntity> remove(String videoId, serviceId) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceVideoRemove, data: {
      "locale": "en",
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "video_id": videoId,
      "service_id": serviceId
    });

    if (apiResponse.status) {
      return VideoRepoEntity.fromJson(apiResponse.response.data);
    } else {
      return VideoRepoEntity(message: apiResponse.msg, data: null);
    }
  }

  Future<VideoRepoEntity> add(videoLink, videoType, thumbnail, serviceId) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServiceVideoUpload, data: {
      "locale": "en",
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "video_link": videoLink,
      "video_type": videoType,
      "video_thumbnail": thumbnail,
    "service_id": serviceId
    });
    if (apiResponse.status) {
      return VideoRepoEntity.fromJson(apiResponse.response.data);
    } else {
      return VideoRepoEntity(message: apiResponse.msg, data: null);
    }
  }
}
