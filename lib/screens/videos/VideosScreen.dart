import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/video_repo_entity.dart';
import 'package:evento_provider/playVideo/PlayVideo.dart';
import 'package:evento_provider/screens/videos/repo/VideoRepo.dart';
import 'package:evento_provider/screens/videos/widget/AddVideoScreen.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/material.dart';

class VideosScreen extends StatefulWidget {
  final String serviceId;

  VideosScreen(this.serviceId);
  @override
  _VideosScreenState createState() =>
      _VideosScreenState(GlobalCustomer.customerEntity.data.id);
}

class _VideosScreenState extends State<VideosScreen> {
  final repo = VideoRepo();
  final String providerId;

  _VideosScreenState(this.providerId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        centerTitle: true,
        elevation: 5,
        title: Text("VIDEOS"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30))),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Add More Videos",
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(00),
                  bottomLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30))),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => AddVideo(widget.serviceId)));
          }),
      body: FutureBuilder<VideoRepoEntity>(
          future: repo.fetch(widget.serviceId),
          builder: (context, snapshot) {
            return streamConsumer(snapshot, () {
              if (snapshot.data.data != null &&
                  snapshot.data.status &&
                  snapshot.data.data.isNotEmpty) {
                return ListView.builder(
                    itemCount: snapshot.data.data.length,
                    itemBuilder: (context, index) {
                      return VideoItemWidget(snapshot.data.data[index],
                          (data) async {
                          repo.remove(data.id, widget.serviceId).then((value) {
                            setState(() {});
                          });
                      });
                    });
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/notFound.png"),
                      Text(
                        "No Videos Added By Provider.".toUpperCase(),
                        style: TextStyle(
                            color: Colors.blueGrey.withOpacity(0.6),
                            fontSize: 16.0),
                      )
                    ],
                  ),
                );
              }
            });
          }),
    );
  }
}

class VideoItemWidget extends StatefulWidget {
  final VideoRepoData data;
  final ValueChanged<VideoRepoData> callback;

  VideoItemWidget(this.data, this.callback);

  @override
  State<StatefulWidget> createState() {
    return VideoItemWidgetState();
  }
}

class VideoItemWidgetState extends State<VideoItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.only(top: 8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              children: [
                Material(
                  elevation: 5,
                  child: Container(
                    height: 180,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(getVideoThumbnail()),
                            fit: BoxFit.cover)),
                  ),
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: InkWell(
                      onTap: () {
                        widget.callback(widget.data);
                      },
                      child: Card(
                        shape: CircleBorder(),
                        child: Icon(Icons.close),
                      ),
                    )),
                Positioned(
                  top: 60,
                  left: (screenWidth(context) * 0.50) - 30,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (conetxt) => PlayVideo(
                                  widget.data.videoLink,
                                  widget.data.videoType)));
                    },
                    child: SizedBox(
                      height: 60,
                      width: 60,
                      child: Card(
                        child: Icon(
                          Icons.play_arrow,
                          size: 30,
                          color: Colors.white,
                        ),
                        shape: CircleBorder(),
                        elevation: 5,
                        color: Colors.blueAccent,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  String getVideoThumbnail() {
    if (widget.data.videoType == "youtube_link") {
      return "https://img.youtube.com/vi/${widget.data.videoLink}/sddefault.jpg";
    } else {
      return widget.data.videoThumbnail;
    }
  }
}
