import 'package:evento_provider/model/booking_entity.dart';
import 'package:evento_provider/screens/chat/chat_screen.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class UserDetailsScreen extends StatelessWidget {
  BookingData data;

  UserDetailsScreen(this.data);

  itemRow({context, name, value}) => SizedBox(
        width: screenWidth(context)-50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only( top: 8),
                child: Text(
                  name,
                  style: TextStyle(
                      color: Colors.lightBlue,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 8,left: 10,right: 10),
                child: Text(
                  ":",
                  style: TextStyle(
                      color: Colors.lightBlue,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only( top: 8),
                child: Text(
                  value,
                  style: TextStyle(
                      color: Colors.lightBlue,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.lightBlueAccent, Colors.blue[900]])),
          width: double.infinity,
          height: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(
                height: 60,
                width: screenWidth(context),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(30)),
                      color: Colors.white),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.lightBlueAccent,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: Text(
                            "${data.userData.fname.toUpperCase()} DETAILS",
                            style: TextStyle(
                                color: Colors.lightBlue,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.transparent,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Card(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Transform.translate(
                            offset: Offset(00, -55),
                            child: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              radius: 60,
                              backgroundImage: data.userData.profilePic == null
                                  ? AssetImage("assets/user.png")
                                  : NetworkImage(data.userData.profilePic),
                            ),
                          ),
                          Transform.translate(
                            offset: Offset(00, -45),
                            child: Text(
                              "${data.userData.fname.toUpperCase()} ${data.userData.lname.toUpperCase()}",
                              style: TextStyle(
                                  color: Colors.lightBlue,
                                  fontSize: 25,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Transform.translate(
                            offset: Offset(00, -40),
                            child: Text(
                              data.userData.mobile.isNotEmpty
                                  ? data.userData.mobile
                                  : data.userData.email,
                              style: TextStyle(
                                  color: Colors.lightBlue[200],
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Transform.translate(
                            offset: Offset(00, -30),
                            child: SizedBox(
                              height: 67,
                              width: 67,
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ChatScreen(
                                            name:
                                            "${data.userData.fname} ${data.userData.lname}",
                                            photoUrl: data.userData.profilePic ?? "",
                                            receiverUid: data.userData.id,
                                          )));
                                },
                                child: Card(
                                  shape: CircleBorder(),
                                  elevation: 6,
                                  color: Colors.lightBlue,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.chat_bubble,
                                          color: Colors.white,
                                          size: 18,
                                        ),
                                        Text(
                                          "Chat",
                                          style: TextStyle(
                                              color: Colors.white, fontSize: 14),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Transform.translate(
                            offset: Offset(00, -15),
                            child: SizedBox(
                              height: 10,
                              width: screenWidth(context) - 50,
                              child: Divider(
                                color: Colors.lightBlue,
                                height: 9,
                                indent: 3,
                              ),
                            ),
                          ),

                          itemRow(
                            context: context,
                            name:"Booking Date",
                            value: data.bookingDate.substring(0,10),
                          ),
                          itemRow(context: context,name: "Event Type",value: data.eventTypeData.eventName),
                          itemRow(
                              name: "Event Name",
                              context: context,
                              value: data.eventData.eventTitle),
                          itemRow(
                              name: "Service Name",
                              context: context,
                              value: data.serviceData.serviceTitle),
                          itemRow(
                            context: context,
                            name:"Price",
                            value: data.totalPrice,
                          ),
                        ],
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(0, 90, 0, 0),
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(20))),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
