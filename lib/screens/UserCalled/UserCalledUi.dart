import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/model/user_called_entity.dart';
import 'package:evento_provider/screens/UserVisited/Widgets/UserVisitedItem.dart';
import 'package:evento_provider/screens/UserVisited/repo/UserVisitedRepo.dart';
import 'package:flutter/material.dart';

import 'UserCalledDetail.dart';
import 'UserCalledItem.dart';

class UserCalledUi extends StatelessWidget {
  UserVisitedRepo repo = UserVisitedRepo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back_ios)),
          title: Text("User Who Have Called You"),
          centerTitle: true,
          elevation: 6,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
        ),
        body: FutureBuilder<UserCalledEntity>(
            future: repo.userCalled(),
            builder: (context, snapshot) {
              return streamConsumer(snapshot, () {
                if (snapshot.data.data != null &&
                    snapshot.data.status &&
                    snapshot.data.data.isNotEmpty) {
                  return ListView.builder(
                      itemCount: snapshot.data.data.length,
                      itemBuilder: (context, index) => InkWell(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (Context) => UserCalledDetails(snapshot.data.data[index]))),
                          child: UserCalledItem(snapshot.data.data[index])));
                } else {
                  return Container(child: Center(child: Text("No User Found")));
                }
              });
            }));
  }
}
