import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/model/packages_entity.dart';
import 'package:evento_provider/screens/Packages/AddPackages.dart';
import 'package:evento_provider/screens/Packages/repo/PackagesRepo.dart';
import 'package:flutter/material.dart';

class PackagesScreen extends StatefulWidget {
  final String serviceId;

  PackagesScreen(this.serviceId);

  @override
  _PackagesScreenState createState() => _PackagesScreenState(serviceId);
}

class _PackagesScreenState extends State<PackagesScreen> {
  final String serviceId;
  final repo = PackagesRepo();

  _PackagesScreenState(this.serviceId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Add More Packages",
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(00),
                  bottomLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30))),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AddPackages(widget.serviceId)));
          }),
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30))),
        title: Text("PACKAGES"),
        elevation: 6,
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
      ),
      body: FutureBuilder<PackagesEntity>(
          future: repo.getPackages(serviceId),
          builder: (context, snapshot) {
            return streamConsumer(snapshot, () {
              if (snapshot.data.data != null &&
                  snapshot.data.status &&
                  snapshot.data.data.isNotEmpty) {
                return Center(
                  child: ListView.builder(
                      itemCount: snapshot.data.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        final data = snapshot.data.data[index];
                        return InkWell(
                          onTap: () {
                          /*  Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AlbumImageScreen(
                                        albumName: data.albumName,
                                        albumId: data.id,
                                        providerId: data.providerId,
                                        serviceId: data.serviceId)));*/
                          },
                          child: Center(child: bannerItem(data)),
                        );
                      }),
                );
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/notFound.png"),
                      Text(
                        "No Packages Added.".toUpperCase(),
                        style: TextStyle(
                            color: Colors.blueGrey.withOpacity(0.6),
                            fontSize: 16.0),
                      )
                    ],
                  ),
                );
              }
            });
          }),
    );
  }

  bannerItem(PackagesData data) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Material(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
              elevation: 6,
              child: Container(
                decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(data.packageName.toUpperCase(),style:TextStyle(color: Colors.blue,fontSize: 18,fontWeight: FontWeight.w600) ,),
                    Text(data.packageDescription,style: TextStyle(color: Colors.blue,fontWeight: FontWeight.w400),),
                    Text(data.packagePrice,style: TextStyle(color: Colors.blue,fontWeight: FontWeight.w400),),
                  ],
                ),
              ),
            ),
          ),
        ),
        Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () async {
                repo.removePackage(widget.serviceId,data.id).then((value) {
                  setState(() {});
                });
              },
              child: Card(
                shape: CircleBorder(),
                child: Icon(Icons.close),
              ),
            ))
      ],
    );
  }
}
