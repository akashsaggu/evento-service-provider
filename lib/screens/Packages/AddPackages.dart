import 'package:evento_provider/screens/Packages/repo/PackagesRepo.dart';
import 'package:flutter/material.dart';

class AddPackages extends StatefulWidget {
  final String id;

  AddPackages(this.id);

  @override
  _AddPackagesState createState() => _AddPackagesState(id);
}

class _AddPackagesState extends State<AddPackages> {
  var id;

  String price;

  String desc;

  String name;

  _AddPackagesState(this.id);

  final descriptionTextController = TextEditingController();
  final nameTextController = TextEditingController();
  final priceTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var repo = PackagesRepo();
    return Scaffold(
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30))),
        title: Text("ADD PACKAGES"),
        elevation: 6,
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: nameTextController,
                decoration: InputDecoration(
                    labelText: "Package Name",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: descriptionTextController,
                decoration: InputDecoration(
                    labelText: "Package Description",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: priceTextController,
                decoration: InputDecoration(
                    labelText: "Package Price",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                color: Colors.blue,
                child: Text(
                  "Add Package",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w600, color: Colors.white),
                ),
                onPressed: () async {
                  repo
                      .addPackages(
                          widget.id,
                          nameTextController.text,
                          descriptionTextController.text,
                          priceTextController.text)
                      .then((value) {
                    if (value.status) {
                      Navigator.pop(context);
                      setState(() {});
                    }
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
