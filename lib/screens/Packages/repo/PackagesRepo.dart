import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/packages_entity.dart';

class PackagesRepo extends BaseRepository {
  Future<PackagesEntity> getPackages(String serviceId) async {
    final ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.servicePackageGet, data: {
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId
    });

    if (apiResponse.status) {
      return PackagesEntity.fromJson(apiResponse.response.data);
    } else {
      return PackagesEntity(status: false);
    }
  }
  Future<PackagesEntity> removePackage(String serviceId,String packagesId) async {
    final ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ServicePackageDelete, data: {
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId,
      "package_id" : packagesId
    });

    if (apiResponse.status) {
      return PackagesEntity.fromJson(apiResponse.response.data);
    } else {
      return PackagesEntity(status: false);
    }
  }
  Future<PackagesEntity> addPackages(String serviceId,String packageName,String packageDesc,String packagePrice) async {
    final ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.servicePackageAdd, data: {
      "provider_id": GlobalCustomer.customerEntity.data.id,
      "service_id": serviceId,
      "package_name" : packageName,
      "package_description":packageDesc,
      "package_price":packagePrice,
    });

    if (apiResponse.status) {
      return PackagesEntity.fromJson(apiResponse.response.data);
    } else {
      return PackagesEntity(status: false);
    }
  }
}
