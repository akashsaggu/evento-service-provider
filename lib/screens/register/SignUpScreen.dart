import 'dart:io';
import 'dart:math';

import 'package:evento_provider/address/MapWidget.dart';
import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/bloc/CategoryBloc.dart';
import 'package:evento_provider/model/register_helper_entity.dart';
import 'package:evento_provider/screens/register/repo/RegisterRepo.dart';
import 'package:evento_provider/services/validations.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:evento_provider/widgets/CategorySpinner.dart';
import 'package:evento_provider/widgets/CitySpinner.dart';
import 'package:evento_provider/widgets/CountrySpinner.dart';
import 'package:evento_provider/widgets/DaysBottomSheet.dart';
import 'package:evento_provider/widgets/MultipleMobileNo.dart';
import 'package:evento_provider/widgets/ProgressWidget.dart';
import 'package:evento_provider/widgets/SubCategorySpinner.dart';
import 'package:evento_provider/widgets/TimeBottomSheet.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;

final firstNameController = TextEditingController();
final lastNameController = TextEditingController();
final phoneController = TextEditingController();
final emailController = TextEditingController();
final passwordController = TextEditingController();
final confirmPasswordController = TextEditingController();
final addressTextController = TextEditingController();
final addressController = TextEditingController();
final tagLineController = TextEditingController();
final instaController = TextEditingController();
final facebookController = TextEditingController();

var categoryValue = "";
var subCategoryValue = "";
var countryValue = "";
var cityValue = "";
var startDay = "Select Start Day";
var endDay = "Select End Time";
var startTime = "Select Start Time";
var endTime = "Select End Time";
File image;

String fcmToken = "dsfdg";
String gender = "Male";
String dob = "";
String lastAddress = "";
AddressHelper addressObject = AddressHelper();

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProgressWidgetWrapper(child: SignUpScreenUi());
  }
}

class SignUpScreenUi extends StatelessWidget {
  final registerRepo = RegisterRepo();
  final mobile_number = MultipleMobileNoWidget();

  @override
  Widget build(BuildContext context) {
    final categoryBloc = CategoryBloc();
    categoryBloc.getCategories();
    categoryBloc.getCountries("en");

    final email = TextFormField(
      controller: emailController,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'Email',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    final firstName = TextFormField(
      controller: firstNameController,
      style: TextStyle(color: Colors.white),
      autofocus: false,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'Full name',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );
    final lastName = TextFormField(
      controller: lastNameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'Last name',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    /* final mobileNumber = TextFormField(
      controller: phoneController,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.number,
      autofocus: false,
      decoration: InputDecoration(
        prefix: CountryPickerWidget(),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'Mobile number',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );*/

    final password = TextFormField(
      controller: passwordController,
      style: TextStyle(color: Colors.white),
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        hintText: 'Password',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    final confirmPassword = TextFormField(
      controller: confirmPasswordController,
      style: TextStyle(color: Colors.white),
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        hintText: 'Confirm Password',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    final signUpButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          signUp(context);
        },
        padding: EdgeInsets.all(12),
        color: Colors.blueAccent,
        child: Text('Sign up', style: TextStyle(color: Colors.white)),
      ),
    );

    /* final tagline = TextFormField(
      controller: tagLineController,
      style: TextStyle(color: Colors.white),
      autofocus: false,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'TagLine',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );
*/
    final facebookLink = TextFormField(
      controller: facebookController,
      style: TextStyle(color: Colors.white),
      autofocus: false,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'Facebook ID',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    final instaLink = TextFormField(
      controller: instaController,
      style: TextStyle(color: Colors.white),
      autofocus: false,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'Instagram ID',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    final alreadyAccountLabel = Center(
        child: InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: RichText(
        text: TextSpan(
          // Note: Styles for TextSpans must be explicitly defined.
          // Child text spans will inherit styles from parent
          style: TextStyle(
            fontSize: 14.0,
            color: Colors.white,
          ),
          children: <TextSpan>[
            TextSpan(text: "Already have an account? "),
            TextSpan(
                text: 'Log in',
                style: new TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.blueAccent)),
          ],
        ),
      ),
    ));
    final category = CategorySpinner(
      (value) {
        categoryValue = value.id;
      },
    );

    final subCategory = SubCategorySpinner(
      (value) {
        subCategoryValue = value.id;
      },
    );

    final country = CountrySpinner(
      (value) {
        countryValue = value.id;
      },
    );

    final city = CitySpinner(
      (value) {
        cityValue = value.id;
      },
    );

    final categoryLabel = Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("Select Category", style: TextStyle(color: Colors.white)));

    final countryLabel = Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("Select Country", style: TextStyle(color: Colors.white)));

    final cityLabel = Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("Select City", style: TextStyle(color: Colors.white)));

    final subCategoryLabel = Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8.0),
        child:
            Text("Select SubCategory", style: TextStyle(color: Colors.white)));

    final selectStartDay = DaysBottomSheet((value) {
      startDay = value;
    }, "Select Start day");

    final selectEndDay = DaysBottomSheet((value) {
      endDay = value;
    }, "Select end day");

    final time = TimeBottomSheet((value) {
      startTime = value;
    }, "Select Start time");

    final EndTime = TimeBottomSheet((value) {
      endTime = value;
    }, "Select End time");

    final profile = ImageWidget(() async {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
      return image;
    });

    return WillPopScope(
      onWillPop: () async {
        clearFields(context);
        return true;
      },
      child: BlocProvider<CategoryBloc>(
        bloc: categoryBloc,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Colors.blue[900], Colors.blueAccent],
                        begin: Alignment.bottomRight,
                        end: Alignment.topCenter)),
                child: Center(
                  child: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.only(left: 24.0, right: 24.0),
                    children: <Widget>[
                      SizedBox(height: 8.0),
                      profile,
                      SizedBox(height: 24.0),
                      firstName,
                      SizedBox(height: 8.0),
                      email,
                      SizedBox(height: 8.0),
                      mobile_number,
                      SizedBox(height: 8.0),
                      password,
                      SizedBox(height: 8.0),
                      confirmPassword,
                      SizedBox(height: 4.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: countryLabel),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: cityLabel),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: country),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: city),
                        ],
                      ),
                      SizedBox(height: 8.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: category),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: subCategory),
                        ],
                      ),
                      /*      */ /*SizedBox(height: 8.0),
                      tagline,*/ /*
                      SizedBox(height: 8.0),
                      facebookLink,
                      SizedBox(height: 8.0),
                      instaLink,
                      SizedBox(height: 8.0),
                      AddresWidget((value) {
                        addressObject = value;
                      }),
                      */ /* SizedBox(height: 8.0),
                      DateBottomSheet((value) {
                        dob = value;
                      }, "Select DOB"),*/ /*
                      SizedBox(height: 8.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: time),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: EndTime),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: selectStartDay),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: selectEndDay),
                        ],
                      ),
                      SizedBox(height: 4.0),
                      */ /* GenderSelection((value) {
                        gender = value;
                      }),*/ /*
                     
                      ),*/
                      SizedBox(height: 8.0),
                      signUpButton,
                      SizedBox(height: 16.0),
                      alreadyAccountLabel,
                      SizedBox(height: 16.0)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void clearFields(context) {
    firstNameController.clear();
    lastNameController.clear();
    phoneController.clear();
    emailController.clear();
    passwordController.clear();
    confirmPasswordController.clear();
    addressTextController.clear();
    addressController.clear();
    tagLineController.clear();
    instaController.clear();
    facebookController.clear();

    categoryValue = "";
    subCategoryValue = "";
    countryValue = "";
    cityValue = "";
    startDay = "Select Start Day";
    endDay = "Select End Time";
    startTime = "Select Start Time";
    endTime = "Select End Time";
    image = null;

    fcmToken = "dsfdg";
    gender = "Male";
    dob = "";
    lastAddress = "";
    addressObject = null;
  }

  void signUp(BuildContext context) async {
    final progressBloc = BlocProvider.of<ProgressBloc>(context);
    if (validate(context) && image != null) {
      progressBloc.showProgress();
      registerRepo
          .doRegisterCustomer(RegisterHelperEntity(
              fname: firstNameController.text,
              lname: lastNameController.text,
              email: emailController.text,
              password: passwordController.text,
              deviceId: "ghdsfhdgsjkhfh",
              fcmToken: fcmToken ?? "tuyt",
              mobile: getMobileNumbers(),
              countryId: countryValue,
              cityId: cityValue,
              category: categoryValue,
              subCategory: subCategoryValue,
              /*instagramPageId: instaController.text,
        facebookPageId: facebookController.text,
        
        address: addressObject.address,
        lat: addressObject.lat,
        long: addressObject.long,
        serviceWorkingDays: "$startDay - $endDay",
        serviceWorkingHour: "$startTime - $endTime",*/
              profilePic: await uploadImageAndGetUrl()))
          .then((value) {
        if (value.status) {
          progressBloc.hideProgress();
          toast(context, "Register succesfully.");
          clearFields(context);
          Navigator.pop(context);
        } else {
          progressBloc.hideProgress();
          toast(context, "Sorry something went wrong.");
        }
      }).catchError((error) {
        progressBloc.hideProgress();
        toast(context, error.toString());
      });
    } else {
      toast(context, "Please Check All the fields");
    }
  }

  String getMobileNumbers() {
    var mobileNumbers = "";
    mobile_number.mobileForms.forEach((mobileForms) {
      if (mobileForms.controller.text.isNotEmpty)
        mobileNumbers = mobileNumbers +
            "${mobileNumbers.isEmpty ? '' : ','}" +
            "${mobileForms.formWidget.countryCode} " +
            mobileForms.controller.text;
    });
    return mobileNumbers;
  }

  bool validate(context) {
    final firstNameValidate =
        Validations.validateName(firstNameController.text);
    /* final facebookLinkValidate =
        Validations.validateNotEmpty(facebookController.text, "Facebook link");
    final instaLinkValidate =
        Validations.validateNotEmpty(instaController.text, "Instagram link");*/
    //final lastNameValidate = Validations.validateName(lastNameController.text);
    final emailNameValidate = Validations.validateEmail(emailController.text);
    final passwordValidate =
        Validations.validatePassword(passwordController.text);
    final confirmPasswordValidate =
        Validations.validatePassword(confirmPasswordController.text);

    /*if (!facebookLinkValidate.status) {
      toast(context, facebookLinkValidate.msg);
      return false;
    }

    if (!instaLinkValidate.status) {
      toast(context, instaLinkValidate.msg);
      return false;
    }*/

    if (!firstNameValidate.status) {
      toast(context, firstNameValidate.msg);
      return false;
    }
//    if (!lastNameValidate.status) {
//      toast(context, lastNameValidate.msg);
//      return false;
//    }

    if (!emailNameValidate.status) {
      toast(context, emailNameValidate.msg);
      return false;
    }

    if (!passwordValidate.status) {
      toast(context, passwordValidate.msg);
      return false;
    }
    if (!confirmPasswordValidate.status) {
      toast(context, confirmPasswordValidate.msg);
      return false;
    }

    if (passwordController.text != confirmPasswordController.text) {
      toast(context, "Password and Confirm password does not match.");
      return false;
    }

    return true;
  }

  List<DropdownMenuItem<String>> buildDropdownMenuItems(List categories) {
    List<DropdownMenuItem<String>> items = List();
    for (String c in categories) {
      items.add(
        DropdownMenuItem(
          value: c,
          child: Text(c),
        ),
      );
    }
    return items;
  }

  Future<String> uploadImageAndGetUrl() async {
    final StorageReference storageRef = FirebaseStorage.instance.ref().child(p
        .basename(Random().nextInt(10000).toString() + p.basename(image.path)));
    final uploadTask = storageRef.putFile(
      image,
      StorageMetadata(
        contentType: "image" + '/' + p.extension(image.path),
      ),
    );
    final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
    return await downloadUrl.ref.getDownloadURL();
  }
}

class ImageWidget extends StatefulWidget {
  final Future<File> Function() onClickCallback;

  ImageWidget(this.onClickCallback);

  @override
  State<StatefulWidget> createState() {
    return ImageWidgetState();
  }
}

File done;

class ImageWidgetState extends State<ImageWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        done = await widget.onClickCallback();
        setState(() {});
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          done == null
              ? Icon(
                  Icons.camera_alt,
                  size: 180,
                  color: Colors.white,
                )
              : CircleAvatar(
                  backgroundImage: FileImage(done),
                  radius: 70,
                ),
        ],
      ),
    );
  }
}
