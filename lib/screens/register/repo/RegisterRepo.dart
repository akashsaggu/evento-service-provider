import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/model/category_entity.dart';
import 'package:evento_provider/model/city_entity.dart';
import 'package:evento_provider/model/customer_entity.dart';
import 'package:evento_provider/model/country_entity.dart';
import 'package:evento_provider/model/register_helper_entity.dart';
import 'package:evento_provider/model/sub_category_entity.dart';

class RegisterRepo extends BaseRepository {

  Future<CustomerEntity> doRegisterCustomer(RegisterHelperEntity data) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.ProviderSignUP, data: data.toJson());

    if (apiResponse.status) {
      return CustomerEntity.fromJson(apiResponse.response.data);
    } else {
      return CustomerEntity(message: apiResponse.msg, data: null);
    }
  }
  Future<CustomerEntity> editCustomer(RegisterHelperEntity data) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.UpdateServiceProvider, data: data.toJson());

    if (apiResponse.status) {
      return CustomerEntity.fromJson(apiResponse.response.data);
    } else {
      return CustomerEntity(message: apiResponse.msg, data: null);
    }
  }

  Future<CategoryEntity> getCategories() async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.Categories, data: {"locale": "en"});

    if (apiResponse.status) {
      return CategoryEntity.fromJson(apiResponse.response.data);
    } else {
      return CategoryEntity(message: apiResponse.msg, data: null);
    }
  }

  Future<SubCategoryEntity> getSubCategories(String categoryId) async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.SubCategories,
        data: {"locale": "en", "categoryId": categoryId});

    if (apiResponse.status) {
      return SubCategoryEntity.fromJson(apiResponse.response.data);
    } else {
      return SubCategoryEntity(message: apiResponse.msg, data: null);
    }
  }


  Future<CountryEntity> getCountryList(String countryLang) async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.CountryList,
        data: {"countryLang": "en"});
    if (apiResponse.status) {
      final countryEntity = CountryEntity.fromJson(apiResponse.response.data);
      return countryEntity..status = true;
    } else {
      return CountryEntity(message: apiResponse.msg, data: null);
    }
  }


  Future<CityEntity> getCityList(String countryId) async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.CityList,
        data: {"locale": "en","CountryId":countryId});
    if (apiResponse.status) {
      final countryEntity = CityEntity.fromJson(apiResponse.response.data);
      return countryEntity..status = true;
    } else {
      return CityEntity(message: apiResponse.msg, data: null);
    }
  }


}
