import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/model/all_chat_user_entity.dart';
import 'package:evento_provider/screens/chat/chat_screen.dart';
import 'package:evento_provider/screens/chat/repo/ChatRepo.dart';
import 'package:flutter/material.dart';

class AllChatUsersScreen extends StatelessWidget {
  final repo = ChatRepo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: AppColors.primaryColor,
        title: Text(
          "QUERIES".toUpperCase(),
          style: TextStyle(fontFamily: "Roboto"),
        ),
        centerTitle: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40),
                bottomRight: Radius.circular(40))),
      ),
      body: ChatUsersList(),
    );
  }
}

class ChatUsersList extends StatelessWidget {
  final repo = ChatRepo();

  @override
  Widget build(BuildContext context) {
    return GlobalCustomer.customerEntity != null
        ? FutureBuilder<AllChatUserEntity>(
            future: repo.fetchAllUsers(),
            builder: (context, snapshot) {
              return streamConsumer(snapshot, () {
                if (snapshot.data.data != null &&
                    snapshot.data.status &&
                    snapshot.data.data.isNotEmpty) {
                  return Container(
                    child: ListView.builder(
                      itemCount: snapshot.data.data.length,
                      itemBuilder: ((context, index) {
                        return ListTile(
                          leading: CircleAvatar(
                            backgroundImage: NetworkImage(
                                snapshot.data.data[index].profilePic == null
                                    ? ""
                                    : snapshot.data.data[index].profilePic),
                          ),
                          title: Text(
                              snapshot.data.data[index].fname.toUpperCase() +
                                      " " +
                                      snapshot?.data?.data[index]?.lname
                                          ?.toUpperCase() ??
                                  "",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                          subtitle: Text(snapshot.data.data[index].email,
                              style: TextStyle(
                                color: Colors.grey,
                              )),
                          onTap: (() {
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => ChatScreen(
                                        name: snapshot.data.data[index].fname,
                                        photoUrl: snapshot
                                            .data.data[index].profilePic,
                                        receiverUid:
                                            snapshot.data.data[index].id)));
                          }),
                        );
                      }),
                    ),
                  );
                } else {
                  /*return new Center(
                      child: Text(snapshot?.data?.message ??
                          "Sorry Something went wrong."));*/
                  return Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset("assets/notFound.png"),
                        Text(
                          "No User Query Found.".toUpperCase(),
                          style:
                              TextStyle(color: Colors.blueGrey, fontSize: 16.0),
                        )
                      ],
                    ),
                  );
                }
              });
            })
        : Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset("assets/nologin.png"),
                Text(
                  "You Need to login first.".toUpperCase(),
                  style: TextStyle(
                      color: Colors.blueGrey.withOpacity(0.6), fontSize: 16.0),
                )
              ],
            ),
          );
  }
}
