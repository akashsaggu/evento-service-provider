

import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/repo/UserRepository.dart';
import 'package:evento_provider/model/all_chat_user_entity.dart';

class ChatRepo extends BaseRepository {
  Future<AllChatUserEntity> fetchAllUsers() async {
    final userData = await UserRepo().getUserDetail();
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.AllChatUsers,
        data: {"locale": "en", "user_id": userData.data.id});
    if (apiResponse.status) {
      return AllChatUserEntity.fromJson(apiResponse.response.data);
    } else {
      return AllChatUserEntity(message: apiResponse.msg, data: null);
    }
  }
}
