import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/model/services_entity.dart';
import 'package:evento_provider/screens/Packages/PackagesScreen.dart';
import 'package:evento_provider/screens/banners/BannersScreen.dart';
import 'package:evento_provider/screens/gallery/GalleryAlbumScreen.dart';
import 'package:evento_provider/screens/services/edit/EditServiceScreen.dart';
import 'package:evento_provider/screens/services/repo/ServicesRepo.dart';
import 'package:evento_provider/screens/videos/VideosScreen.dart';
import 'package:flutter/material.dart';

class ServiceOptionScreen extends StatelessWidget {
  EditServiceScreen editServices;
  final ServicesData data;

  ServiceOptionScreen (this.data) {
    editServices = EditServiceScreen(data);
  }
  ServiceRepo serviceRepo = ServiceRepo();

  @override
  Widget build(BuildContext context) {
    List<Widget> options = [
      _itemHome("Banners", Icons.image, () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BannersScreen(data.serviceId)));
      }),
      _itemHome("Videos", Icons.videocam, () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => VideosScreen(data.serviceId)));
      }),
      _itemHome("Edit Services", Icons.edit, () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => editServices));
      }), _itemHome("Gallery", Icons.picture_in_picture, () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => GalleryAlbumScreen(data.serviceId)));
      }),_itemHome("Packages", Icons.monetization_on, () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => PackagesScreen(data.serviceId)));
      }),
      _itemHome("Delete", Icons.delete, () {
        serviceRepo.deleteService(data.serviceId).then((value){
          Navigator.of(context).pop();
        });
      }),
    ];
    return Scaffold(
      /*appBar: AppBar(
        elevation: 10,
        centerTitle: true,
        title: Text("Home"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
                bottomRight: Radius.circular(50),
                topLeft: Radius.circular(00),
                topRight: Radius.circular(00))),
      ),*/
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.lightBlueAccent, AppColors.primaryColor])),
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.arrow_back_ios,color: Color(0xffffffff),),
                  ),
                  Center(
                    child: Text(
                      "SERVICE",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(30))),
                        elevation: 7,
                        borderOnForeground: false,
                        color: Colors.white,
                        child: GridView.builder(
                            physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: options.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2),
                            itemBuilder: (context, index) => options[index]),
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _itemHome(String name, IconData icon, VoidCallback onTap) {
    return Card(
      color: AppColors.primaryColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30))),
      elevation: 6,
      margin: EdgeInsets.all(20),
      child: InkWell(
        splashColor: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        onTap: () {
          onTap();
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              icon,
              size: 30,
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                name,
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
