import 'package:evento_provider/base/StreamConsumer.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/banner_repo_entity.dart';
import 'package:evento_provider/screens/EditProfile/EditProfileScreen.dart';
import 'package:evento_provider/screens/banners/repo/BannerRepo.dart';
import 'package:evento_provider/screens/banners/widget/Add_Banner.dart';
import 'package:evento_provider/screens/services/create/CreateServiceScreen.dart';
import 'package:evento_provider/screens/services/listing/repo/ServicesRepo.dart';
import 'package:evento_provider/screens/services/listing/widget/ServiceItems.dart';
import 'package:evento_provider/widgets/ProgressWidget.dart';
import 'package:flutter/material.dart';
import 'package:evento_provider/model/services_entity.dart';
import 'package:toast/toast.dart';

final createServiceScreen = CreateServiceScreen();

class ServicesScreen extends StatefulWidget {
  @override
  _ServicesScreenState createState() =>
      _ServicesScreenState(GlobalCustomer.customerEntity.data.id);
}

class _ServicesScreenState extends State<ServicesScreen> {
  final String providerId;
  final repo = ServiceRepo();

  _ServicesScreenState(this.providerId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Add Service",
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(00),
                  bottomLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30))),
          onPressed: () {
            if(GlobalCustomer.customerEntity.data.address.isNotEmpty){
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => createServiceScreen));
          }else{
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => EditProfileScreen()));
              Toast.show("You need to fill all the fields before creating any service", context,duration: Toast.LENGTH_LONG,gravity: Toast.CENTER,backgroundRadius: 40);
            }}),
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30))),
        title: Text("SERVICES"),
        elevation: 6,
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
      ),
      body: FutureBuilder<ServicesEntity>(
          future: repo.fetch(),
          builder: (context, snapshot) {
            return streamConsumer(snapshot, () {
              if (snapshot.data.data != null &&
                  snapshot.data.status &&
                  snapshot.data.data.isNotEmpty) {
                return ListView.builder(
                    itemCount: snapshot.data.data.length,
                    itemBuilder: (context, index) {
                      return ServiceItem(snapshot.data.data[index]);
                    });
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset("assets/notFound.png"),
                      Text(
                        "No Services Added Yet".toUpperCase(),
                        style: TextStyle(
                            color: Colors.blueGrey.withOpacity(0.6),
                            fontSize: 16.0),
                      )
                    ],
                  ),
                );
              }
            });
          }),
    );
  }

  bannerItem(BannerRepoData data) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 180,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(data.bannerImage),
                      fit: BoxFit.cover)),
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomRight,
            child: InkWell(
              onTap: () async {
                repo.remove(data.id).then((value) {
                  setState(() {});
                });
              },
              child: Card(
                shape: CircleBorder(),
                child: Icon(Icons.close),
              ),
            ))
      ],
    );
  }
}
