import 'package:evento_provider/model/services_entity.dart';
import 'package:evento_provider/screens/services/editOptions/ServiceOptionScreen.dart';
import 'package:flutter/material.dart';

class ServiceItem extends StatelessWidget {
  final ServicesData data;

  ServiceItem(this.data);

  List<Widget> _buildStarRatingButtons() {
    List<Widget> buttons = [];

    for (int rateValue = 1; rateValue <= 5; rateValue++) {
      final starRatingButton = Icon(
          data.serviceRating >= rateValue ? Icons.star : Icons.star_border,
          color: Colors.yellow,
          size: 26);
      buttons.add(starRatingButton);
    }

    return buttons;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ServiceOptionScreen(data)));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
                height: 160,
                width: MediaQuery.of(context).size.width,
                child: Material(
                  elevation: 7,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        image: DecorationImage(
                            image: NetworkImage(
                              data.serviceDefaultImage,
                            ),
                            fit: BoxFit.cover)),
                  ),
                )),
          ),
        ),
        Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 230,
              width: MediaQuery.of(context).size.width - 100,
              child: Card(
                elevation: 8,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                margin: EdgeInsets.only(top: 130),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8),
                      child: Text(
                        data.serviceTitle,
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: _buildStarRatingButtons(),
                    ),
                  /*  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(
                            Icons.location_on,
                            color: Colors.black,
                            size: 18,
                          ),
                          Flexible(
                              child: Text(
                            data.serviceAddress,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ))
                        ],
                      ),
                    )*/
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
