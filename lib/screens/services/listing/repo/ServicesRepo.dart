import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/banner_repo_entity.dart';
import 'package:evento_provider/model/services_entity.dart';

class ServiceRepo extends BaseRepository {
  Future<ServicesEntity> fetch() async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.GetProviderServices,
        data: {"locale": "en", "provider_id": GlobalCustomer.customerEntity.data.id});
    if (apiResponse.status) {
      return ServicesEntity.fromJson(apiResponse.response.data);
    } else {
      return ServicesEntity(message: apiResponse.msg, data: null);
    }
  }
  Future<BannerRepoEntity> remove(String bannerId) async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.ServiceBannerImgRemove,
        data: {"locale": "en", "provider_id": GlobalCustomer.customerEntity.data.id,"banner_id": bannerId});
    if (apiResponse.status) {
      return BannerRepoEntity.fromJson(apiResponse.response.data);
    } else {
      return BannerRepoEntity(message: apiResponse.msg, data: null);
    }
  }
  Future<BannerRepoEntity> add(String bannerImage) async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.ServiceBannerImgAdd,
        data: {"locale": "en", "provider_id": GlobalCustomer.customerEntity.data.id,"banner_image": bannerImage});
    if (apiResponse.status) {
      return BannerRepoEntity.fromJson(apiResponse.response.data);
    } else {
      return BannerRepoEntity(message: apiResponse.msg, data: null);
    }
  }
}
