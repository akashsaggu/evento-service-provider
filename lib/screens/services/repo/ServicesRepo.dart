import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/model/customer_entity.dart';
import 'package:evento_provider/model/service_add_helper_entity.dart';

class ServiceRepo extends BaseRepository {

  Future<CustomerEntity> doAddService(ServiceAddHelperEntity data) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.AddService, data: data.toJson());

    //todo change to common entity
    if (apiResponse.status) {
      return CustomerEntity.fromJson(apiResponse.response.data);
    } else {
      return CustomerEntity(message: apiResponse.msg, data: null);
    }
  }

  Future<CustomerEntity> updateService(ServiceAddHelperEntity data) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.UpdateService, data: data.toJson());

    //todo change to common entity
    if (apiResponse.status) {
      return CustomerEntity.fromJson(apiResponse.response.data);
    } else {
      return CustomerEntity(message: apiResponse.msg, data: null);
    }
  }
  Future<CustomerEntity> deleteService(String serviceId) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.DeleteService, data: {
          "service_id":serviceId
    });

    //todo change to common entity
    if (apiResponse.status) {
      return CustomerEntity.fromJson(apiResponse.response.data);
    } else {
      return CustomerEntity(message: apiResponse.msg, data: null);
    }
  }

}
