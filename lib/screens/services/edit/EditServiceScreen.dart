import 'dart:io';
import 'dart:math';

import 'package:evento_provider/address/MapWidget.dart';
import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/bloc/CategoryBloc.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/model/service_add_helper_entity.dart';
import 'package:evento_provider/model/services_entity.dart';
import 'package:evento_provider/screens/services/repo/ServicesRepo.dart';
import 'package:evento_provider/services/validations.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:evento_provider/widgets/AddressWidget.dart';
import 'package:evento_provider/widgets/CategorySpinner.dart';
import 'package:evento_provider/widgets/CitySpinner.dart';
import 'package:evento_provider/widgets/CountrySpinner.dart';
import 'package:evento_provider/widgets/DaysBottomSheet.dart';
import 'package:evento_provider/widgets/MultipleMobileNo.dart';
import 'package:evento_provider/widgets/ProgressWidget.dart';
import 'package:evento_provider/widgets/SubCategorySpinner.dart';
import 'package:evento_provider/widgets/TimeBottomSheet.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;

var categoryValue = "";
var subCategoryValue = "";
var countryValue = "";
var cityValue = "";
var startDay = "Select Start Day";
var endDay = "Select End Time";
var startTime = "Select Start Time";
var endTime = "Select End Time";
File image;
final instaController = TextEditingController();
final facebookController = TextEditingController();
final titleController = TextEditingController();
final headerController = TextEditingController();
final descriptionTextController = TextEditingController();
AddressHelper addressObject;
ServicesData data;

class EditServiceScreen extends StatelessWidget {
  EditServiceScreen(ServicesData dataA) {
    data = dataA;
  }

  @override
  Widget build(BuildContext context) {
    return ProgressWidgetWrapper(
      child: EditServiceScreenUi(),
    );
  }
}

class EditServiceScreenUi extends StatelessWidget {
  bool isDataSet = false;

  void setPreviousFields() {
    if (!isDataSet) {
      titleController.text = data.serviceTitle;
      headerController.text = data.serviceHeading;
      descriptionTextController.text = data.serviceDescription;
     /* facebookController.text =
          GlobalCustomer.customerEntity.data.facebookPageId;
      instaController.text = GlobalCustomer.customerEntity.data.instagramPageId;*/
      categoryValue = data.categoryId;
      subCategoryValue = data.subcategoryId;
      countryValue = data.countryId;
      cityValue = data.cityId;
     /* startDay = "${data.serviceWorkingDays.substring(0, 3)}day";
      endDay = "${data.serviceWorkingDays.substring(4, 7)}day";
      startTime = "${data.serviceWorkingHours.substring(0, 2)}:00";
      endTime = "${data.serviceWorkingHours.substring(4, 5)}:00";*/
      isDataSet = true;
    }
  }

  final category = CategorySpinner(
    (value) {
      categoryValue = value.id;
    },
    preSelectedCategory: data.categoryId,
  );

  final subCategory = SubCategorySpinner(
    (value) {
      subCategoryValue = value.id;
    },
    preSelectedSubCategory: data.subcategoryId,
  );

  final country = CountrySpinner(
    (value) {
      countryValue = value.id;
    },
    preSelectedCountry: data.countryId,
  );

  final city = CitySpinner(
    (value) {
      cityValue = value.id;
    },
    preSelectedCity: data.cityId,
  );

  final categoryLabel = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("Select Category", style: TextStyle(color: Colors.white)));

  final countryLabel = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("Select Country", style: TextStyle(color: Colors.white)));

  final cityLabel = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text("Select City", style: TextStyle(color: Colors.white)));

  final subCategoryLabel = Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8.0),
      child: Text("Select SubCategory", style: TextStyle(color: Colors.white)));

/*  final selectStartDay = DaysBottomSheet((value) {
    startDay = value;
  }, "${data.serviceWorkingDays.substring(0, 3)}day");

  final selectEndDay = DaysBottomSheet((value) {
    endDay = value;
  }, "${data.serviceWorkingDays.substring(4, 7)}day");*/

/*  final time = TimeBottomSheet((value) {
    startTime = value;
  }, "${data.serviceWorkingHours.substring(0, 2)}:00");

  final EndTime = TimeBottomSheet((value) {
    endTime = value;
  }, "${data.serviceWorkingHours.substring(4, 5)}:00");*/
/*
  final address = AddresWidget((value) {
    addressObject = value;
  }, address: data.serviceAddress);*/

  final title = TextFormField(
    controller: titleController,
    style: TextStyle(color: Colors.white),
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Service Title',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );
  final heading = TextFormField(
    controller: headerController,
    style: TextStyle(color: Colors.white),
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Service Heading',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  final description = TextFormField(
    controller: descriptionTextController,
    maxLines: 4,
    maxLength: 250,
    keyboardType: TextInputType.multiline,
    autofocus: false,
    style: TextStyle(color: Colors.white),
    decoration: InputDecoration(
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      hintText: 'Descirption',
      fillColor: Colors.blueAccent,
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  final profile = ImageWidget(() async {
    image = await ImagePicker.pickImage(source: ImageSource.gallery);
    return image;
  });

  final facebookLink = TextFormField(
    controller: facebookController,
    style: TextStyle(color: Colors.white),
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Facebook Link',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

  final instaLink = TextFormField(
    controller: instaController,
    style: TextStyle(color: Colors.white),
    autofocus: false,
    decoration: InputDecoration(
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white),
      ),
      fillColor: Colors.blueAccent,
      hintText: 'Instagram Link',
      hintStyle: TextStyle(color: Colors.white),
      contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
    ),
  );

/*
  static List<PreSelectedMobile> getPreselectedMobile() {
    final preSelectedMobileList = <PreSelectedMobile>[];
    if (data.serviceMobileNumbers.contains(",")) {
      data.serviceMobileNumbers.split(",").forEach((mobileN) {
        final mobile = mobileN.split(" ");
        preSelectedMobileList
            .add(PreSelectedMobile(countryCode: mobile[0], mobile: mobile[1]));
      });
    } else {
      final mobile = data.serviceMobileNumbers.split(" ");
      preSelectedMobileList
          .add(PreSelectedMobile(countryCode: mobile[0], mobile: mobile[1]));
    }
    return preSelectedMobileList;
  }

  final mobileNumber = MultipleMobileNoWidget(
    preSelectedMobiles: getPreselectedMobile(),
  );
*/

  @override
  Widget build(BuildContext context) {
    final categoryBloc = CategoryBloc();
    setPreviousFields();
    categoryBloc.getCategories();
    categoryBloc.getCountries("en");
    final createServiceButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          addService(context);
        },
        padding: EdgeInsets.all(12),
        color: Colors.blueAccent,
        child: Text('Update Service', style: TextStyle(color: Colors.white)),
      ),
    );
    return WillPopScope(
      onWillPop: () async {
        clearFields();
        return true;
      },
      child: BlocProvider<CategoryBloc>(
        bloc: categoryBloc,
        child: Scaffold(
          backgroundColor: AppColors.primaryColor,
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30))),
            title: Text("UPDATE SERVICE"),
            elevation: 6,
            centerTitle: true,
            backgroundColor: Colors.blueAccent,
          ),
          body: Stack(
            children: <Widget>[

              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Colors.blue[900], Colors.blueAccent],
                        begin: Alignment.bottomRight,
                        end: Alignment.topCenter)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView(
                    children: <Widget>[
                      SizedBox(height: 8.0),
                      profile,
                      SizedBox(height: 8.0),
                      title,
                      SizedBox(height: 8.0),
                      heading,
                      SizedBox(height: 8.0),
                      description,
                      SizedBox(height: 8.0),
                      /*mobileNumber,
                      SizedBox(height: 8.0),
                      address,
                      SizedBox(height: 8.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: time),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: EndTime),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: selectStartDay),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: selectEndDay),
                        ],
                      ),*/
                      SizedBox(height: 4.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: countryLabel),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: cityLabel),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: country),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: city),
                        ],
                      ),
                      SizedBox(height: 8.0),
                      /*Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: categoryLabel),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: subCategoryLabel),
                        ],
                      ),*/
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: category),
                          SizedBox(height: 8.0, width: 20.0),
                          Expanded(child: subCategory),
                        ],
                      ),
                      SizedBox(height: 8.0),
                     /* facebookLink,
                      SizedBox(height: 8.0),
                      instaLink,
                      SizedBox(height: 8.0),*/
                      createServiceButton
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void clearFields() {
    categoryValue = "";
    subCategoryValue = "";
    countryValue = "";
    cityValue = "";
    startDay = "Select Start Day";
    endDay = "Select End Time";
    startTime = "Select Start Time";
    endTime = "Select End Time";
    image = null;
    instaController.clear();
    facebookController.clear();
    titleController.clear();
    headerController.clear();
    descriptionTextController.clear();
    addressObject = null;
  }
/*

  String getMobileNumbers() {
    var mobileNumbers = "";
    mobileNumber.mobileForms.forEach((mobileForms) {
      if (mobileForms.controller.text.isNotEmpty)
        mobileNumbers = mobileNumbers +
            "${mobileNumbers.isEmpty ? '' : ','}" +
            "${mobileForms.formWidget.countryCode} " +
            mobileForms.controller.text;
    });
    return mobileNumbers;
  }
*/

  void addService(BuildContext context) async {
    final progressBloc = BlocProvider.of<ProgressBloc>(context);
    if (validate(context)) {
      progressBloc.showProgress();
      ServiceRepo()
          .updateService(ServiceAddHelperEntity(
              serviceId: data.serviceId,
              providerId: GlobalCustomer.customerEntity.data.id,
//              serviceMobileNumbers: getMobileNumbers(),
              countryId: countryValue,
              cityId: cityValue,
              serviceTitle: titleController.text,
              currentDate: DateTime.now().toString(),
              categoryId: categoryValue,
              subcategoryId: subCategoryValue,
//              serviceAddress: addressObject == null
//                  ? data.serviceAddress
//                  : addressObject.address,
//              serviceLat:
//                  addressObject == null ? data.serviceLat : addressObject.lat,
//              serviceLong:
//                  addressObject == null ? data.serviceLong : addressObject.long,
              serviceHeading: headerController.text,
              //serviceStartingFrom: DateTime.now().toString(),
              serviceDescription: descriptionTextController.text,
//              serviceWorkingDays: "" + startDay + "-" + endDay + "",
//              serviceWorkingHours: "" + startTime + "-" + endTime + "",
//              serviceFacebookPageId: facebookController.text,
//              serviceInstaPageId: instaController.text,
              serviceDefaultImage: image != null
                  ? await uploadImageAndGetUrl()
                  : data.serviceDefaultImage))
          .then((value) {
        if (value.status) {
          progressBloc.hideProgress();
          toast(context, "Service Updated Successfully.");
          clearFields();
          Navigator.pop(context);
        } else {
          progressBloc.hideProgress();
          toast(context, "Sorry something went wrong.");
        }
      }).catchError((error) {
        progressBloc.hideProgress();
        toast(context, error.toString());
      });
    }
  }

  Future<String> uploadImageAndGetUrl() async {
    final StorageReference storageRef = FirebaseStorage.instance.ref().child(p
        .basename(Random().nextInt(10000).toString() + p.basename(image.path)));
    final uploadTask = storageRef.putFile(
      image,
      StorageMetadata(
        contentType: "image" + '/' + p.extension(image.path),
      ),
    );
    final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
    return await downloadUrl.ref.getDownloadURL();
  }

  bool validate(context) {
    final titleValidate = Validations.validateName(titleController.text);
   /* final facebookLinkValidate =
        Validations.validateNotEmpty(facebookController.text, "Facebook link");
    final instaLinkValidate =
        Validations.validateNotEmpty(instaController.text, "Instagram link");*/
    final headerValidate = Validations.validateName(headerController.text);
    final descriptionValidate = Validations.validateNotEmpty(
        descriptionTextController.text, "Description.");

    /*if (!facebookLinkValidate.status) {
      toast(context, facebookLinkValidate.msg);
      return false;
    }

    if (!instaLinkValidate.status) {
      toast(context, instaLinkValidate.msg);
      return false;
    }*/

    if (!titleValidate.status) {
      toast(context, titleValidate.msg);
      return false;
    }
    if (!headerValidate.status) {
      toast(context, headerValidate.msg);
      return false;
    }

    if (!descriptionValidate.status) {
      toast(context, descriptionValidate.msg);
      return false;
    }

    return true;
  }
}

class ImageWidget extends StatefulWidget {
  final Future<File> Function() onClickCallback;

  ImageWidget(this.onClickCallback);

  @override
  State<StatefulWidget> createState() {
    return ImageWidgetState();
  }
}

class ImageWidgetState extends State<ImageWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        image = await widget.onClickCallback();
        setState(() {});
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 54.0),
            child: CircleAvatar(
                radius: 70.0,
                backgroundImage: image == null
                    ? NetworkImage(data.serviceDefaultImage,scale: 1)
                    : FileImage(image,scale: 1)),
          ),
        ],
      ),
    );
  }
}
