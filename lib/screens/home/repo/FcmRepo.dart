import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/model/common_entity.dart';

class FcmRepo extends BaseRepository {
  Future<CommonEntity> updateFcm(String fcmToken) async {
    ApiResponse apiResponse = await apiHitter
        .getPostApiResponse(ApiEndpoint.UpdateServiceProviderFcm, data: {
      "id": GlobalCustomer.customerEntity.data.id,
      "fcm_token": fcmToken,
      "device_id" : GlobalCustomer.customerEntity.data.deviceId,
    });
    if (apiResponse.status) {
      return CommonEntity.fromJson(apiResponse.response.data);
    } else {
      return CommonEntity(
        message: apiResponse.msg,
      );
    }
  }
}
