import 'package:evento_provider/common/repo/UserRepository.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/screens/Bookings/Bookings.dart';
import 'package:evento_provider/screens/EditProfile/EditProfileScreen.dart';
import 'package:evento_provider/screens/UserCalled/UserCalledUi.dart';
import 'package:evento_provider/screens/UserVisited/UserVisitedUI.dart';
import 'package:evento_provider/screens/allChat/all_users_screen.dart';
import 'package:evento_provider/screens/home/repo/FcmRepo.dart';
import 'package:evento_provider/screens/home/repo/NotificaionHandler.dart';
import 'package:evento_provider/screens/services/listing/ServicesScreen.dart';
import 'package:evento_provider/screens/signIn/SignInScreen.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
Future<bool> _exitApp(BuildContext context) {
  return showDialog(
    context: context,
    child: new AlertDialog(
      elevation: 9,

      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
      title: new Text('Do you want to exit this application?'),
      content: new Text("We can't to see you leaving..."),
      actions: <Widget>[
        new FlatButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: new Text('No'),
        ),
        new FlatButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: new Text('Yes'),
        ),
      ],
    ),
  ) ??
      false;
}
class HomeOptionScreen extends StatelessWidget {
  final editProfile = EditProfileScreen();
  final createServiceScreen = ServicesScreen();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FcmRepo repo = FcmRepo();

  @override
  Widget build(BuildContext context) {
    _firebaseMessaging.getToken().then((value) {
      repo.updateFcm(value);
    });
        FirebaseNotifications().setUpFirebase(context);
    List<Widget> options = [
      _itemHome("Bookings", Icons.library_books, () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => BookingScreen()));
      }),
      _itemHome("Services", Icons.settings_remote, () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => createServiceScreen));
      }),
      _itemHome("Queries", Icons.chat, () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => AllChatUsersScreen()));
      }),
        _itemHome("User Visited", Icons.person, () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => UserVisitedScreen()));
      }),
      _itemHome("User Called", Icons.person, () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => UserCalledUi()));
      }),
      _itemHome("Edit Profile", Icons.edit, () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => editProfile));
      }),
      _itemHome("SignOut", Icons.call_missed_outgoing, () {
        UserRepo().clearPreference().then((value) {
          if (value) {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => SignInScreen()));
          } else {
            toast(context, "Sorry Something Went Wrong.");
          }
        });
      })
    ];
    return WillPopScope(
      onWillPop:()async{

        return _exitApp(context);
      },
      child: Scaffold(
        /*appBar: AppBar(
          elevation: 10,
          centerTitle: true,
          title: Text("Home"),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50),
                  bottomRight: Radius.circular(50),
                  topLeft: Radius.circular(00),
                  topRight: Radius.circular(00))),
        ),*/
        body: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.lightBlueAccent, AppColors.primaryColor])),
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 60,
                  ),
                  Center(
                    child: Text(
                      "HOME",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(30))),
                        elevation: 7,
                        borderOnForeground: false,
                        color: Colors.white,
                        child: GridView.builder(
                            physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: options.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2),
                            itemBuilder: (context, index) => options[index]),
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _itemHome(String name, IconData icon, VoidCallback onTap) {
    return Card(
      color: AppColors.primaryColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30))),
      elevation: 6,
      margin: EdgeInsets.all(20),
      child: InkWell(
        splashColor: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        onTap: () {
          onTap();
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              icon,
              size: 30,
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                name,
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
