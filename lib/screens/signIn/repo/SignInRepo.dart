import 'package:evento_provider/base/BaseRepository.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:evento_provider/base/network/ApiHitter.dart';
import 'package:evento_provider/model/customer_entity.dart';
import 'package:evento_provider/model/sign_in_helper_entity.dart';

class SignInRepo extends BaseRepository {

  // ignore: missing_return
  Future<CustomerEntity> doSignIn(SignInHelperEntity data) async {
    ApiResponse apiResponse = await apiHitter.getPostApiResponse(
        ApiEndpoint.ProviderSignIn, data: data.toJson());


    if (apiResponse.status) {
      return CustomerEntity.fromJson(apiResponse.response.data);
    } else {
      return CustomerEntity(message: apiResponse.msg, data: null);
    }

  }

}


