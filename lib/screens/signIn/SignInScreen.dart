// ignore: must_be_immutable
import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/common/repo/UserRepository.dart';
import 'package:evento_provider/model/sign_in_helper_entity.dart';
import 'package:evento_provider/screens/home/HomeOptionScreen.dart';
import 'package:evento_provider/screens/register/SignUpScreen.dart';
import 'package:evento_provider/screens/signIn/repo/SignInRepo.dart';
import 'package:evento_provider/services/validations.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:evento_provider/widgets/ProgressWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

final signUp = SignUpScreen();

// ignore: must_be_immutable
class SignInScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProgressWidgetWrapper(child: SignInScreenUi());
  }
}


class SignInScreenUi extends StatelessWidget {
  String fcmToken;
  final emailTextController = TextEditingController();
  final passWordTextController = TextEditingController();
  final signInRepo = SignInRepo();
  final signInHelperEntity = SignInHelperEntity();


  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 72.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final email = TextFormField(
      controller: emailTextController,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        prefixIcon: Image.asset("assets/email.png"),
        fillColor: Colors.blueAccent,
        hintText: 'Email',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    final password = TextFormField(
      controller: passWordTextController,
      autofocus: false,
      style: TextStyle(color: Colors.white),
      obscureText: true,
      decoration: InputDecoration(
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        prefixIcon: Image.asset("assets/password.png"),
        hintText: 'Password',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          if (validateEmailAndPassword(context)) {
            createLoginData();
            loginWithEmail(context);
          }
        },
        padding: EdgeInsets.all(12),
        color: Colors.blueAccent,
        child: Text('LOGIN', style: TextStyle(color: Colors.white)),
      ),
    );


    final forgotLabel = FlatButton(
      child: Container(
        child: Align(
          alignment: Alignment.centerRight,
          child: Text(
            'Forgot password?',
            textAlign: TextAlign.end,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
      onPressed: () {},
    );

    final signUpLabel = Center(
        child: RichText(
          text: TextSpan(
            // Note: Styles for TextSpans must be explicitly defined.
            // Child text spans will inherit styles from parent
            style: TextStyle(
              fontSize: 14.0,
              color: Colors.black,
            ),
            children: <TextSpan>[
              TextSpan(
                  text: "Don't have an Account? ",
                  style: TextStyle(color: Colors.white)),
              TextSpan(
                  text: 'SIGNUP',
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => signUp, maintainState: true));
                    },
                  style: new TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.blueAccent)),
            ],
          ),
        ));

    final loginLabel = FlatButton(
      child: Text(
        "LOGIN",
        style: TextStyle(
            color: Colors.white, fontSize: 22.0, fontWeight: FontWeight.bold),
      ),
      onPressed: () {},
    );

    return Scaffold(
      body: ProgressWidgetWrapper(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.blue[900], Colors.blueAccent],
                      begin: Alignment.bottomRight,
                      end: Alignment.topCenter)),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  child: Center(
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        logo,
                        loginLabel,
                        SizedBox(height: 24.0),
                        email,
                        SizedBox(height: 16.0),
                        password,
                        SizedBox(height: 4.0),
                        forgotLabel,
                        SizedBox(height: 16.0),
                        loginButton,
                        SizedBox(height: 16.0),
                        signUpLabel,
                        SizedBox(height: 16.0)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool validateEmailAndPassword(BuildContext context) {
    final emailText = emailTextController.text;
    final passText = passWordTextController.text;
    final emailValidate = Validations.validateEmail(emailText);
    if (!emailValidate.status) {
      toast(context, emailValidate.msg);
      return false;
    }

    return true;
  }

  final homeOptionScreen = HomeOptionScreen();
  void loginWithEmail(BuildContext context) async {
    final progressBloc = BlocProvider.of<ProgressBloc>(context);
    progressBloc.showProgress();
    signInRepo
        .doSignIn(signInHelperEntity)
        .then((value) {
      if (value.status) {
        UserRepo().saveUser(value).then((value) {
          if (value) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => homeOptionScreen));
          } else {
            toast(context, "Sorry something went wrong.");
          }
          progressBloc.hideProgress();
        });
      } else {
        progressBloc.hideProgress();
        toast(context, value.message);
      }
    }).catchError((error) {
      progressBloc.hideProgress();
      toast(context, error.toString());
    });
  }

  void createLoginData() {
    signInHelperEntity.email = emailTextController.text;
    signInHelperEntity.password = passWordTextController.text;
    signInHelperEntity.deviceId = "dsfdsafd";
    signInHelperEntity.fcmToken = "dsafdsfxfdsfgds";
  }
}


