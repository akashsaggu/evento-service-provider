import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:evento_provider/common/repo/UserRepository.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/model/customer_entity.dart';
import 'package:evento_provider/model/event_entity.dart';
import 'package:evento_provider/screens/chat/repo/ChatRepo.dart';
import 'package:evento_provider/screens/chat/widgets/EventItem.dart';
import 'package:evento_provider/screens/chat/widgets/NoEventItem.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'full_screen_image.dart';
import 'models/message.dart';

// ignore: must_be_immutable
class ChatScreen extends StatefulWidget {
  String name;
  String photoUrl;
  String receiverUid;

  ChatScreen({this.name, this.photoUrl, this.receiverUid});

  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  Message _message;
  var _formKey = GlobalKey<FormState>();
  var map = Map<String, dynamic>();
  CollectionReference _collectionReference;
  DocumentSnapshot documentSnapshot;
  String _senderuid;
  var listItem;
  String receiverPhotoUrl, senderPhotoUrl = "", receiverName, senderName;
  StreamSubscription<DocumentSnapshot> subscription;
  File imageFile;
  StorageReference _storageReference;
  TextEditingController _messageController;
  ScrollController _scrollController = new ScrollController();
  StreamController<bool> progressStream = StreamController<bool>();

  @override
  void initState() {
    super.initState();

    _messageController = TextEditingController();
    getUID().then((user) {
      setState(() {
        _senderuid = user.data.id;
        print("sender uid : $_senderuid");
        getSenderPhotoUrl(_senderuid).then((snapshot) {
          setState(() {
            //senderPhotoUrl = snapshot['photoUrl'];
            senderName = snapshot['fname'];
          });
        });
        getReceiverPhotoUrl(widget.receiverUid).then((snapshot) {
          setState(() {
            receiverPhotoUrl = snapshot['photoUrl'] ?? "";
            receiverName = snapshot['fname'];
          });
        });
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription?.cancel();
  }

  void addMessageToDb(Message message) async {
    print("Message : ${message.message}");
    map = message.toMap();

    print("Map : ${map}");
    Firestore.instance.runTransaction((transaction) async {
      await transaction.set(
          Firestore.instance
              .collection("chat")
              .document(message.senderUid)
              .collection("users")
              .document(widget.receiverUid),
          {
            'foo': "foo",
          });
    });
    _collectionReference = Firestore.instance
        .collection("chat")
        .document(message.senderUid)
        .collection("users")
        .document(widget.receiverUid)
        .collection("messages");

    _collectionReference.add(map).whenComplete(() {
      print("Messages added to db");
    });

    Firestore.instance.runTransaction((transaction) async {
      await transaction.set(
          Firestore.instance
              .collection("chat")
              .document(widget.receiverUid)
              .collection("users")
              .document(message.senderUid),
          {
            'foo': "foo",
          });
    });
    _collectionReference = Firestore.instance
        .collection("chat")
        .document(widget.receiverUid)
        .collection("users")
        .document(message.senderUid)
        .collection("messages");

    _collectionReference.add(map).whenComplete(() {
      print("Messages added to db");
    });

    _messageController.text = "";
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.name.toUpperCase()),
        ),
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return [
              SliverToBoxAdapter(
                child: Container(
                    height: 0, child: EventsList(widget.receiverUid)),
              )
            ];
          },
          body: Form(
            key: _formKey,
            child: _senderuid == null
                ? Container(
                    child: Center(child: CircularProgressIndicator()),
                  )
                : Column(
                    children: <Widget>[
                      //buildListLayout(),
                      ChatMessagesListWidget(),
                      ProgressIndicator(progressStream.stream),
                      buildInput(),
                    ],
                  ),
          ),
        ));
  }

  void showProgress() {
    progressStream.sink.add(true);
  }

  void hideProgress() {
    progressStream.sink.add(false);
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 1.0),
              child: new IconButton(
                icon: new Icon(Icons.image),
                onPressed: () {
                  pickImage();
                },
                color: AppColors.primaryColor,
              ),
            ),
            color: Colors.white,
          ),

          // Edit text
          Flexible(
            child: Container(
              child: TextFormField(
                validator: (String input) {
                  if (input.isEmpty) {
                    return "Please enter message";
                  }
                },
                style: TextStyle(fontSize: 15.0),
                controller: _messageController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Send a message',
                  hintStyle: TextStyle(color: Colors.blueGrey),
                ),
              ),
            ),
          ),

          // Button send message
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 8.0),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    sendMessage();
                  }
                },
                color: AppColors.primaryColor,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: new BoxDecoration(
          border: new Border(
              top: new BorderSide(color: Color(0xffE8E8E8), width: 0.5)),
          color: Colors.white),
    );
  }

  Widget ChatInputWidget() {
    return Container(
      height: 55.0,
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 4.0),
            child: IconButton(
              splashColor: Colors.white,
              icon: Icon(
                Icons.camera_alt,
                color: AppColors.primaryColor,
              ),
              onPressed: () {
                pickImage();
              },
            ),
          ),
          Flexible(
            child: TextFormField(
              validator: (String input) {
                if (input.isEmpty) {
                  return "Please enter message";
                }
              },
              controller: _messageController,
              decoration: InputDecoration(
                  hintText: "Enter message...",
                  labelText: "Message",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
              onFieldSubmitted: (value) {
                _messageController.text = value;
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 4.0),
            child: IconButton(
              splashColor: Colors.white,
              icon: Icon(
                Icons.send,
                color: AppColors.primaryColor,
              ),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  sendMessage();
                }
              },
            ),
          )
        ],
      ),
    );
  }

  Future<String> pickImage() async {
    showProgress();
    var selectedImage =
        await ImagePicker.pickImage(source: ImageSource.gallery);
    if (selectedImage == null) {
      hideProgress();
    }
    setState(() {
      imageFile = selectedImage;
    });
    _storageReference = FirebaseStorage.instance
        .ref()
        .child('${DateTime.now().millisecondsSinceEpoch}');
    StorageUploadTask storageUploadTask = _storageReference.putFile(imageFile);
    var url = await (await storageUploadTask.onComplete).ref.getDownloadURL();

    print("URL: $url");
    uploadImageToDb(url);
    return url;
  }

  void uploadImageToDb(String downloadUrl) {
    _message = Message.withoutMessage(
        receiverUid: widget.receiverUid,
        senderUid: _senderuid,
        photoUrl: downloadUrl,
        timestamp: FieldValue.serverTimestamp(),
        type: 'image');
    var map = Map<String, dynamic>();
    map['senderUid'] = _message.senderUid;
    map['receiverUid'] = _message.receiverUid;
    map['type'] = _message.type;
    map['timestamp'] = _message.timestamp;
    map['photoUrl'] = _message.photoUrl;

    print("Map : ${map}");
    Firestore.instance.runTransaction((transaction) async {
      await transaction.set(
          Firestore.instance
              .collection("chat")
              .document(_message.senderUid)
              .collection("users")
              .document(widget.receiverUid),
          {
            'foo': "foo",
          });
    });
    _collectionReference = Firestore.instance
        .collection("chat")
        .document(_message.senderUid)
        .collection("users")
        .document(widget.receiverUid)
        .collection("messages");

    _collectionReference.add(map).whenComplete(() {
      print("Messages added to db");
    });

    Firestore.instance.runTransaction((transaction) async {
      await transaction.set(
          Firestore.instance
              .collection("chat")
              .document(widget.receiverUid)
              .collection("users")
              .document(_message.senderUid),
          {
            'foo': "foo",
          });
    });

    _collectionReference = Firestore.instance
        .collection("chat")
        .document(widget.receiverUid)
        .collection("users")
        .document(_message.senderUid)
        .collection("messages");

    _collectionReference.add(map).whenComplete(() {
      print("Messages added to db");
    });
    hideProgress();
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
  }

  void sendMessage() async {
    print("Inside send message");
    var text = _messageController.text;
    print(text);
    _message = Message(
        receiverUid: widget.receiverUid,
        senderUid: _senderuid,
        message: text,
        timestamp: FieldValue.serverTimestamp(),
        type: 'text');
    print(
        "receiverUid: ${widget.receiverUid} , senderUid : ${_senderuid} , message: ${text}");
    print(
        "timestamp: ${DateTime.now().millisecond}, type: ${text != null ? 'text' : 'image'}");
    addMessageToDb(_message);
  }

  Future<CustomerEntity> getUID() async {
    final userRepo = UserRepo();
    CustomerEntity user = await userRepo.getUserDetail();
    return user;
  }

  Future<DocumentSnapshot> getSenderPhotoUrl(String uid) {
    var senderDocumentSnapshot =
        Firestore.instance.collection('ServiceProvider').document(uid).get();
    return senderDocumentSnapshot;
  }

  Future<DocumentSnapshot> getReceiverPhotoUrl(String uid) {
    var receiverDocumentSnapshot =
        Firestore.instance.collection('customer').document(uid).get();
    return receiverDocumentSnapshot;
  }

  Widget ChatMessagesListWidget() {
    print("SENDERUID : $_senderuid");
    return Flexible(
      child: StreamBuilder(
        stream: Firestore.instance
            .collection('chat')
            .document(_senderuid)
            .collection('users')
            .document(widget.receiverUid)
            .collection("messages")
            .orderBy('timestamp', descending: false)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            listItem = snapshot.data.documents;
            Timer(Duration(milliseconds: 1000), () {
              _scrollController.animateTo(
                  _scrollController.position.maxScrollExtent,
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.easeOut);
            });
            return ListView.builder(
              controller: _scrollController,

              padding: EdgeInsets.all(10.0),
              itemBuilder: (context, index) {
                return chatMessageItem(snapshot.data.documents[index]);
              },
              itemCount: snapshot.data.documents.length,
            );
          }
        },
      ),
    );
  }

  Widget chatMessageItem(DocumentSnapshot documentSnapshot) {
    return buildChatLayout(documentSnapshot);
  }

  Widget buildChatLayout(DocumentSnapshot snapshot) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            mainAxisAlignment: snapshot['senderUid'] == _senderuid
                ? MainAxisAlignment.end
                : MainAxisAlignment.start,
            children: <Widget>[
              snapshot['senderUid'] == _senderuid
                  ? Container(
                      height: 40,
                      width: 40,
                      decoration: ShapeDecoration(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(60.0)),
                          color: AppColors.primaryColor),
                      child: Center(
                        child: Text(
                          senderName == null
                              ? ""
                              : senderName.substring(0, 1).toUpperCase(),
                          style: TextStyle(fontSize: 16.0, color: Colors.white),
                        ),
                      ),
                    )
                  : CircleAvatar(
                      backgroundImage: receiverPhotoUrl == null
                          ? AssetImage('assets/blankimage.png')
                          : NetworkImage(receiverPhotoUrl),
                      radius: 20.0,
                    ),
              SizedBox(
                width: 10.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  snapshot['senderUid'] == _senderuid
                      ? new Text(
                          senderName == null ? "" : senderName.toUpperCase(),
                          style: TextStyle(
                              color: AppColors.primaryColor,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        )
                      : new Text(
                          receiverName == null
                              ? ""
                              : receiverName.toUpperCase(),
                          style: TextStyle(
                              color: AppColors.primaryColor,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                  snapshot['type'] == 'text'
                      ? Container(
                          margin: EdgeInsets.only(top: 8.0),
                          decoration: ShapeDecoration(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              color: Color(0xFFFFFDD0)),
                          padding: EdgeInsets.all(8.0),
                          width: screenWidth(context) / 1.8,
                          child: Text(
                            snapshot['message'],
                            style: TextStyle(
                                color: Color(0xFF696969), fontSize: 14.0),
                            softWrap: true,
                          ))
                      : Container(
                          margin: EdgeInsets.only(top: 8.0),
                          decoration: ShapeDecoration(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0)),
                              color: Color(0xFFecf0f1)),
                          padding: EdgeInsets.all(8.0),
                          width: screenWidth(context) / 1.8,
                          child: InkWell(
                            onTap: (() {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (context) => FullScreenImage(
                                            photoUrl: snapshot['photoUrl'],
                                          )));
                            }),
                            child: Hero(
                              tag: snapshot['photoUrl'],
                              child: FadeInImage(
                                image: NetworkImage(snapshot['photoUrl']),
                                placeholder:
                                    AssetImage('assets/blankimage.png'),
                                width: 200.0,
                                height: 200.0,
                              ),
                            ),
                          ))
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}

class ProgressIndicator extends StatelessWidget {
  final Stream<bool> stream;

  ProgressIndicator(this.stream);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: stream,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot.data) {
            return Container(
                color: AppColors.primaryColor,
                padding: EdgeInsets.all(16.0),
                child: Center(
                    child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                )));
          } else {
            return SizedBox();
          }
        });
  }
}

class EventsList extends StatelessWidget {
  final String customerId;

  EventsList(this.customerId);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<EventEntity>(
        future: ChatRepo().fetchEvents(customerId),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.status &&
                snapshot.data.data != null &&
                snapshot.data.data.isNotEmpty) {
              return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: snapshot.data.data.length + 1,
                  itemBuilder: (context, index) {
                    if (index == snapshot.data.data.length) {
                      return SizedBox(height: 0.0,);
                    } else {
                      return EventItem(snapshot.data.data[index].data);
                    }
                  });
            } else {
              return Visibility(visible: false,child: getNoEvent(context));
            }
          } else {
            return Visibility(visible: false,child: getNoEvent(context));
          }
        });
  }

  Widget getNoEvent(context) {
    return NoEventItem();
  }
}
