import 'package:evento_provider/model/event_entity.dart';
import 'package:flutter/material.dart';
import 'package:random_color/random_color.dart';

class NoEventItem extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    RandomColor _randomColor = RandomColor();

    Color _color = _randomColor.randomColor(
        colorHue: ColorHue.blue,
        colorBrightness: ColorBrightness.custom(Range(35, 40)));
    return Center(
      child: Builder(builder: (context) {
        return SizedBox(
          height: 120,
          width: 210,
          child: Card(
            color: _color,
            elevation: 10,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Text(
                "No Events Added By Customer.",
                style: TextStyle(color: Colors.white),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        );
      }),
    );
  }
}