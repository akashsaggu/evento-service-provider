import 'dart:async';
import 'dart:ui';

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/common/repo/UserRepository.dart';
import 'package:evento_provider/constants/PrefConstant.dart';
import 'package:evento_provider/screens/home/HomeOptionScreen.dart';
import 'package:evento_provider/screens/home/repo/NotificaionHandler.dart';
import 'package:evento_provider/screens/landing/GoThrough.dart';
import 'package:evento_provider/screens/signIn/SignInScreen.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:evento_provider/widgets/localizationBloc.dart';
import 'package:evento_provider/widgets/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashS extends StatefulWidget {
  final _SplashScreenState _splashScreenState = _SplashScreenState();
  final signIn = SignInScreen();

  @override
  State<StatefulWidget> createState() {
    return _splashScreenState;
  }
}

class _SplashScreenState extends State<SplashS> {
  void handleTimeout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => widget.signIn, maintainState: true));

    final bool isGoThroughDone = prefs.get(PrefConstant.GOTHROUGH) ?? false;
    UserRepo().getUserDetail().then((value) {
      if (value != null) {
        final homeScreen = HomeOptionScreen();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => homeScreen, maintainState: true));
      } else {
        if (isGoThroughDone) {
          final signInScreen = SignInScreen();
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => signInScreen, maintainState: true));
        } else {
          final goThrough = GoThrough();
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => goThrough, maintainState: true));
        }
      }
    });
  }

  startTimeout() async {
    var duration = const Duration(seconds: 3);
    return new Timer(duration, handleTimeout);
  }

  @override
  void initState() {
    super.initState();
    startTimeout();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: screenHeight(context),
        width: screenWidth(context),
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 0.5, sigmaY: 0.5),
              child: Container(
                decoration: BoxDecoration(gradient:LinearGradient(
                    colors: [Colors.blue[900], Colors.blueAccent],
                    begin: Alignment.bottomRight,
                    end: Alignment.topCenter)),
                child: Center(
                    child: Image.asset(
                  "assets/logo.png",
                  height: 250.0,
                  width: 250.0,
                )),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Splash extends StatelessWidget {
  final languageBloc = LocalizationBloc.instance;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LocalizationBloc>(
        bloc: languageBloc,
        child: MaterialApp(
          locale: Locale("en"),
          localizationsDelegates: [
            /*const TranslationsDelegate(),*/
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('ar', ''), // Arabic
            const Locale('en', ''), // English
          ],
          debugShowCheckedModeBanner: false,
          theme: hrTheme,
          title: "Evento",
          home: SplashS(),
        ));
  }
}
