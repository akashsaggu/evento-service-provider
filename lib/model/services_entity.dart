class ServicesEntity {
	List<ServicesData> data;
	String message;
	bool status;

	ServicesEntity({this.data, this.message, this.status});

	ServicesEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<ServicesData>();(json['data'] as List).forEach((v) { data.add(new ServicesData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class ServicesData {
	String updatedOn;
	String serviceLat;
	String serviceTitle;
	String utcDeletedOn;
	String serviceAddress;
	String serviceInstaPageId;
	String serviceHeading;
	String serviceWorkingDays;
	String subcategoryId;
	String categoryId;
	String serviceMobileNumbers;
	String serviceId;
	String serviceDefaultImage;
	String utcCreatedOn;
	String serviceDescription;
	int serviceRating;
	String utcUpdatedOn;
	String serviceWorkingHours;
	String deletedOn;
	String serviceFacebookPageId;
	String serviceLong;
	String createdOn;
	String providerId;
	bool rowStatus;
	String countryId;
	String cityId;

	ServicesData({this.updatedOn, this.serviceLat, this.serviceTitle, this.utcDeletedOn, this.serviceAddress, this.serviceInstaPageId, this.serviceHeading, this.serviceWorkingDays, this.subcategoryId, this.categoryId, this.serviceMobileNumbers, this.serviceId, this.serviceDefaultImage, this.utcCreatedOn, this.serviceDescription, this.serviceRating, this.utcUpdatedOn, this.serviceWorkingHours, this.deletedOn, this.serviceFacebookPageId, this.serviceLong, this.createdOn, this.providerId, this.rowStatus, this.countryId, this.cityId});

	ServicesData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		serviceLat = json['service_lat'];
		serviceTitle = json['service_title'];
		utcDeletedOn = json['utc_deleted_on'];
		serviceAddress = json['service_address'];
		serviceInstaPageId = json['service_insta_page_id'];
		serviceHeading = json['service_heading'];
		serviceWorkingDays = json['service_working_days'];
		subcategoryId = json['subcategory_id'];
		categoryId = json['category_id'];
		serviceMobileNumbers = json['service_mobile_numbers'];
		serviceId = json['service_id'];
		serviceDefaultImage = json['service_default_image'];
		utcCreatedOn = json['utc_created_on'];
		serviceDescription = json['service_description'];
		serviceRating = json['service_rating'];
		utcUpdatedOn = json['utc_updated_on'];
		serviceWorkingHours = json['service_working_hours'];
		deletedOn = json['deleted_on'];
		serviceFacebookPageId = json['service_facebook_page_id'];
		serviceLong = json['service_long'];
		createdOn = json['created_on'];
		providerId = json['provider_id'];
		rowStatus = json['row_status'];
		countryId = json['country_id'];
		cityId = json['city_id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['service_lat'] = this.serviceLat;
		data['service_title'] = this.serviceTitle;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['service_address'] = this.serviceAddress;
		data['service_insta_page_id'] = this.serviceInstaPageId;
		data['service_heading'] = this.serviceHeading;
		data['service_working_days'] = this.serviceWorkingDays;
		data['subcategory_id'] = this.subcategoryId;
		data['category_id'] = this.categoryId;
		data['service_mobile_numbers'] = this.serviceMobileNumbers;
		data['service_id'] = this.serviceId;
		data['service_default_image'] = this.serviceDefaultImage;
		data['utc_created_on'] = this.utcCreatedOn;
		data['service_description'] = this.serviceDescription;
		data['service_rating'] = this.serviceRating;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['service_working_hours'] = this.serviceWorkingHours;
		data['deleted_on'] = this.deletedOn;
		data['service_facebook_page_id'] = this.serviceFacebookPageId;
		data['service_long'] = this.serviceLong;
		data['created_on'] = this.createdOn;
		data['provider_id'] = this.providerId;
		data['row_status'] = this.rowStatus;
		data['country_id'] = this.countryId;
		data['city_id'] = this.cityId;
		return data;
	}
}
