class BannerRepoEntity {
  List<BannerRepoData> data;
  int totalBanner;
  String message;
  bool status;

  BannerRepoEntity({this.data, this.totalBanner, this.message, this.status = false});

  BannerRepoEntity.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<BannerRepoData>();
      (json['data'] as List).forEach((v) {
        data.add(new BannerRepoData.fromJson(v));
      });
    }
    totalBanner = json['total_banner'];
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['total_banner'] = this.totalBanner;
    data['message'] = this.message;
    data['status'] = this.status;
    return data;
  }
}

class BannerRepoData {
  String providerId;
  String bannerImage;
  String id;
  bool rowStatus;

  BannerRepoData({this.providerId, this.bannerImage, this.id, this.rowStatus});

  BannerRepoData.fromJson(Map<String, dynamic> json) {
    providerId = json['provider_id'];
    bannerImage = json['banner_image'];
    id = json['id'];
    rowStatus = json['row_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['provider_id'] = this.providerId;
    data['banner_image'] = this.bannerImage;
    data['id'] = this.id;
    data['row_status'] = this.rowStatus;
    return data;
  }
}
