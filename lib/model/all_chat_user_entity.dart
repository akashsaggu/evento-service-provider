class AllChatUserEntity {
	List<AllChatUserData> data;
	int totalChatUser;
	String message;
	bool status;

	AllChatUserEntity({this.data, this.totalChatUser, this.message, this.status});

	AllChatUserEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<AllChatUserData>();(json['data'] as List).forEach((v) { data.add(new AllChatUserData.fromJson(v)); });
		}
		totalChatUser = json['total_chat_user'];
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['total_chat_user'] = this.totalChatUser;
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class AllChatUserData {
	String updatedOn;
	String serviceTitle;
	String currDate;
	String gender;
	String deviceId;
	String dateOfBirth;
	int rating;
	String facebookLink;
	String long;
	String serviceWorkingDays;
	String lname;
	String cityName;
	String serviceOffer;
	String countryName;
	String fcmToken;
	String id;
	String lat;
	String email;
	String fname;
	String serviceDescription;
	String address;
	String serviceName;
	String approval;
	String profilePic;
	String mobile;
	String instaLink;
	String serviceStartingFrom;
	String currentDate;
	String createdOn;
	String tagline;
	String dateOfJoining;
	String serviceWorkingHour;
	String subcategory;
	String category;
	bool rowStatus;
	String countryId;
	String cityId;

	AllChatUserData({this.updatedOn, this.serviceTitle, this.currDate, this.gender, this.dateOfBirth, this.rating, this.facebookLink, this.long, this.serviceWorkingDays, this.lname, this.cityName, this.serviceOffer, this.countryName, this.fcmToken, this.id, this.lat, this.email, this.fname, this.serviceDescription, this.address, this.deviceId, this.serviceName, this.approval, this.profilePic, this.mobile, this.instaLink, this.serviceStartingFrom, this.currentDate, this.createdOn, this.tagline, this.dateOfJoining, this.serviceWorkingHour, this.subcategory, this.category, this.rowStatus, this.countryId, this.cityId});

	AllChatUserData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		serviceTitle = json['service_title'];
		currDate = json['curr_date'];
		gender = json['gender'];
		dateOfBirth = json['date_of_birth'];
		rating = json['rating'];
		facebookLink = json['facebook_link'];
		long = json['long'];
		serviceWorkingDays = json['service_working_days'];
		lname = json['lname'];
		cityName = json['city_name'];
		serviceOffer = json['service_offer'];
		countryName = json['country_name'];
		fcmToken = json['fcm_token'];
		id = json['id'];
		lat = json['lat'];
		email = json['email'];
		fname = json['fname'];
		serviceDescription = json['service_description'];
		address = json['address'];
		deviceId = json['device_id'];
		serviceName = json['service_name'];
		approval = json['approval'];
		profilePic = json['profile_pic'];
		mobile = json['mobile'];
		instaLink = json['insta_link'];
		serviceStartingFrom = json['service_starting_from'];
		currentDate = json['current_date'];
		createdOn = json['created_on'];
		tagline = json['tagline'];
		dateOfJoining = json['date_of_joining'];
		serviceWorkingHour = json['service_working_hour'];
		subcategory = json['subcategory'];
		category = json['category'];
		rowStatus = json['row_status'];
		countryId = json['country_id'];
		cityId = json['city_id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['service_title'] = this.serviceTitle;
		data['curr_date'] = this.currDate;
		data['gender'] = this.gender;
		data['device_id'] = this.deviceId;
		data['date_of_birth'] = this.dateOfBirth;
		data['rating'] = this.rating;
		data['facebook_link'] = this.facebookLink;
		data['long'] = this.long;
		data['service_working_days'] = this.serviceWorkingDays;
		data['lname'] = this.lname;
		data['city_name'] = this.cityName;
		data['service_offer'] = this.serviceOffer;
		data['country_name'] = this.countryName;
		data['fcm_token'] = this.fcmToken;
		data['id'] = this.id;
		data['lat'] = this.lat;
		data['email'] = this.email;
		data['fname'] = this.fname;
		data['service_description'] = this.serviceDescription;
		data['address'] = this.address;
		data['device_id'] = this.deviceId;
		data['service_name'] = this.serviceName;
		data['approval'] = this.approval;
		data['profile_pic'] = this.profilePic;
		data['mobile'] = this.mobile;
		data['insta_link'] = this.instaLink;
		data['service_starting_from'] = this.serviceStartingFrom;
		data['current_date'] = this.currentDate;
		data['created_on'] = this.createdOn;
		data['tagline'] = this.tagline;
		data['date_of_joining'] = this.dateOfJoining;
		data['service_working_hour'] = this.serviceWorkingHour;
		data['subcategory'] = this.subcategory;
		data['category'] = this.category;
		data['row_status'] = this.rowStatus;
		data['country_id'] = this.countryId;
		data['city_id'] = this.cityId;
		return data;
	}
}
