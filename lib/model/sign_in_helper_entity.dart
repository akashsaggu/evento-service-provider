class SignInHelperEntity {
	String password;
	String deviceId;
	String fcmToken;
	String email;

	SignInHelperEntity({this.password, this.deviceId, this.fcmToken, this.email});

	SignInHelperEntity.fromJson(Map<String, dynamic> json) {
		password = json['password'];
		deviceId = json['device_id'];
		fcmToken = json['fcm_token'];
		email = json['email'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['password'] = this.password;
		data['device_id'] = this.deviceId;
		data['fcm_token'] = this.fcmToken;
		data['email'] = this.email;
		return data;
	}
}
