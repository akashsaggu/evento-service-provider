class PackagesEntity {
	List<PackagesData> data;
	String message;
	bool status;

	PackagesEntity({this.data, this.message, this.status});

	PackagesEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<PackagesData>();(json['data'] as List).forEach((v) { data.add(new PackagesData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class PackagesData {
	var updatedOn;
	var utcCreatedOn;
	var utcDeletedOn;
	var utcUpdatedOn;
	String packagePrice;
	String packageDescription;
	String deletedOn;
	String createdOn;
	String serviceId;
	String packageName;
	String providerId;
	String id;
	bool rowStatus;

	PackagesData({this.updatedOn, this.utcCreatedOn, this.utcDeletedOn, this.utcUpdatedOn, this.packagePrice, this.packageDescription, this.deletedOn, this.createdOn, this.serviceId, this.packageName, this.providerId, this.id, this.rowStatus});

	PackagesData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		utcCreatedOn = json['utc_created_on'];
		utcDeletedOn = json['utc_deleted_on'];
		utcUpdatedOn = json['utc_updated_on'];
		packagePrice = json['package_price'];
		packageDescription = json['package_description'];
		deletedOn = json['deleted_on'];
		createdOn = json['created_on'];
		serviceId = json['service_id'];
		packageName = json['package_name'];
		providerId = json['provider_id'];
		id = json['id'];
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['utc_created_on'] = this.utcCreatedOn;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['package_price'] = this.packagePrice;
		data['package_description'] = this.packageDescription;
		data['deleted_on'] = this.deletedOn;
		data['created_on'] = this.createdOn;
		data['service_id'] = this.serviceId;
		data['package_name'] = this.packageName;
		data['provider_id'] = this.providerId;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		return data;
	}
}
