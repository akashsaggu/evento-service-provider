class VideoRepoEntity {
	List<VideoRepoData> data;
	String message;
	int totalVideos;
	bool status;

	VideoRepoEntity({this.data, this.message, this.totalVideos, this.status});

	VideoRepoEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<VideoRepoData>();(json['data'] as List).forEach((v) { data.add(new VideoRepoData.fromJson(v)); });
		}
		message = json['message'];
		totalVideos = json['total_videos'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['total_videos'] = this.totalVideos;
		data['status'] = this.status;
		return data;
	}
}

class VideoRepoData {
	String videoThumbnail;
	String videoLink;
	String providerId;
	String id;
	bool rowStatus;
	String videoType;

	VideoRepoData({ this.videoThumbnail, this.videoLink,  this.providerId, this.id, this.rowStatus, this.videoType});

	VideoRepoData.fromJson(Map<String, dynamic> json) {
		videoThumbnail = json['video_thumbnail'];
		videoLink = json['video_link'];
		providerId = json['provider_id'];
		id = json['id'];
		rowStatus = json['row_status'];
		videoType = json['video_type'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['video_thumbnail'] = this.videoThumbnail;
		data['video_link'] = this.videoLink;
		data['provider_id'] = this.providerId;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['video_type'] = this.videoType;
		return data;
	}
}
