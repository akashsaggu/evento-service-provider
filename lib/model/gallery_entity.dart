class GalleryEntity {
	List<GalleryData> data;
	String message;
	bool status;

	GalleryEntity({this.data, this.message, this.status = false});

	GalleryEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<GalleryData>();(json['data'] as List).forEach((v) { data.add(new GalleryData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class GalleryData {
	var utcCreatedOn;
	String updatedOn;
	String deletedOn;
	var utcDeletedOn;
	String createdOn;
	String albumImage;
	String albumName;
	String serviceId;
	String providerId;
	String id;
	bool rowStatus;
	var utcUpdatedOn;

	GalleryData({this.utcCreatedOn, this.updatedOn, this.deletedOn, this.utcDeletedOn, this.createdOn, this.albumImage, this.albumName, this.serviceId, this.providerId, this.id, this.rowStatus, this.utcUpdatedOn});

	GalleryData.fromJson(Map<String, dynamic> json) {
		utcCreatedOn = json['utc_created_on'];
		updatedOn = json['updated_on'];
		deletedOn = json['deleted_on'];
		utcDeletedOn = json['utc_deleted_on'];
		createdOn = json['created_on'];
		albumImage = json['album_image'];
		albumName = json['album_name'];
		serviceId = json['service_id'];
		providerId = json['provider_id'];
		id = json['id'];
		rowStatus = json['row_status'];
		utcUpdatedOn = json['utc_updated_on'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['utc_created_on'] = this.utcCreatedOn;
		data['updated_on'] = this.updatedOn;
		data['deleted_on'] = this.deletedOn;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['created_on'] = this.createdOn;
		data['album_image'] = this.albumImage;
		data['album_name'] = this.albumName;
		data['service_id'] = this.serviceId;
		data['provider_id'] = this.providerId;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['utc_updated_on'] = this.utcUpdatedOn;
		return data;
	}
}
