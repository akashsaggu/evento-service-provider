import 'package:evento_provider/widgets/spinner/SpinnerItem.dart';

class SubCategoryEntity {
	List<SubCategoryData> data;
	String message;
	bool status;

	SubCategoryEntity({this.data, this.message, this.status= false});

	SubCategoryEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<SubCategoryData>();(json['data'] as List).forEach((v) { data.add(new SubCategoryData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class SubCategoryData  implements SpinnerItem{
	String updatedOn;
	String deletedOn;
	String createdOn;
	String subcategoryName;
	String subcategoryPic;
	String id;
	bool rowStatus;

	SubCategoryData({ this.updatedOn, this.deletedOn, this.createdOn, this.subcategoryName, this.subcategoryPic, this.id, this.rowStatus});

	SubCategoryData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		deletedOn = json['deleted_on'];
		createdOn = json['created_on'];
		subcategoryName = json['subcategory_name'];
		subcategoryPic = json['subcategory_pic'];
		id = json['id'];
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['deleted_on'] = this.deletedOn;
		data['created_on'] = this.createdOn;
		data['subcategory_name'] = this.subcategoryName;
		data['subcategory_pic'] = this.subcategoryPic;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		return data;
	}

  @override
  String toValue() {
    return subcategoryName;
  }
}
