class ServiceAddHelperEntity {
	String serviceTitle;
	String serviceLat;
	String serviceDescription;
	String serviceAddress;
	String serviceInstaPageId;
	String serviceHeading;
	String serviceWorkingHours;
	String subcategoryId;
	String serviceWorkingDays;
	String currentDate;
	String serviceFacebookPageId;
	String categoryId;
	String serviceLong;
	String serviceMobileNumbers;
	String providerId;
	String serviceDefaultImage;
	String countryId;
	String serviceId;
	String cityId;

	ServiceAddHelperEntity({this.serviceTitle, this.serviceLat, this.serviceId, this.serviceDescription, this.serviceAddress, this.serviceInstaPageId, this.serviceHeading, this.serviceWorkingHours, this.subcategoryId, this.serviceWorkingDays, this.currentDate, this.serviceFacebookPageId, this.categoryId, this.serviceLong, this.serviceMobileNumbers, this.providerId, this.serviceDefaultImage, this.countryId, this.cityId});

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['service_title'] = this.serviceTitle;
		data['service_lat'] = this.serviceLat;
		data['service_description'] = this.serviceDescription;
		data['service_address'] = this.serviceAddress;
		data['service_insta_page_id'] = this.serviceInstaPageId;
		data['service_heading'] = this.serviceHeading;
		data['service_working_hours'] = this.serviceWorkingHours;
		data['subcategory_id'] = this.subcategoryId;
		data['service_working_days'] = this.serviceWorkingDays;
		data['current_date'] = this.currentDate;
		data['service_facebook_page_id'] = this.serviceFacebookPageId;
		data['category_id'] = this.categoryId;
		data['service_long'] = this.serviceLong;
		data['service_mobile_numbers'] = this.serviceMobileNumbers;
		data['provider_id'] = this.providerId;
		data['service_default_image'] = this.serviceDefaultImage;
		data['country_id'] = this.countryId;
		data['city_id'] = this.cityId;
		data['service_id'] = this.serviceId;
		return data;
	}
}
