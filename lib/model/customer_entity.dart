class CustomerEntity {
	CustomerData data;
	String message;
	bool status;

	CustomerEntity({this.data, this.message, this.status});

	CustomerEntity.fromJson(Map<String, dynamic> json) {
		data = json['data'] != null ? new CustomerData.fromJson(json['data']) : null;
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] = this.data.toJson();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class CustomerData {
	var updatedOn;
	String country;
	var utcDeletedOn;
	String city;
	int rating;
	String long;
	var utcLastLoggedIn;
	String password;
	String lname;
	String lastLoggedIn;
	String fcmToken;
	String id;
	String lat;
	String email;
	var utcCreatedOn;
	String fname;
	String address;
	String deviceId;
	String workingDays;
	String profilePic;
	String mobile;
	String approvedStatus;
	String facebookPageId;
	var utcUpdatedOn;
	String deletedOn;
	var createdOn;
	String workingHours;
	String category;
	String subcategory;
	bool rowStatus;
	String instagramPageId;

	CustomerData({this.updatedOn, this.country, this.utcDeletedOn, this.city, this.rating, this.long, this.utcLastLoggedIn, this.password, this.lname, this.lastLoggedIn, this.fcmToken, this.id, this.lat, this.email, this.utcCreatedOn, this.fname, this.address, this.deviceId, this.workingDays, this.profilePic, this.mobile, this.approvedStatus, this.facebookPageId, this.utcUpdatedOn, this.deletedOn, this.createdOn, this.workingHours, this.category, this.subcategory, this.rowStatus, this.instagramPageId});

	CustomerData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		country = json['country'];
		utcDeletedOn = json['utc_deleted_on'];
		city = json['city'];
		rating = json['rating'];
		long = json['long'];
		utcLastLoggedIn = json['utc_last_logged_in'];
		password = json['password'];
		lname = json['lname'];
		lastLoggedIn = json['last_logged_in'];
		fcmToken = json['fcm_token'];
		id = json['id'];
		lat = json['lat'];
		email = json['email'];
		utcCreatedOn = json['utc_created_on'];
		fname = json['fname'];
		address = json['address'];
		deviceId = json['device_id'];
		workingDays = json['working_days'];
		profilePic = json['profile_pic'];
		mobile = json['mobile'];
		approvedStatus = json['approved_status'];
		facebookPageId = json['facebook_page_id'];
		utcUpdatedOn = json['utc_updated_on'];
		deletedOn = json['deleted_on'];
		createdOn = json['created_on'];
		workingHours = json['working_hours'];
		category = json['category'];
		subcategory = json['subcategory'];
		rowStatus = json['row_status'];
		instagramPageId = json['instagram_page_id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['country'] = this.country;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['city'] = this.city;
		data['rating'] = this.rating;
		data['long'] = this.long;
		data['utc_last_logged_in'] = this.utcLastLoggedIn;
		data['password'] = this.password;
		data['lname'] = this.lname;
		data['last_logged_in'] = this.lastLoggedIn;
		data['fcm_token'] = this.fcmToken;
		data['id'] = this.id;
		data['lat'] = this.lat;
		data['email'] = this.email;
		data['utc_created_on'] = this.utcCreatedOn;
		data['fname'] = this.fname;
		data['address'] = this.address;
		data['device_id'] = this.deviceId;
		data['working_days'] = this.workingDays;
		data['profile_pic'] = this.profilePic;
		data['mobile'] = this.mobile;
		data['approved_status'] = this.approvedStatus;
		data['facebook_page_id'] = this.facebookPageId;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['deleted_on'] = this.deletedOn;
		data['created_on'] = this.createdOn;
		data['working_hours'] = this.workingHours;
		data['category'] = this.category;
		data['subcategory'] = this.subcategory;
		data['row_status'] = this.rowStatus;
		data['instagram_page_id'] = this.instagramPageId;
		return data;
	}
}
