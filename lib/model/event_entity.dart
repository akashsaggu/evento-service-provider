class EventEntity {
	int totalEvents;
	List<EventData> data;
	String message;
	bool status;

	EventEntity({this.totalEvents, this.data, this.message, this.status = false});

	EventEntity.fromJson(Map<String, dynamic> json) {
		totalEvents = json['total_events'];
		if (json['data'] != null) {
			data = new List<EventData>();(json['data'] as List).forEach((v) { data.add(new EventData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['total_events'] = this.totalEvents;
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class EventData {
	EventDataData data;
	String id;

	EventData({this.data, this.id});

	EventData.fromJson(Map<String, dynamic> json) {
		data = json['data'] != null ? new EventDataData.fromJson(json['data']) : null;
		id = json['id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] = this.data.toJson();
    }
		data['id'] = this.id;
		return data;
	}
}

class EventDataData {
	String deletedOn;
	String currentDate;
	String updated_on;
	String createdOn;
	String userId;
	String eventDate;
	String eventDescription;
	bool rowStatus;

	EventDataData({this.deletedOn, this.currentDate, this.updated_on, this.createdOn, this.userId, this.eventDate, this.eventDescription, this.rowStatus});

	EventDataData.fromJson(Map<String, dynamic> json) {
		deletedOn = json['deleted_on'];
		currentDate = json['current_date'];
		updated_on = json['updated-on'];
		createdOn = json['created_on'];
		userId = json['user_id'];
		eventDate = json['event_date'];
		eventDescription = json['event_description'];
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['deleted_on'] = this.deletedOn;
		data['current_date'] = this.currentDate;
		data['updated-on'] = this.updated_on;
		data['created_on'] = this.createdOn;
		data['user_id'] = this.userId;
		data['event_date'] = this.eventDate;
		data['event_description'] = this.eventDescription;
		data['row_status'] = this.rowStatus;
		return data;
	}
}
