class UserCalledEntity {
	List<UserCalledData> data;
	String message;
	bool status;

	UserCalledEntity({this.data, this.message, this.status});

	UserCalledEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<UserCalledData>();(json['data'] as List).forEach((v) { data.add(new UserCalledData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class UserCalledData {
	String updatedOn;
	var utcCreatedOn;
	var utcDeletedOn;
	UserCalledDataUserdata userData;
	String visitTime;
	UserCalledDataProviderdata providerData;
	String mobile;
	String utcUpdatedOn;
	String deletedOn;
	String createdOn;
	String userId;
	String providerId;
	String id;
	bool rowStatus;
	String visitDate;

	UserCalledData({this.updatedOn, this.utcCreatedOn, this.utcDeletedOn, this.userData, this.visitTime, this.providerData, this.mobile, this.utcUpdatedOn, this.deletedOn, this.createdOn, this.userId, this.providerId, this.id, this.rowStatus, this.visitDate});

	UserCalledData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		utcCreatedOn = json['utc_created_on'];
		utcDeletedOn = json['utc_deleted_on'];
		userData = json['userData'] != null ? new UserCalledDataUserdata.fromJson(json['userData']) : null;
		visitTime = json['visit_time'];
		providerData = json['providerData'] != null ? new UserCalledDataProviderdata.fromJson(json['providerData']) : null;
		mobile = json['mobile'];
		utcUpdatedOn = json['utc_updated_on'];
		deletedOn = json['deleted_on'];
		createdOn = json['created_on'];
		userId = json['user_id'];
		providerId = json['provider_id'];
		id = json['id'];
		rowStatus = json['row_status'];
		visitDate = json['visit_date'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['utc_created_on'] = this.utcCreatedOn;
		data['utc_deleted_on'] = this.utcDeletedOn;
		if (this.userData != null) {
      data['userData'] = this.userData.toJson();
    }
		data['visit_time'] = this.visitTime;
		if (this.providerData != null) {
      data['providerData'] = this.providerData.toJson();
    }
		data['mobile'] = this.mobile;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['deleted_on'] = this.deletedOn;
		data['created_on'] = this.createdOn;
		data['user_id'] = this.userId;
		data['provider_id'] = this.providerId;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['visit_date'] = this.visitDate;
		return data;
	}
}

class UserCalledDataUserdata {
	String utcCreatedOn;
	String updatedOn;
	String fname;
	String utcDeletedOn;
	String deviceId;
	String loginType;
	String selectedEvent;
	String profilePic;
	String mobile;
	String utcUpdatedOn;
	String utcLastLoggedIn;
	String deletedOn;
	String password;
	String lname;
	String socialToken;
	String lastLoggedIn;
	String createdOn;
	String fcmToken;
	String id;
	bool rowStatus;
	String email;

	UserCalledDataUserdata({this.utcCreatedOn, this.updatedOn, this.fname, this.utcDeletedOn, this.deviceId, this.loginType, this.selectedEvent, this.profilePic, this.mobile, this.utcUpdatedOn, this.utcLastLoggedIn, this.deletedOn, this.password, this.lname, this.socialToken, this.lastLoggedIn, this.createdOn, this.fcmToken, this.id, this.rowStatus, this.email});

	UserCalledDataUserdata.fromJson(Map<String, dynamic> json) {
		utcCreatedOn = json['utc_created_on'];
		updatedOn = json['updated_on'];
		fname = json['fname'];
		utcDeletedOn = json['utc_deleted_on'];
		deviceId = json['device_id'];
		loginType = json['login_type'];
		selectedEvent = json['selected_event'];
		profilePic = json['profile_pic'];
		mobile = json['mobile'];
		utcUpdatedOn = json['utc_updated_on'];
		utcLastLoggedIn = json['utc_last_logged_in'];
		deletedOn = json['deleted_on'];
		password = json['password'];
		lname = json['lname'];
		socialToken = json['social_token'];
		lastLoggedIn = json['last_logged_in'];
		createdOn = json['created_on'];
		fcmToken = json['fcm_token'];
		id = json['id'];
		rowStatus = json['row_status'];
		email = json['email'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['utc_created_on'] = this.utcCreatedOn;
		data['updated_on'] = this.updatedOn;
		data['fname'] = this.fname;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['device_id'] = this.deviceId;
		data['login_type'] = this.loginType;
		data['selected_event'] = this.selectedEvent;
		data['profile_pic'] = this.profilePic;
		data['mobile'] = this.mobile;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['utc_last_logged_in'] = this.utcLastLoggedIn;
		data['deleted_on'] = this.deletedOn;
		data['password'] = this.password;
		data['lname'] = this.lname;
		data['social_token'] = this.socialToken;
		data['last_logged_in'] = this.lastLoggedIn;
		data['created_on'] = this.createdOn;
		data['fcm_token'] = this.fcmToken;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['email'] = this.email;
		return data;
	}
}

class UserCalledDataProviderdata {
	String updatedOn;
	String country;
	String utcDeletedOn;
	String city;
	int rating;
	String long;
	String utcLastLoggedIn;
	String password;
	String lname;
	String lastLoggedIn;
	String fcmToken;
	String id;
	String lat;
	String email;
	String utcCreatedOn;
	String fname;
	String address;
	String deviceId;
	String workingDays;
	String profilePic;
	String mobile;
	String approvedStatus;
	String facebookPageId;
	int utcUpdatedOn;
	String deletedOn;
	String createdOn;
	String workingHours;
	String category;
	String subcategory;
	bool rowStatus;
	String instagramPageId;

	UserCalledDataProviderdata({this.updatedOn, this.country, this.utcDeletedOn, this.city, this.rating, this.long, this.utcLastLoggedIn, this.password, this.lname, this.lastLoggedIn, this.fcmToken, this.id, this.lat, this.email, this.utcCreatedOn, this.fname, this.address, this.deviceId, this.workingDays, this.profilePic, this.mobile, this.approvedStatus, this.facebookPageId, this.utcUpdatedOn, this.deletedOn, this.createdOn, this.workingHours, this.category, this.subcategory, this.rowStatus, this.instagramPageId});

	UserCalledDataProviderdata.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		country = json['country'];
		utcDeletedOn = json['utc_deleted_on'];
		city = json['city'];
		rating = json['rating'];
		long = json['long'];
		utcLastLoggedIn = json['utc_last_logged_in'];
		password = json['password'];
		lname = json['lname'];
		lastLoggedIn = json['last_logged_in'];
		fcmToken = json['fcm_token'];
		id = json['id'];
		lat = json['lat'];
		email = json['email'];
		utcCreatedOn = json['utc_created_on'];
		fname = json['fname'];
		address = json['address'];
		deviceId = json['device_id'];
		workingDays = json['working_days'];
		profilePic = json['profile_pic'];
		mobile = json['mobile'];
		approvedStatus = json['approved_status'];
		facebookPageId = json['facebook_page_id'];
		utcUpdatedOn = json['utc_updated_on'];
		deletedOn = json['deleted_on'];
		createdOn = json['created_on'];
		workingHours = json['working_hours'];
		category = json['category'];
		subcategory = json['subcategory'];
		rowStatus = json['row_status'];
		instagramPageId = json['instagram_page_id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['country'] = this.country;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['city'] = this.city;
		data['rating'] = this.rating;
		data['long'] = this.long;
		data['utc_last_logged_in'] = this.utcLastLoggedIn;
		data['password'] = this.password;
		data['lname'] = this.lname;
		data['last_logged_in'] = this.lastLoggedIn;
		data['fcm_token'] = this.fcmToken;
		data['id'] = this.id;
		data['lat'] = this.lat;
		data['email'] = this.email;
		data['utc_created_on'] = this.utcCreatedOn;
		data['fname'] = this.fname;
		data['address'] = this.address;
		data['device_id'] = this.deviceId;
		data['working_days'] = this.workingDays;
		data['profile_pic'] = this.profilePic;
		data['mobile'] = this.mobile;
		data['approved_status'] = this.approvedStatus;
		data['facebook_page_id'] = this.facebookPageId;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['deleted_on'] = this.deletedOn;
		data['created_on'] = this.createdOn;
		data['working_hours'] = this.workingHours;
		data['category'] = this.category;
		data['subcategory'] = this.subcategory;
		data['row_status'] = this.rowStatus;
		data['instagram_page_id'] = this.instagramPageId;
		return data;
	}
}
