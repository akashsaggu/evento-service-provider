class RegisterHelperEntity {
  String cityId;
  String providerID;
  String countryId;
  String long;
  String serviceWorkingDays;
  String fname;
  String lname;
  String password;
  String currentDate;
  String deviceId;
  String fcmToken;
  String facebookPageId;
  String email;
  String lat;
  String address;
  String rating;
  String mobile;
  String profilePic;
  String serviceStartingFrom;
  String category;
  String subCategory;
  String serviceWorkingHour;
  String instagramPageId;

  RegisterHelperEntity({

    this.fname,
    this.lname,
    this.password,
    this.currentDate,
    this.deviceId,
    this.fcmToken,
    this.facebookPageId,
    this.email,
    this.instagramPageId,
    this.countryId,
    this.cityId,
    this.category,
    this.subCategory,
    this.address,
    this.long,
    this.lat,
    this.mobile,
    this.rating,
    this.serviceWorkingDays,
    this.serviceWorkingHour,
    this.profilePic,
    this.providerID
  });

  RegisterHelperEntity.fromJson(Map<String, dynamic> json) {
    providerID = json['id'];
    fname = json['fname'];
    lname = json['lname'];
    email = json['email'];
    password = json['password'];
    deviceId = json['device_id'];
    fcmToken = json['fcm_token'];
    currentDate = json['current_date'];
    instagramPageId = json['instagram_page_id'];
    facebookPageId = json['facebook_page_id'];
    countryId = json['country'];
    cityId = json['city'];
    category = json['category']; //todo:
    subCategory = json['subcategory']; //todo:
    address = json['address']; //todo:
    long = json['long'];
    lat = json['lat']; //todo:
    mobile = json['mobile']; //todo:
    rating = json['rating'];
    serviceWorkingDays = json['working_days'];
    serviceWorkingHour = json['working_hours'];
    profilePic = json['profile_pic']; //todo:
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.providerID;
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    data['email'] = this.email;
    data['password'] = this.password;
    data['device_id'] = this.deviceId;
    data['fcm_token'] = this.fcmToken;
    data['current_date'] = this.currentDate;
    data['instagram_page_id'] = this.instagramPageId;
    data['facebook_page_id'] = this.facebookPageId;
    data['country'] = this.countryId;
    data['city'] = this.cityId;
    data['category'] = this.category;
    data['subcategory'] = this.subCategory;
    data['address'] = this.address;
    data['long'] = this.long;
    data['lat'] = this.lat;
    data['mobile'] = this.mobile;
    data['rating'] = this.rating;
    data['working_days'] = this.serviceWorkingDays;
    data['working_hours'] = this.serviceWorkingHour;
    data['profile_pic'] = this.profilePic;

    return data;
  }
}
