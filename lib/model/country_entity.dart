import 'package:evento_provider/widgets/spinner/SpinnerItem.dart';

class CountryEntity {
	List<CountryData> data;
	String message;
	bool status;

	CountryEntity({this.data, this.message, this.status = false});

	CountryEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<CountryData>();(json['data'] as List).forEach((v) { data.add(new CountryData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class CountryData  implements SpinnerItem{
	String updatedOn;
	String deletedOn;
	String createdOn;
	String countryName;
	String currency;
	String id;
	bool rowStatus;

	CountryData({this.updatedOn, this.deletedOn, this.createdOn, this.countryName, this.currency, this.id, this.rowStatus});

	CountryData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		deletedOn = json['deleted_on'];
		createdOn = json['created_on'];
		countryName = json['country_name'];
		currency = json['currency'];
		id = json['id'];
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['deleted_on'] = this.deletedOn;
		data['created_on'] = this.createdOn;
		data['country_name'] = this.countryName;
		data['currency'] = this.currency;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		return data;
	}

  @override
  String toValue() {
    return countryName;
  }
}
