class AlbumImageEntity {
	List<AlbumImageData> data;
	String message;
	bool status;

	AlbumImageEntity({this.data, this.message, this.status = false});

	AlbumImageEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<AlbumImageData>();(json['data'] as List).forEach((v) { data.add(new AlbumImageData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class AlbumImageData {
	var utcCreatedOn;
	String updatedOn;
	String utcDeletedOn;
	String imageTitle;
	String utcUpdatedOn;
	String deletedOn;
	String imageName;
	String createdOn;
	String serviceId;
	String providerId;
	String albumId;
	String id;
	bool rowStatus;

	AlbumImageData({this.utcCreatedOn, this.updatedOn, this.utcDeletedOn, this.imageTitle, this.utcUpdatedOn, this.deletedOn, this.imageName, this.createdOn, this.serviceId, this.providerId, this.albumId, this.id, this.rowStatus});

	AlbumImageData.fromJson(Map<String, dynamic> json) {
		utcCreatedOn = json['utc_created_on'];
		updatedOn = json['updated_on'];
		utcDeletedOn = json['utc_deleted_on'];
		imageTitle = json['image_title'];
		utcUpdatedOn = json['utc_updated_on'];
		deletedOn = json['deleted_on'];
		imageName = json['image_name'];
		createdOn = json['created_on'];
		serviceId = json['service_id'];
		providerId = json['provider_id'];
		albumId = json['album_id'];
		id = json['id'];
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['utc_created_on'] = this.utcCreatedOn;
		data['updated_on'] = this.updatedOn;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['image_title'] = this.imageTitle;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['deleted_on'] = this.deletedOn;
		data['image_name'] = this.imageName;
		data['created_on'] = this.createdOn;
		data['service_id'] = this.serviceId;
		data['provider_id'] = this.providerId;
		data['album_id'] = this.albumId;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		return data;
	}
}
