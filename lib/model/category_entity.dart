import 'package:evento_provider/widgets/spinner/SpinnerItem.dart';

class CategoryEntity  {
	int totalCategories;
	List<CategoryData> data;
	String message;
	bool status;

	CategoryEntity({this.totalCategories, this.data, this.message, this.status= false});

	CategoryEntity.fromJson(Map<String, dynamic> json) {
		totalCategories = json['total_categories'];
		if (json['data'] != null) {
			data = new List<CategoryData>();(json['data'] as List).forEach((v) { data.add(new CategoryData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['total_categories'] = this.totalCategories;
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}

}

class CategoryData implements SpinnerItem {
	String updatedOn;
	String deletedOn;
	String categoryNameEn;
	String categoryName;
	String createdOn;
	String categoryPic;
	CategoryDataSubcategorydata subCategoryData;
	String categoryNameAr;
	String id;
	bool rowStatus;

	CategoryData({this.updatedOn, this.deletedOn, this.categoryNameEn, this.categoryName, this.createdOn, this.categoryPic, this.subCategoryData, this.categoryNameAr, this.id, this.rowStatus});

	CategoryData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		deletedOn = json['deleted_on'];
		categoryNameEn = json['category_name_en'];
		categoryName = json['category_name'];
		createdOn = json['created_on'];
		categoryPic = json['category_pic'];
		subCategoryData = json['subCategoryData'] != null ? new CategoryDataSubcategorydata.fromJson(json['subCategoryData']) : null;
		categoryNameAr = json['category_name_ar'];
		id = json['id'];
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['deleted_on'] = this.deletedOn;
		data['category_name_en'] = this.categoryNameEn;
		data['category_name'] = this.categoryName;
		data['created_on'] = this.createdOn;
		data['category_pic'] = this.categoryPic;
		if (this.subCategoryData != null) {
      data['subCategoryData'] = this.subCategoryData.toJson();
    }
		data['category_name_ar'] = this.categoryNameAr;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		return data;
	}

  @override
  String toValue() {

    return categoryName;
  }
}

class CategoryDataSubcategorydata {


	//CategoryDataSubcategorydata({});

	CategoryDataSubcategorydata.fromJson(Map<String, dynamic> json) {
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		return data;
	}
}
