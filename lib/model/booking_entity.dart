class BookingEntity {
	List<BookingData> data;
	String message;
	bool status;

	BookingEntity({this.data, this.message, this.status});

	BookingEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<BookingData>();(json['data'] as List).forEach((v) { data.add(new BookingData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class BookingData {
	String utcCreatedOn;
	String updatedOn;
	String utcDeletedOn;
	BookingDataUserdata userData;
	String totalPrice;
	BookingDataProviderdata providerData;
	String bookingDate;
	String utcUpdatedOn;
	String bookingId;
	String deletedOn;
	String eventType;
	String eventId;
	BookingDataEventdata eventData;
	String userId;
	String createdOn;
	String serviceId;
	BookingDataServicedata serviceData;
	String providerId;
	BookingDataEventtypedata eventTypeData;
	bool rowStatus;

	BookingData({this.utcCreatedOn, this.updatedOn, this.utcDeletedOn, this.userData, this.totalPrice, this.providerData, this.bookingDate, this.utcUpdatedOn, this.bookingId, this.deletedOn, this.eventType, this.eventId, this.eventData, this.userId, this.createdOn, this.serviceId, this.serviceData, this.providerId, this.eventTypeData, this.rowStatus});

	BookingData.fromJson(Map<String, dynamic> json) {
		utcCreatedOn = json['utc_created_on'];
		updatedOn = json['updated_on'];
		utcDeletedOn = json['utc_deleted_on'];
		userData = json['userData'] != null ? new BookingDataUserdata.fromJson(json['userData']) : null;
		totalPrice = json['total_price'];
		providerData = json['providerData'] != null ? new BookingDataProviderdata.fromJson(json['providerData']) : null;
		bookingDate = json['booking_date'];
		utcUpdatedOn = json['utc_updated_on'];
		bookingId = json['booking_id'];
		deletedOn = json['deleted_on'];
		eventType = json['event_type'];
		eventId = json['event_id'];
		eventData = json['eventData'] != null ? new BookingDataEventdata.fromJson(json['eventData']) : null;
		userId = json['user_id'];
		createdOn = json['created_on'];
		serviceId = json['service_id'];
		serviceData = json['serviceData'] != null ? new BookingDataServicedata.fromJson(json['serviceData']) : null;
		providerId = json['provider_id'];
		eventTypeData = json['eventTypeData'] != null ? new BookingDataEventtypedata.fromJson(json['eventTypeData']) : null;
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['utc_created_on'] = this.utcCreatedOn;
		data['updated_on'] = this.updatedOn;
		data['utc_deleted_on'] = this.utcDeletedOn;
		if (this.userData != null) {
      data['userData'] = this.userData.toJson();
    }
		data['total_price'] = this.totalPrice;
		if (this.providerData != null) {
      data['providerData'] = this.providerData.toJson();
    }
		data['booking_date'] = this.bookingDate;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['booking_id'] = this.bookingId;
		data['deleted_on'] = this.deletedOn;
		data['event_type'] = this.eventType;
		data['event_id'] = this.eventId;
		if (this.eventData != null) {
      data['eventData'] = this.eventData.toJson();
    }
		data['user_id'] = this.userId;
		data['created_on'] = this.createdOn;
		data['service_id'] = this.serviceId;
		if (this.serviceData != null) {
      data['serviceData'] = this.serviceData.toJson();
    }
		data['provider_id'] = this.providerId;
		if (this.eventTypeData != null) {
      data['eventTypeData'] = this.eventTypeData.toJson();
    }
		data['row_status'] = this.rowStatus;
		return data;
	}
}

class BookingDataUserdata {
	String updatedOn;
	String utcCreatedOn;
	String fname;
	String profilePic;
	String utcDeletedOn;
	String deviceId;
	String loginType;
	String selectedEvent;
	String mobile;
	String utcUpdatedOn;
	String utcLastLoggedIn;
	String deletedOn;
	String socialToken;
	String password;
	String lname;
	String lastLoggedIn;
	String createdOn;
	String fcmToken;
	String id;
	bool rowStatus;
	String email;

	BookingDataUserdata({this.profilePic,this.updatedOn, this.utcCreatedOn, this.fname, this.utcDeletedOn, this.deviceId, this.loginType, this.selectedEvent, this.mobile, this.utcUpdatedOn, this.utcLastLoggedIn, this.deletedOn, this.socialToken, this.password, this.lname, this.lastLoggedIn, this.createdOn, this.fcmToken, this.id, this.rowStatus, this.email});

	BookingDataUserdata.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		utcCreatedOn = json['utc_created_on'];
		fname = json['fname'];
		utcDeletedOn = json['utc_deleted_on'];
		deviceId = json['device_id'];
		loginType = json['login_type'];
		selectedEvent = json['selected_event'];
		mobile = json['mobile'];
		utcUpdatedOn = json['utc_updated_on'];
		utcLastLoggedIn = json['utc_last_logged_in'];
		deletedOn = json['deleted_on'];
		socialToken = json['social_token'];
		password = json['password'];
		lname = json['lname'];
		profilePic = json['profile_pic'];
		lastLoggedIn = json['last_logged_in'];
		createdOn = json['created_on'];
		fcmToken = json['fcm_token'];
		id = json['id'];
		rowStatus = json['row_status'];
		email = json['email'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['utc_created_on'] = this.utcCreatedOn;
		data['fname'] = this.fname;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['device_id'] = this.deviceId;
		data['login_type'] = this.loginType;
		data['selected_event'] = this.selectedEvent;
		data['mobile'] = this.mobile;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['utc_last_logged_in'] = this.utcLastLoggedIn;
		data['deleted_on'] = this.deletedOn;
		data['social_token'] = this.socialToken;
		data['password'] = this.password;
		data['lname'] = this.lname;
		data['profile_pic'] = this.profilePic;
		data['last_logged_in'] = this.lastLoggedIn;
		data['created_on'] = this.createdOn;
		data['fcm_token'] = this.fcmToken;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['email'] = this.email;
		return data;
	}
}

class BookingDataProviderdata {
	String updatedOn;
	String country;
	String utcDeletedOn;
	String deviceId;
	String city;
	int rating;
	String long;
	String utcLastLoggedIn;
	String password;
	String lname;
	String lastLoggedIn;
	String fcmToken;
	String id;
	String lat;
	String email;
	String utcCreatedOn;
	String fname;
	String address;
	String workingDays;
	String profilePic;
	String mobile;
	String approvedStatus;
	String facebookPageId;
	int utcUpdatedOn;
	String deletedOn;
	String createdOn;
	String workingHours;
	String category;
	String subcategory;
	bool rowStatus;
	String instagramPageId;

	BookingDataProviderdata({this.updatedOn, this.country, this.utcDeletedOn, this.deviceId, this.city, this.rating, this.long, this.utcLastLoggedIn, this.password, this.lname, this.lastLoggedIn, this.fcmToken, this.id, this.lat, this.email, this.utcCreatedOn, this.fname, this.address, this.workingDays,this.profilePic, this.mobile, this.approvedStatus, this.facebookPageId, this.utcUpdatedOn, this.deletedOn, this.createdOn, this.workingHours, this.category, this.subcategory, this.rowStatus, this.instagramPageId});

	BookingDataProviderdata.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		country = json['country'];
		utcDeletedOn = json['utc_deleted_on'];
		deviceId = json['device_id'];
		city = json['city'];
		rating = json['rating'];
		long = json['long'];
		utcLastLoggedIn = json['utc_last_logged_in'];
		password = json['password'];
		lname = json['lname'];
		lastLoggedIn = json['last_logged_in'];
		fcmToken = json['fcm_token'];
		id = json['id'];
		lat = json['lat'];
		email = json['email'];
		utcCreatedOn = json['utc_created_on'];
		fname = json['fname'];
		address = json['address'];
		workingDays = json['working_days'];
		deviceId = json['device_id'];
		profilePic = json['profile_pic'];
		mobile = json['mobile'];
		approvedStatus = json['approved_status'];
		facebookPageId = json['facebook_page_id'];
		utcUpdatedOn = json['utc_updated_on'];
		deletedOn = json['deleted_on'];
		createdOn = json['created_on'];
		workingHours = json['working_hours'];
		category = json['category'];
		subcategory = json['subcategory'];
		rowStatus = json['row_status'];
		instagramPageId = json['instagram_page_id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['country'] = this.country;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['device_id'] = this.deviceId;
		data['city'] = this.city;
		data['rating'] = this.rating;
		data['long'] = this.long;
		data['utc_last_logged_in'] = this.utcLastLoggedIn;
		data['password'] = this.password;
		data['lname'] = this.lname;
		data['last_logged_in'] = this.lastLoggedIn;
		data['fcm_token'] = this.fcmToken;
		data['id'] = this.id;
		data['lat'] = this.lat;
		data['email'] = this.email;
		data['utc_created_on'] = this.utcCreatedOn;
		data['fname'] = this.fname;
		data['address'] = this.address;
		data['working_days'] = this.workingDays;
		data['device_id'] = this.deviceId;
		data['profile_pic'] = this.profilePic;
		data['mobile'] = this.mobile;
		data['approved_status'] = this.approvedStatus;
		data['facebook_page_id'] = this.facebookPageId;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['deleted_on'] = this.deletedOn;
		data['created_on'] = this.createdOn;
		data['working_hours'] = this.workingHours;
		data['category'] = this.category;
		data['subcategory'] = this.subcategory;
		data['row_status'] = this.rowStatus;
		data['instagram_page_id'] = this.instagramPageId;
		return data;
	}
}

class BookingDataEventdata {
	String updatedOn;
	String utcCreatedOn;
	String utcDeletedOn;
	String eventTitle;
	String utcUpdatedOn;
	String deletedOn;
	String eventType;
	String createdOn;
	String userId;
	String eventDate;
	String eventNumberGuests;
	String id;
	bool rowStatus;
	String countryId;
	String cityId;

	BookingDataEventdata({this.updatedOn, this.utcCreatedOn, this.utcDeletedOn, this.eventTitle, this.utcUpdatedOn, this.deletedOn, this.eventType, this.createdOn, this.userId, this.eventDate, this.eventNumberGuests, this.id, this.rowStatus, this.countryId, this.cityId});

	BookingDataEventdata.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		utcCreatedOn = json['utc_created_on'];
		utcDeletedOn = json['utc_deleted_on'];
		eventTitle = json['event_title'];
		utcUpdatedOn = json['utc_updated_on'];
		deletedOn = json['deleted_on'];
		eventType = json['event_type'];
		createdOn = json['created_on'];
		userId = json['user_id'];
		eventDate = json['event_date'];
		eventNumberGuests = json['event_number_guests'];
		id = json['id'];
		rowStatus = json['row_status'];
		countryId = json['country_id'];
		cityId = json['city_id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['utc_created_on'] = this.utcCreatedOn;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['event_title'] = this.eventTitle;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['deleted_on'] = this.deletedOn;
		data['event_type'] = this.eventType;
		data['created_on'] = this.createdOn;
		data['user_id'] = this.userId;
		data['event_date'] = this.eventDate;
		data['event_number_guests'] = this.eventNumberGuests;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['country_id'] = this.countryId;
		data['city_id'] = this.cityId;
		return data;
	}
}

class BookingDataServicedata {
	String utcCreatedOn;
	String updatedOn;
	String serviceTitle;
	String serviceDescription;
	String utcDeletedOn;
	String servicePrice;
	int serviceRating;
	String utcUpdatedOn;
	String serviceHeading;
	String deletedOn;
	String subcategoryId;
	String categoryId;
	String createdOn;
	String providerId;
	String serviceDefaultImage;
	String id;
	bool rowStatus;
	String countryId;
	String cityId;

	BookingDataServicedata({this.utcCreatedOn, this.updatedOn, this.serviceTitle, this.serviceDescription, this.utcDeletedOn, this.servicePrice, this.serviceRating, this.utcUpdatedOn, this.serviceHeading, this.deletedOn, this.subcategoryId, this.categoryId, this.createdOn, this.providerId, this.serviceDefaultImage, this.id, this.rowStatus, this.countryId, this.cityId});

	BookingDataServicedata.fromJson(Map<String, dynamic> json) {
		utcCreatedOn = json['utc_created_on'];
		updatedOn = json['updated_on'];
		serviceTitle = json['service_title'];
		serviceDescription = json['service_description'];
		utcDeletedOn = json['utc_deleted_on'];
		servicePrice = json['service_price'];
		serviceRating = json['service_rating'];
		utcUpdatedOn = json['utc_updated_on'];
		serviceHeading = json['service_heading'];
		deletedOn = json['deleted_on'];
		subcategoryId = json['subcategory_id'];
		categoryId = json['category_id'];
		createdOn = json['created_on'];
		providerId = json['provider_id'];
		serviceDefaultImage = json['service_default_image'];
		id = json['id'];
		rowStatus = json['row_status'];
		countryId = json['country_id'];
		cityId = json['city_id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['utc_created_on'] = this.utcCreatedOn;
		data['updated_on'] = this.updatedOn;
		data['service_title'] = this.serviceTitle;
		data['service_description'] = this.serviceDescription;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['service_price'] = this.servicePrice;
		data['service_rating'] = this.serviceRating;
		data['utc_updated_on'] = this.utcUpdatedOn;
		data['service_heading'] = this.serviceHeading;
		data['deleted_on'] = this.deletedOn;
		data['subcategory_id'] = this.subcategoryId;
		data['category_id'] = this.categoryId;
		data['created_on'] = this.createdOn;
		data['provider_id'] = this.providerId;
		data['service_default_image'] = this.serviceDefaultImage;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['country_id'] = this.countryId;
		data['city_id'] = this.cityId;
		return data;
	}
}

class BookingDataEventtypedata {
	String utcCreatedOn;
	String updatedOn;
	String deletedOn;
	String utcDeletedOn;
	String createdOn;
	String eventName;
	String id;
	bool rowStatus;
	String utcUpdatedOn;

	BookingDataEventtypedata({this.utcCreatedOn, this.updatedOn, this.deletedOn, this.utcDeletedOn, this.createdOn, this.eventName, this.id, this.rowStatus, this.utcUpdatedOn});

	BookingDataEventtypedata.fromJson(Map<String, dynamic> json) {
		utcCreatedOn = json['utc_created_on'];
		updatedOn = json['updated_on'];
		deletedOn = json['deleted_on'];
		utcDeletedOn = json['utc_deleted_on'];
		createdOn = json['created_on'];
		eventName = json['event_name'];
		id = json['id'];
		rowStatus = json['row_status'];
		utcUpdatedOn = json['utc_updated_on'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['utc_created_on'] = this.utcCreatedOn;
		data['updated_on'] = this.updatedOn;
		data['deleted_on'] = this.deletedOn;
		data['utc_deleted_on'] = this.utcDeletedOn;
		data['created_on'] = this.createdOn;
		data['event_name'] = this.eventName;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		data['utc_updated_on'] = this.utcUpdatedOn;
		return data;
	}
}
