import 'package:evento_provider/widgets/spinner/SpinnerItem.dart';

class CityEntity {
	List<CityData> data;
	String message;
	bool status;

	CityEntity({this.data, this.message, this.status= false});

	CityEntity.fromJson(Map<String, dynamic> json) {
		if (json['data'] != null) {
			data = new List<CityData>();(json['data'] as List).forEach((v) { data.add(new CityData.fromJson(v)); });
		}
		message = json['message'];
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
      data['data'] =  this.data.map((v) => v.toJson()).toList();
    }
		data['message'] = this.message;
		data['status'] = this.status;
		return data;
	}
}

class CityData  implements SpinnerItem{
	String updatedOn;
	String deletedOn;
	String cityName;
	String createdOn;
	String id;
	bool rowStatus;

	CityData({this.updatedOn, this.deletedOn, this.cityName, this.createdOn, this.id, this.rowStatus});

	CityData.fromJson(Map<String, dynamic> json) {
		updatedOn = json['updated_on'];
		deletedOn = json['deleted_on'];
		cityName = json['city_name'];
		createdOn = json['created_on'];
		id = json['id'];
		rowStatus = json['row_status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['updated_on'] = this.updatedOn;
		data['deleted_on'] = this.deletedOn;
		data['city_name'] = this.cityName;
		data['created_on'] = this.createdOn;
		data['id'] = this.id;
		data['row_status'] = this.rowStatus;
		return data;
	}
	@override
  String toValue() {
    return cityName;
  }

}
