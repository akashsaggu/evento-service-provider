import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:evento_provider/base/constants/ApiEndpoint.dart';
import 'package:intl/intl.dart';

class ApiHitter {
  static Dio _dio;

  static Dio getDio() {
    if (_dio == null) {
      BaseOptions options = new BaseOptions(
          baseUrl: ApiEndpoint.BaseUrl,
          connectTimeout: 30000,
          receiveTimeout: 30000);
      final now = new DateTime.now();
      final formatter = new DateFormat('yyyy-MM-dd H:m:s');
      _dio = new Dio(options)
        ..interceptors
            .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
          options.data["time_zone"] = DateTime.now().timeZoneName;
          options.data["timestamp"] = DateTime.now().millisecondsSinceEpoch;
          options.data["current_date"] = formatter.format(now);
          print(options.data);
          return options;
        }, onResponse: (Response response) {
          //   print(response.data.toString());
          //print(response);
          return response; // continue
        }, onError: (DioError e) {
          return e;
        }));
      return _dio;
    } else {
      return _dio;
    }
  }

  Future<ApiResponse> getApiResponse(String endPoint,
      {Map<String, dynamic> headers,
      Map<String, dynamic> queryParameters}) async {
    try {
      Response response = await getDio().get(endPoint,
          options: Options(headers: headers), queryParameters: queryParameters);
      return ApiResponse(true, response: response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return ApiResponse(false, msg: "$error");
    }
  }

  Future<ApiResponse> getPostApiResponse(String endPoint,
      {Map<String, dynamic> headers, Map<String, dynamic> data}) async {
    try {
      var response = await getDio()
          .post(endPoint, options: Options(headers: headers), data: data);
      return ApiResponse(true,
          response: response, msg: response.data["message"]);
    } catch (error, stacktrace) {
      try {
        return ApiResponse(false,
            msg: "${error?.response?.data ?? "Sorry Something went wrong."}");
      } catch (e) {
        return ApiResponse(false, msg: "Sorry Something went wrong.");
      }
    }
  }
}

class ApiResponse {
  final bool status;
  final String msg;
  final Response response;

  ApiResponse(this.status, {this.msg = "Success", this.response});
}
