class ApiEndpoint {
  static const String BaseUrl = "https://us-central1-evento-b05d7.cloudfunctions.net/webApi/api/v1/";
  static const String Categories = "getCategory";
  static const String SubCategories = "getSubCategory";
  static const String CountryList = "getCountry";
  static const String CityList = "getCity";
  static const String GetEvents = "getEvents";
  static const String AllChatUsers = "providerChatUsers";
  static const String ServiceVideoGet = "serviceVideoGet";
  static const String ServiceVideoRemove = "serviceVideoRemove";
  static const String ServiceVideoUpload = "serviceVideoUpload";
  static const String ServiceBannerImgGet = "serviceBannerImgGet";
  static const String ServiceBannerImgRemove = "serviceBannerImgRemove";
  static const String ServiceBannerImgAdd = "serviceBannerImgAdd";
  static const String ProviderSignUP = "providerSignUp";
  static const String UpdateServiceProvider = "updateServiceProvider";
  static const String ProviderSignIn = "providerSignIn";
  static const String AddService = "serviceAdd";
  static const String UpdateService = "serviceUpdate";
  static const String GetProviderServices = "getProviderServices";
  static const String DeleteService = "serviceDelete";
  static const String GetProviderBookings = "getProviderBookings";
  static const String ServiceAlbumImagesGet = "serviceAlbumImagesGet";
  static const String ServiceAlbumGet = "serviceAlbumGet";
  static const String UpdateServiceProviderFcm = "updateServiceProviderFcm";
  static const String GetUserVisitorProviderLogsData = "getUserVisitorProviderLogsData";
  static const String ServiceAlbumAdd = "serviceAlbumAdd";
  static const String ServiceAlbumDelete = "serviceAlbumDelete";
  static const String ServiceAlbumImageAdd = "serviceAlbumImageAdd";
  static const String ServiceAlbumImageDelete = "serviceAlbumImageDelete";
  static const String ServicePackageDelete = "servicePackageDelete";
  static const String servicePackageGet = "servicePackageGet";
  static const String servicePackageAdd = "servicePackageAdd";

}
