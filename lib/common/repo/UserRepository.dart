import 'dart:convert';

import 'package:evento_provider/constants/PrefConstant.dart';
import 'package:evento_provider/model/customer_entity.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../GlobalCustomer.dart';

class UserRepo {
  Future<CustomerEntity> getUserDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userJsonString = prefs.getString(PrefConstant.USER);
    if (userJsonString != null && userJsonString.isNotEmpty) {
      var loginJson = await json.decode(prefs.getString(PrefConstant.USER));
      CustomerEntity loginData = CustomerEntity.fromJson(loginJson);
      GlobalCustomer.customerEntity = loginData;
      return loginData;
    } else {
      GlobalCustomer.customerEntity = null;
      return null;
    }
  }

  Future<bool> clearPreference() async {
    GlobalCustomer.customerEntity = null;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }

  Future<bool> saveUser(CustomerEntity userData) async {
    GlobalCustomer.customerEntity = userData;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PrefConstant.USER, json.encode(userData.toJson()));
  }
}
