import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:rxdart/rxdart.dart';

class AddressBloc extends BlocBase {

  final locationTextStream = BehaviorSubject<String>();


  setLocationText(String location) {
    locationTextStream.add(location);
  }

  @override
  void dispose() {

  }

}