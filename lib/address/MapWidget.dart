import 'dart:async';

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'AddressBloc/AddressBloc.dart';


final houseTextController = TextEditingController();
final landmarkController = TextEditingController();
final addressData = AddressHelper();
double lat;
double long;
String location = "";


class MapWidget extends StatelessWidget {
  final AddressBloc addressBloc = AddressBloc();
  final bool isFromCheckOut;


  MapWidget({this.isFromCheckOut = false});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddressBloc>(
      bloc: addressBloc,
      child: WillPopScope(
        onWillPop: () {
          houseTextController.clear();
          landmarkController.clear();
          lat = null;
          long = null;
          location = "";
          Navigator.of(context).pop();
        },
        child: Scaffold(
            body: Container(
          height: screenHeight(context),
          width: screenWidth(context),
          child: Stack(
            children: <Widget>[
              SafeArea(
                child: Align(
                    child: FractionallySizedBox(
                        child: MapSample(), heightFactor: 0.55),
                    alignment: Alignment.topCenter),
              ),
              Align(
                  child: FractionallySizedBox(
                      child: AddressWidget(isFromCheckOut: isFromCheckOut),
                      heightFactor: 0.45),
                  alignment: Alignment.bottomCenter)
            ],
          ),
        )),
      ),
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();
  CameraPosition _position;

  @override
  Widget build(BuildContext context) {
    final locationBloc = BlocProvider.of<AddressBloc>(context);
    _gotToTheSchool();

    return Stack(
      children: <Widget>[
        GoogleMap(
          myLocationEnabled: true,
          mapType: MapType.normal,
          myLocationButtonEnabled: true,
          initialCameraPosition: CameraPosition(
            target: LatLng(30.7116, 76.7179),
            zoom: 16.0,
          ),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          onCameraMove: (position) async {
            _position = position;
          },
          onCameraIdle: () async {
            if (_position != null) {
              lat = _position.target.latitude;
              addressData.lat=_position.target.latitude.toString();
              long = _position.target.longitude;
              addressData.long=_position.target.longitude.toString();
              final coordinates = new Coordinates(lat, long);
              List<Address> addresses = (await Geocoder.local.findAddressesFromCoordinates(coordinates)).cast<Address>();
              var first = addresses.first;
              location = first.addressLine;
              locationBloc.setLocationText(first.addressLine);
              print("${first.featureName} : ${first.addressLine}");
            }
          },
        ),
        Align(
            child: Image.asset("assets/pin.png",
                height: 55.0, width: 55.0),
            alignment: Alignment.center)
      ],
    );
  }

  Future<void> _gotToTheSchool() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(30.7117, 76.7180),
        tilt: 59.440717697143555,
        zoom: 16.0)));
  }
}

class AddressWidget extends StatelessWidget {
  final bool isFromCheckOut;

  AddressWidget({this.isFromCheckOut = false});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Material(
        elevation: 16.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(22.0)),
        child: Container(
          height: 380.0,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(22.0),
                  topRight: Radius.circular(22.0))),
          padding: EdgeInsets.all(16.0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Set service location".toUpperCase(),
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black)),
                Text(
                  "Location",
                  style: TextStyle(color: Colors.blueGrey, fontSize: 16.0),
                ),
                LocationText(),
                /*TextFormField(
                  controller: houseTextController,
                  decoration: InputDecoration(
                    labelText: "".toUpperCase(),
                    labelStyle: TextStyle(color: Colors.blueGrey),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            width: 0.0, color: AppColors.primaryColor)),
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.0, color: AppColors.lightGrey),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12.0),
                            bottomRight: Radius.circular(12.0))),
                    disabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.0, color: AppColors.lightGrey),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12.0),
                            bottomRight: Radius.circular(12.0))),
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.0, color: AppColors.lightGrey),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12.0),
                            bottomRight: Radius.circular(12.0))),
                  ),
                ),*/
                TextFormField(
                  controller: landmarkController,
                  decoration: InputDecoration(
                    labelText: "Landmark".toUpperCase(),
                    labelStyle: TextStyle(color: Colors.blueGrey),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            width: 0.0, color: AppColors.primaryColor)),
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.0, color: AppColors.lightGrey),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12.0),
                            bottomRight: Radius.circular(12.0))),
                    disabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.0, color: AppColors.lightGrey),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12.0),
                            bottomRight: Radius.circular(12.0))),
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.0, color: AppColors.lightGrey),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12.0),
                            bottomRight: Radius.circular(12.0))),
                  ),
                ),
                InkWell(
                  onTap: () {
                    final houseString = houseTextController.text;
                    final landmarkString = landmarkController.text;
                    final address =
                        "$location ${houseString.isNotEmpty ? ", House/Flat no. $houseString" : ""} ${houseString.isNotEmpty ? ",Landmark $landmarkString" : ""}";
                    addressData.address = address;
                      Navigator.pop(context, addressData);
                  },
                  child: Center(
                      child: Container(
                    width: screenWidth(context) * 0.85,
                    height: 48.0,
                    decoration: BoxDecoration(
                        color: AppColors.primaryColor,
                        borderRadius: BorderRadius.circular(8.0),
                        shape: BoxShape.rectangle),
                    child: Center(
                      child: Text(
                        "Submit".toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )),
                )
              ]),
        ),
      ),
    );
  }
}

class LocationText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locationBloc = BlocProvider.of<AddressBloc>(context);
    return StreamBuilder<String>(
        stream: locationBloc.locationTextStream.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Text(snapshot.data,
                maxLines: 3, overflow: TextOverflow.ellipsis);
          } else {
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(AppColors.primaryColor),
            ));
          }
        });
  }
}

class AddressHelper {
  String address = "";
  String lat = "";
  String long = "";
  String first = "";
  String addressLine = "";
  String featureName = "";

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['lat'] = this.lat;
    data['long'] = this.long;

    return data;
  }
}
