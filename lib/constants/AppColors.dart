import 'package:flutter/material.dart';

class AppColors {

  static Color primaryColor = Color(0xff5486f5);
  static Color lightGrey = Color(0xffe3e8ec);

}