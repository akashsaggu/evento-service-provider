import 'package:evento_provider/address/MapWidget.dart';
import 'package:evento_provider/common/GlobalCustomer.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';

// ignore: must_be_immutable
class AddresWidget extends StatefulWidget {
  ValueChanged<AddressHelper> selectedAddress;
  String address;

  AddresWidget(this.selectedAddress, {this.address = "Select Address"});

  @override
  _AddresWidgetState createState() => _AddresWidgetState();
}

class _AddresWidgetState extends State<AddresWidget> {
  String address;

  @override
  void initState() {
    address = widget.address;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
      const EdgeInsets.only(top: 2.0, left: 8.0, right: 8.0, bottom: 8.0),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                address ?? "Select Address",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
                height: 40,
                width: GlobalCustomer.customerEntity.data.address.isEmpty?130:80,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  color: Colors.blueAccent,
                  onPressed: () async {
                    checkAndAskPermission(context);
                  },
                  elevation: 10,
                  child: Text(
                    GlobalCustomer.customerEntity.data.address.isEmpty?"SELECT ADDRESS":"CHANGE",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                )),
          )
        ],
      ),
    );
  }

  checkAndAskPermission(context) async {
    final mapWidget = MapWidget();
    final hasPermission =
    await SimplePermissions.checkPermission(Permission.AccessFineLocation);

    if (hasPermission) {
      final AddressHelper addresss = await Navigator.push(
          context, MaterialPageRoute(builder: (context) => mapWidget));

      if (address != null) {
        setState(() {
          address = addresss.address;
        });
        widget.selectedAddress(addresss);
      }
    } else {
      final permissionStatus = await SimplePermissions.requestPermission(
          Permission.AccessFineLocation);
      if (permissionStatus == PermissionStatus.authorized ||
          permissionStatus == PermissionStatus.notDetermined) {
        final AddressHelper addresss = await Navigator.push(
            context, MaterialPageRoute(builder: (context) => mapWidget));

        if (address != null) {
          setState(() {
            address = addresss.address;
          });
          widget.selectedAddress(addresss);
        }
      } else {
        toast(context, "Please provide location permission.");
      }
    }
  }
}
