import 'package:flutter/material.dart';

import 'CountryPickerWidget.dart';

// ignore: must_be_immutable
class MultipleMobileNoWidget extends StatefulWidget {
  List<MobileForm> mobileForms = [];
  List<PreSelectedMobile> preSelectedMobiles;

  MultipleMobileNoWidget({this.preSelectedMobiles = const []});

  @override
  State<StatefulWidget> createState() {
    return _MultipleMobileNoWidgetState((value) {
      mobileForms = value;
    });
  }
}

class _MultipleMobileNoWidgetState extends State<MultipleMobileNoWidget> {
  List<MobileForm> mobileForms = [];
  ValueChanged<List<MobileForm>> mobileFormChangeListener;

  _MultipleMobileNoWidgetState(this.mobileFormChangeListener);

  @override
  void initState() {
    mobileForms = [];
    if (widget.preSelectedMobiles.isEmpty) {
      mobileForms.add(MobileForm.prepareMobileForm("+20",
          TextEditingController(), MobileFormType.Add,
              (type, mobileFormWidget) {
            setState(() {
              getRemoveWidget("");
            });
          }));
    } else {
    for (int i = 0; i < widget.preSelectedMobiles.length; i++) {
    if (i == 0) {
    mobileForms.add(MobileForm.prepareMobileForm(
    widget.preSelectedMobiles[i].countryCode,TextEditingController()
    ..text = widget.preSelectedMobiles[i].mobile, MobileFormType.Add, (
    type, mobileFormWidget) {
    getRemoveWidget(widget.preSelectedMobiles[i].mobile, country: widget.preSelectedMobiles[i].countryCode);
    }));
    } else {
    getRemoveWidget(widget.preSelectedMobiles[i].mobile, country: widget.preSelectedMobiles[i].countryCode);
    }
    }
    }
  }

  void getRemoveWidget(value, {country = "+20"}) {
    mobileForms.add(
        MobileForm.prepareMobileForm(country, TextEditingController()
          ..text = value, MobileFormType.Remove,
                (type, mobileFormWidget) {
              setState(() {
                widget.mobileForms.remove(widget.mobileForms
                    .firstWhere((value) =>
                value.formWidget == mobileFormWidget));
              });
            }));
  }

  @override
  Widget build(BuildContext context) {
    mobileFormChangeListener(mobileForms);
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: mobileForms.length,
        itemBuilder: (context, index) => mobileForms[index].formWidget);
  }
}

class MobileForm {
  final TextEditingController controller;
  final MobileFormWidget formWidget;

  MobileForm(this.controller, this.formWidget);

  static MobileForm prepareMobileForm(String country,
      TextEditingController controller, MobileFormType type,
      Function(MobileFormType, MobileFormWidget) callback) {
    final textController = controller;
    return MobileForm(
        textController,
        MobileFormWidget(type, textController, callback, country));
  }
}

enum MobileFormType { Add, Remove }

// ignore: must_be_immutable
class MobileFormWidget extends StatelessWidget {
  final MobileFormType type;
  final TextEditingController textEditingController;
  final Function(MobileFormType, MobileFormWidget) callback;
  String countryCode;

  MobileFormWidget(this.type, this.textEditingController, this.callback,
      this.countryCode);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textEditingController,
      style: TextStyle(color: Colors.white),
      keyboardType: TextInputType.number,
      autofocus: false,
      maxLength: 10,
      decoration: InputDecoration(
        prefixIcon: CountryPickerWidget(countryCode, (value) {
          countryCode = value.dialCode;
        }),
        suffixIcon: InkWell(
          onTap: () {
            callback(type, this);
          },
          child: Icon(
            type == MobileFormType.Add ? Icons.add : Icons.remove,
            color: Colors.white,
          ),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        fillColor: Colors.blueAccent,
        hintText: 'Mobile number',
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.fromLTRB(10.0, 12.0, 10.0, 12.0),
      ),);
  }
}


class PreSelectedMobile {
  final String countryCode;
  final String mobile;

  PreSelectedMobile({this.countryCode, this.mobile});
}