import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TimeBottomSheet extends StatefulWidget {
  ValueChanged<String> selectListener;
  String selectedTime;

  TimeBottomSheet(this.selectListener, this.selectedTime);

  @override
  _TimeBottomSheetState createState() => _TimeBottomSheetState();
}

class _TimeBottomSheetState extends State<TimeBottomSheet> {
  String selected;

  @override
  Widget build(BuildContext context) {
    List<String> timing = [
      "08:00 - 09:00",
      "09:00 - 10:00",
      "10:00 - 11:00",
      "11:00 - 12:00",
      "12:00 - 13:00",
      "13:00 - 14:00",
      "14:00 - 15:00",
      "15:00 - 16:00",
      "16:00 - 17:00",
      "17:00 - 18:00",
      "18:00 - 19:00",
      "19:00 - 20:00",
      "20:00 - 21:00",
    ];
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
        child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            onPressed: () => Scaffold.of(context)
                    .showBottomSheet<Null>((BuildContext context) {
                  return SizedBox(
                    width: screenWidth(context),
                    child: new Container(
                        child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: InkWell(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Icon(
                                    Icons.arrow_back,
                                    color: Colors.blue,
                                  )),
                            ),
                            Expanded(
                              child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: new Text(
                                    "CHOOSE TIME",
                                    textAlign: TextAlign.center,
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.transparent,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 300,
                          child: GridView.builder(
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                  childAspectRatio:2.0 ,
                                    crossAxisCount: 3),
                            itemBuilder: (context, index) => InkWell(
                              onTap: () {
                                setState(() {
                                  widget.selectedTime = timing[index];
                                  widget.selectListener(timing[index]);
                                  Navigator.pop(context);
                                });
                              },
                              child: Card(
                                color: Colors.blue,
                                elevation: 6,
                                child: Center(
                                    child: Text(
                                  timing[index],
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                )),
                              ),
                            ),
                            itemCount: timing.length,
                          ),
                        )
                      ],
                    )),
                  );
                }),
            color: Colors.blueAccent,
            child: Text(
              widget.selectedTime,
              style: TextStyle(color: Colors.white),
            )),
      ),
    );
  }
}
