import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/bloc/CategoryBloc.dart';
import 'package:evento_provider/model/category_entity.dart';
import 'package:evento_provider/widgets/spinner/DropDownSpinner.dart';
import 'package:evento_provider/widgets/spinner/SpinnerItem.dart';
import 'package:flutter/material.dart';


// ignore: must_be_immutable
class CategorySpinner extends StatelessWidget {
  final ValueChanged<CategoryData> valueChanged;
  String preSelectedCategory;
  int selectedIndex;

  CategorySpinner(this.valueChanged, {this.preSelectedCategory});

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CategoryBloc>(context);
    return StreamBuilder<CategoryEntity>(
        stream: bloc.categoryBloc.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.status && snapshot.data.data.isNotEmpty) {
              if (preSelectedCategory != null) {
                try {
                  selectedIndex = snapshot.data.data
                      .indexWhere((category) =>
                  category.id == preSelectedCategory);
                } catch (e) {
                  selectedIndex = 0;
                }
              } else {
                selectedIndex = 0;
              }
              if (selectedIndex == -1) {
                selectedIndex = 0;
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8.0),
                      child: Text("Select Category", style: TextStyle(color: Colors.white))),
                  DropDownSpinner(
                    snapshot.data.data,
                        (value) {
                      bloc.getSubCategories(value.id.toString());
                      valueChanged(value);
                    },
                    selectPos: selectedIndex,
                  ),
                ],
              );
            } else {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8.0),
                      child: Text("Select Category", style: TextStyle(color: Colors.white))),
                  DropDownSpinner([DropDownItem("")], (value) {}),
                ],
              );
            }
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8.0),
                    child: Text("Select Category", style: TextStyle(color: Colors.white))),
                DropDownSpinner([DropDownItem("")], (value) {}),
              ],
            );
          }
        });
  }
}

class DropDownItem implements SpinnerItem {
  String title;
  String message;

  DropDownItem(this.title, {this.message = ""});

  @override
  String toValue() {
    return title;
  }
}