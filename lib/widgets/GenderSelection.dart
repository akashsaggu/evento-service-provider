


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GenderSelection extends StatefulWidget {

  ValueChanged<String> selectListener;
  String selectedValue="Male";


  GenderSelection(this.selectListener);

  @override
  _GenderSelectionState createState() => _GenderSelectionState();
}

class _GenderSelectionState extends State<GenderSelection> {

  String gender="Male";
  Row add_radio_button(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Colors.white,
          value: btnValue,
          groupValue: gender=="Male"?0:1,
          onChanged: _handleRadiobutton,
        ),
        Text(
          title,
          style: TextStyle(color: Colors.white),
        )
      ],
    );
  }

  void _handleRadiobutton(int value) {
    setState(() {
      value==0?gender="Male":gender="Female";
      widget.selectedValue=gender;
      widget.selectListener(gender);
    });
  }



  @override
  Widget build(BuildContext context) {
    return Container(child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Gender*',
          style: TextStyle(color: Colors.white),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            add_radio_button(0, 'Male'),
            add_radio_button(1, 'Female'),
          ],
        ),
      ],
    ),);
  }
}
