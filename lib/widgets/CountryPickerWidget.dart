import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';

class CountryPickerWidget extends StatefulWidget {
  final ValueChanged<CountryCode> countryCodeCallback;
  final intitalCountry;

  CountryPickerWidget(this.intitalCountry,this.countryCodeCallback);

  @override
  _CountryPickerWidgetState createState() => _CountryPickerWidgetState();
}

class _CountryPickerWidgetState extends State<CountryPickerWidget> {
  CountryCode selcode;

  @override
  Widget build(BuildContext context) {
    final countryPicker = CountryCodePicker(
      onChanged: (code) {
        setState(() {
          selcode = code;
        });
        widget.countryCodeCallback(code);
      },
      textStyle: TextStyle(color: Colors.white),
      showFlag: true,
      initialSelection: widget.intitalCountry,
      favorite: ['+20'],
      showCountryOnly: false,
      showOnlyCountryWhenClosed: false,
      alignLeft: false,
      padding: EdgeInsets.all(0),
    );
    return countryPicker;
  }
}
