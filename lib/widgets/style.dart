import 'package:evento_provider/constants/AppColors.dart';
import 'package:flutter/material.dart';


final ThemeData hrTheme = _buildHRTheme();

ThemeData _buildHRTheme() {
  return ThemeData(
      brightness: Brightness.light,
      primaryColor: AppColors.primaryColor,
      primaryColorDark: AppColors.primaryColor,
      inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(0.0)))));
}
