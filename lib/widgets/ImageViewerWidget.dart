import 'package:cached_network_image/cached_network_image.dart';
import 'package:evento_provider/model/album_image_entity.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

class ImageViewerWidget extends StatelessWidget {
  final List<AlbumImageData> items;
  final int index;

  ImageViewerWidget(this.items, this.index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: screenWidth(context),
            color: Colors.black,
            child: Align(
              alignment: AlignmentDirectional.centerStart,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, top: 36.0),
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Flexible(
            child: Container(
                child: PhotoViewGallery.builder(
              builder: (BuildContext context, int index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider:
                      CachedNetworkImageProvider(items[index].imageName),
                );
              },
              itemCount: items.length,
              pageController: PageController(initialPage: index),
            )),
          )
        ],
      ),
    );
  }
}
