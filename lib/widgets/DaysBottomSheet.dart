import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DaysBottomSheet extends StatefulWidget {
  ValueChanged<String> selectListener;
  String selectedValue;

  DaysBottomSheet(this.selectListener, this.selectedValue);

  @override
  _DaysBottomSheetState createState() => _DaysBottomSheetState();
}

class _DaysBottomSheetState extends State<DaysBottomSheet> {
  void weekBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
              color: Colors.blueAccent,
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    title: new Text('Sunday'),
                    onTap: () => {
                          Navigator.pop(context),
                          setState(() {
                            widget.selectedValue = "Sunday";
                            widget.selectListener("Sunday");
                          })
                        }),
                new ListTile(
                  title: new Text('Monday'),
                  onTap: () => {
                        Navigator.pop(context),
                        setState(() {
                          widget.selectedValue = "Monday";
                          widget.selectListener("Monday");
                        })
                      },
                ),
                new ListTile(
                    title: new Text('Tuesday'),
                    onTap: () => {
                          Navigator.pop(context),
                          setState(() {
                            widget.selectedValue = "Tuesday";
                            widget.selectListener("Tuesday");
                          })
                        }),
                new ListTile(
                  title: new Text('Wednesday'),
                  onTap: () => {
                        Navigator.pop(context),
                        setState(() {
                          widget.selectedValue = "Wednesday";
                          widget.selectListener("Wednesday");
                        })
                      },
                ),
                new ListTile(
                    title: new Text('Thrusday'),
                    onTap: () => {
                          Navigator.pop(context),
                          setState(() {
                            widget.selectedValue = "Thrusday";
                            widget.selectListener("Thrusday");
                          })
                        }),
                new ListTile(
                  title: new Text('Friday'),
                  onTap: () => {
                        Navigator.pop(context),
                        setState(() {
                          widget.selectedValue = "Friday";
                          widget.selectListener("Friday");
                        })
                      },
                ),
                new ListTile(
                  title: new Text('Saturday'),
                  onTap: () => {
                        Navigator.pop(context),
                        setState(() {
                          widget.selectedValue = "Saturday";
                          widget.selectListener("Saturday");
                        })
                      },
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        onPressed:()=>weekBottomSheet(context),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          child: Text(
            widget.selectedValue,
            style: TextStyle(color: Colors.white),
          ),
        color: Colors.blueAccent,
      ),
    );
  }
}
