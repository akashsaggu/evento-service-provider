import 'package:flutter/material.dart';

import 'SpinnerItem.dart';

class DropDownSpinner<T extends SpinnerItem> extends StatefulWidget {
  final List<T> items;
  final int selectPos;
  final bool displayBorder;

  get dropDownState => DropDownWidget<T>();
  final ValueChanged<T> valueChangeListener;

  DropDownSpinner(this.items, this.valueChangeListener,
      {this.selectPos = 0, this.displayBorder = true});

  @override
  State<StatefulWidget> createState() {
    return dropDownState;
  }
}

class DropDownWidget<T extends SpinnerItem> extends State<DropDownSpinner>  with AutomaticKeepAliveClientMixin<DropDownSpinner>{
  T selectedValue;
  List<DropdownMenuItem<T>> itemList;

  @override
  void initState() {
    super.initState();
    itemList = widget.items
        .map((item) => DropdownMenuItem<T>(
            value: item,
            child: Text(item?.toValue() ?? "ss",
                style: TextStyle(color: Colors.grey))))
        .toList();
    selectedValue = itemList[widget.selectPos ?? 0].value;
    widget.valueChangeListener(selectedValue);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  width: 2.0,
                  color: widget.displayBorder
                      ? Color(0x66ffd037)
                      : Colors.transparent))),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 4.0, 4.0, 4.0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<T>(
            isDense: true,
            hint: Text(selectedValue?.toValue() ?? "s"),
            value: selectedValue,
            items: itemList,
            onChanged: (value) {
              widget.valueChangeListener(value); // no need
              selectedValue = value;
              setState(() {
                selectedValue = value;
              });
            },
            isExpanded: true,
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
