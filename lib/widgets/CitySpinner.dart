import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/bloc/CategoryBloc.dart';
import 'package:evento_provider/model/city_entity.dart';
import 'package:evento_provider/widgets/spinner/DropDownSpinner.dart';
import 'package:flutter/cupertino.dart';

import 'CategorySpinner.dart';

// ignore: must_be_immutable
class CitySpinner extends StatelessWidget {
  final ValueChanged<CityData> valueChanged;
  String preSelectedCity;
  int selectedIndex;

  CitySpinner(this.valueChanged, {this.preSelectedCity});

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CategoryBloc>(context);
    return StreamBuilder<CityEntity>(
        stream: bloc.cityBloc.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.data != null &&
              snapshot.data.status) {
            final list = new CityEntity.fromJson(snapshot.data.toJson()).data;
            if (preSelectedCity != null) {
              try {
                selectedIndex = snapshot.data.data
                    .indexWhere((city) => city.id == preSelectedCity);
              } catch (e) {
                selectedIndex = 0;
              }
            } else {
              selectedIndex = 0;
            }
            if (selectedIndex == -1) {
              selectedIndex = 0;
            }
            if (snapshot.data.status && snapshot.data.data.isNotEmpty) {
              return DropDownSpinner(
                list,
                (value) {
                  valueChanged(value);
                },
                selectPos: selectedIndex,
              );
            } else {
              return DropDownSpinner([DropDownItem("")], (value) {});
            }
          } else {
            return DropDownSpinner([DropDownItem("")], (value) {});
          }
        });
  }
}
