

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/bloc/CategoryBloc.dart';
import 'package:evento_provider/model/country_entity.dart';
import 'package:evento_provider/widgets/spinner/DropDownSpinner.dart';
import 'package:flutter/cupertino.dart';

import 'CategorySpinner.dart';

// ignore: must_be_immutable
class CountrySpinner extends StatelessWidget {
  final ValueChanged<CountryData> valueChanged;
  String preSelectedCountry;
  int selectedIndex;

  CountrySpinner(this.valueChanged, {this.preSelectedCountry});

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CategoryBloc>(context);
    return StreamBuilder<CountryEntity>(
        stream: bloc.countryBloc.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.status && snapshot.data.data.isNotEmpty) {
              if (preSelectedCountry != null) {
                try {
                  selectedIndex = snapshot.data.data
                      .indexWhere((country) =>
                  country.id == preSelectedCountry);
                } catch (e) {
                  selectedIndex = 0;
                }
              } else {
                selectedIndex = 0;
              }
              if (selectedIndex == -1) {
                selectedIndex = 0;
              }
              return DropDownSpinner(
                snapshot.data.data,
                    (value) {
                  bloc.getCity(value.id.toString());
                  valueChanged(value);
                },
                selectPos: selectedIndex,
              );
            } else {
              return DropDownSpinner([DropDownItem("")], (value) {});
            }
          } else {
            return DropDownSpinner([DropDownItem("")], (value) {});
          }
        });
  }
}
