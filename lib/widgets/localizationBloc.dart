
import 'dart:ui';

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:rxdart/rxdart.dart';


class LocalizationBloc implements BlocBase {

  final bloc = BehaviorSubject<Locale>();
  LocalizationBloc._privateConstructor();

  static final LocalizationBloc instance = LocalizationBloc._privateConstructor();

  void changeLocale(Locale local) {
    bloc.add(local);
  }

  @override
  void dispose() {

  }

}