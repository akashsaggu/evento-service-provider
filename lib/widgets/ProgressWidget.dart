import 'dart:ui';

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/constants/AppColors.dart';
import 'package:evento_provider/utils/Util.dart';
import 'package:flutter/material.dart';

import 'package:rxdart/rxdart.dart';

class ProgressWidgetWrapper extends StatelessWidget {
  final Widget child;
  final bloc = ProgressBloc();

  ProgressWidgetWrapper({@required this.child});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProgressBloc>(
        bloc: bloc,
        child: Container(
          height: screenHeight(context),
          width: screenHeight(context),
          child: Stack(
            children: <Widget>[
              child,
              buildProgressIndicator(),
            ],
          ),
        ));
  }

  Widget buildProgressIndicator() {
    //bloc.hideProgress();
    return ProgressIndicator();
  }
}

class ProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProgressBloc>(context);

    return StreamBuilder<bool>(
        stream: bloc.pBloc,
        initialData: false,
        builder: (context, snapshot) {
          return Visibility(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
              child: new Container(
                height: double.infinity,
                width: double.infinity,
                decoration:
                new BoxDecoration(color: AppColors.primaryColor.withOpacity(0.1)),
                child: Center(child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        AppColors.primaryColor))),
              ),
            ),
            visible: snapshot.data,
          );
        });
  }
}

class ProgressBloc implements BlocBase {
  final pBloc = BehaviorSubject<bool>();

  void showProgress() {
    pBloc.add(true);
  }

  void hideProgress() {
    pBloc.add(false);
  }

  @override
  void dispose() {
    pBloc.close();
  }
}
