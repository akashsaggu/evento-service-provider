import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/bloc/CategoryBloc.dart';
import 'package:evento_provider/model/sub_category_entity.dart';
import 'package:evento_provider/widgets/spinner/DropDownSpinner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'CategorySpinner.dart';

// ignore: must_be_immutable
class SubCategorySpinner extends StatelessWidget {
  final ValueChanged<SubCategoryData> valueChanged;
  String preSelectedSubCategory;
  int selectedIndex;

  SubCategorySpinner(this.valueChanged, {this.preSelectedSubCategory});

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CategoryBloc>(context);
    return StreamBuilder<SubCategoryEntity>(
        stream: bloc.subCategoryBloc.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.status && snapshot.data.data.isNotEmpty) {
              if (preSelectedSubCategory != null) {
                try {
                  selectedIndex = snapshot.data.data.indexWhere((subCategory) =>
                      subCategory.id == preSelectedSubCategory);
                } catch (e) {
                  selectedIndex = 0;
                }
              } else {
                selectedIndex = 0;
              }
              if (selectedIndex == -1) {
                selectedIndex = 0;
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
              Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8.0),
          child: Text("Select SubCategory", style: TextStyle(color: Colors.white))),
                  DropDownSpinner(
                    snapshot.data.data,
                    (value) {
                      //bloc.getSubCategories(value.id.toString());
                      valueChanged(value);
                    },
                    selectPos: selectedIndex,
                  ),
                ],
              );
            } else if (snapshot.data.data.isEmpty) {
              valueChanged(SubCategoryData(id: ""));
              return SizedBox();
            } else {
              valueChanged(SubCategoryData(id: ""));
              return SizedBox();
            }
          } else {
            valueChanged(SubCategoryData(id: ""));
            return SizedBox();
          }
        });
  }
}
