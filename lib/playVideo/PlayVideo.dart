import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player/youtube_player.dart' hide VideoPlayerController;

class PlayVideo extends StatefulWidget {
  final String videoString;
  final String videoType;

  PlayVideo(this.videoString, this.videoType);

  @override
  State<StatefulWidget> createState() {
    return PlayVideoState();
  }
}

class PlayVideoState extends State<PlayVideo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        child: Stack(
          children: <Widget>[
            SafeArea(
              child: Align(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: InkWell(
                      child: Icon(Icons.arrow_back, color: Colors.white),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  alignment: Alignment.topLeft),
            ),
            Align(
              alignment: Alignment.center,
              child: getVideoPlayer(),
            ),
          ],
        ),
      ),
    );
  }

  Widget getVideoPlayer() {
    if (widget.videoType == "youtube_link") {
      return YoutubePlayer(
        context: context,
        source: widget.videoString,
        quality: YoutubeQuality.MEDIUM,
      );
    } else {
      final chewieController = ChewieController(
        videoPlayerController:
            VideoPlayerController.network(widget.videoString),
        aspectRatio: 3 / 2,
        autoPlay: true,
        looping: true,
      );
      return Chewie(
        controller: chewieController,
      );
    }
  }
}
