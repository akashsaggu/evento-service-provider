import 'dart:async';

import 'package:evento_provider/base/bloc/BlocBase.dart';
import 'package:evento_provider/model/category_entity.dart';
import 'package:evento_provider/model/city_entity.dart';
import 'package:evento_provider/model/country_entity.dart';
import 'package:evento_provider/model/sub_category_entity.dart';
import 'package:evento_provider/screens/register/repo/RegisterRepo.dart';
import 'package:rxdart/rxdart.dart';

class CategoryBloc extends BlocBase {
  // ignore: close_sinks
  final categoryBloc = BehaviorSubject<CategoryEntity>();
  // ignore: close_sinks
  final subCategoryBloc = BehaviorSubject<SubCategoryEntity>();
  // ignore: close_sinks
  final countryBloc = BehaviorSubject<CountryEntity>();
  // ignore: close_sinks
  final cityBloc = BehaviorSubject<CityEntity>();
  final addressBloc = StreamController<String>();
  final repo = RegisterRepo();


  getCategories() async {
    var entity = await repo.getCategories();
    categoryBloc.add(entity);
  }

  getSubCategories(String categoryId) async {
    subCategoryBloc.add(SubCategoryEntity(message: "Sub Category data found", data: [])
      ..status = true);
    var entity = await repo.getSubCategories(categoryId);
    subCategoryBloc.add(entity);
  }

  getCountries(String countryLang ) async {
    var entity = await repo.getCountryList("");
    countryBloc.add(entity);
  }

  getCity(String countryId ) async {
    cityBloc.add(CityEntity(message: "City data Found", data: [])
      ..status = true);
    var entity = await repo.getCityList(countryId);
    cityBloc.add(entity);
  }

  setAddress(String address) {
    addressBloc.sink.add(address);
  }

  @override
  void dispose() {
    categoryBloc.close();
  }
}
